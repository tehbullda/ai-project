// Identifier, auratype, effecttype, power, dispellable, lifetime, radius, description
DoT Debuff DamageOverTime 200 1 8.0 0.0 "This effect deals 200 damage per second"
HoT Buff HealingOverTime 500 0 15.0 0.0 "This effect heals 500 health per second"
Stun Debuff Stun 0 0 4.0 0.0 "This effect immobilizes a unit and makes it unable to act"
Slow Debuff Slow 25 1 3.0 0.0 "This effect slows a units movespeed"