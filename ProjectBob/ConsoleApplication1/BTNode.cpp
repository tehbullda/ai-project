//BTNode.cpp

#include "stdafx.h"
#include "BTNode.hpp"

BTNode::BTNode(AIGame::Agent* p_xOwner)
	:m_eState(Invalid)
{
	m_xOwner = p_xOwner;
}

BTNode::BTNode()
	:m_eState(Invalid)
{

}

BTNode::~BTNode()
{
	m_xOwner = nullptr;
}

BTNode::StateType BTNode::Update()
{
	return m_eState;
}

void BTNode::OnInitialize()
{
	m_eState = Running;
}

void BTNode::OnTerminate(BTNode::StateType p_eState)
{
	//m_eState = Invalid;
}

BTNode::StateType BTNode::Tick()
{
	if (m_eState == Invalid)
	{
		OnInitialize();
	}

	m_eState = Update();

	if (m_eState != Running)
	{
		OnTerminate(m_eState);
	}
	return m_eState;
}


void BTNode::SetState(StateType p_eState)
{
	m_eState = p_eState;
}

BTNode::StateType BTNode::GetState()
{
	return m_eState;
}