//BTNode.hpp

#pragma once
#include "stdafx.h"
#include "GameObject.hpp"
#include "Agent.hpp"

class BTNode {
public:
	enum StateType{
		Invalid,
		Success,
		Failure,
		Running,
	};

	BTNode();

	/*Must use this for the root in each tree*/
	BTNode(AIGame::Agent* p_xOwner);
	virtual ~BTNode();

protected:
	virtual StateType Update() = 0;

	virtual void OnInitialize();
	virtual void OnTerminate(StateType p_eState);

	

public:
	StateType Tick();

	void SetState(StateType p_eState);

	/*------debugging-------*/
	StateType GetState();

protected:
	StateType m_eState;
	AIGame::Agent* m_xOwner;
};