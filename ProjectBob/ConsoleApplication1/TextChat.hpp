#pragma once
#include "TextureManager.hpp"
#include "FontManager.hpp"
#include "Sprite.hpp"
#include "Window.hpp"
#include "PremadeButton.hpp"

namespace AIGame
{

	class TextChat
	{
	private:
		TextChat();
	public:
		typedef std::unique_ptr<TextChat> Ptr;
		static Ptr Create();
		~TextChat();

		void update();
		void draw();

		void setPosition(const sf::Vector2f& p_Pos);
		void setPosition(const float& p_X, const float& p_Y);


		void setMaxMessages(const unsigned& p_Num);
		void sendMessage(const std::string& p_Code, const std::string& p_Message);
		void getMessage();
	private:
		void moveBox();
		bool pointInside(const sf::Vector2f& p_Point, const sf::Vector2f& p_Pos, const sf::Vector2f& p_Vol);

		void updateTextPos();
		void addTextContent(const std::string& p_Content);
		void removeTextContent();
		void toggleLock();
	private:
		kartong::TextureManager* m_TextureManager;
		kartong::FontManager* m_FontManager;
		kartong::Window* m_Window;
		kartong::system::input::Input* m_Input;
	private:
		std::vector<std::string> m_Messages;
		std::vector<sf::Text*> m_DisplayText;
		
		bool m_Unlocked;

		sf::RectangleShape* m_Background;
		sf::RectangleShape* m_TopBar;
		sf::RectangleShape* m_ScrollBack;
		kartong::Premade::PremadeButton* m_UpButton;
		kartong::Premade::PremadeButton* m_DownButton;
		sf::RectangleShape* m_Scroller;
		sf::Vector2f m_Position;
		unsigned m_MaxMessages;
		float m_ChatMessageSpacing;
		float m_EdgeSnap;
	public:
		void loadLines();
		std::string getLine(const int& p_Type);

		enum LineTypes{
			Line_Spawn,
			Line_Death,
			Line_Inslut,
			Line_Motivation,
		};

	private:
		std::vector<std::string> m_Lines_Spawn;
		std::vector<std::string> m_Lines_Death;
		std::vector<std::string> m_Lines_Insult;
		std::vector<std::string> m_Lines_Motivation;
	};
}