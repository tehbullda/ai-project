#pragma once
#include "Window.hpp"
#include "ServiceLocator.hpp"
#include "FontManager.hpp"

namespace AIGame
{
	class Grid;
	enum NODETYPE
	{
		EMPTY,
		WALL,
		INCOMINGHAZARD,
		HAZARDS,
		PATHTEST,
		OPENLIST,
		CLOSEDLIST,
		SIZE
	};

	class Node
	{
	public:
		Node(const sf::Vector2f p_Pos);
		~Node();
		
		sf::Vector2i getGridPosition();
		int getType();
		void setType(const int& p_Type);
		void setParent(Node* p_Parent);
		Node* getParent();

		std::string GetOccupiedBy()
		{
			return m_sOccupiedBy;
		};
		void SetOccupiedBy(std::string p_sOccupiedBy)
		{
			m_sOccupiedBy = p_sOccupiedBy;
		};

	public:
		float g;
		float h;
		float f;
	
	private:
		Node* m_Parent;
		Grid* m_Grid;
		sf::Vector2i m_GridPos;
		int m_Type;
		std::string m_sOccupiedBy;
	};

	class Tile
	{
	public:
		Tile(sf::Vector2f p_Pos);
		~Tile();
	
		
		Node* m_Node;
		sf::Text* m_Text;
		sf::Vector2f m_Position;

	private:
		kartong::FontManager* m_FontManager;
	};

	class Grid
	{
	private:
		Grid(const sf::Vector2i& p_GridSize, const float& p_TileSize);
	public:
		~Grid();

		typedef std::unique_ptr<Grid> Ptr;
		static Ptr Create(const sf::Vector2i& p_GridSize, const float& p_TileSize);

		void initialize();
		void update();
		void draw();

	private:
		sf::Color getColor(const int& p_Type);
		void RemoveAllParents();
	public:
		sf::Vector2i getGridPos(const sf::Vector2f& p_Pos);
		sf::Vector2i getGridPos(const float& p_X, const float& p_Y);
		sf::Vector2f getPixelPos(const sf::Vector2i& p_Pos);
		sf::Vector2f getPixelPos(const int& p_X, const int& p_Y);
		sf::Vector2f getPixelPos(Node* p_xNode);
		sf::Vector2i getGridSize();

		void setPosition(const sf::Vector2f& p_Pos);
		void setPosition(const float& p_X, const float& p_Y);
		void setType(const sf::Vector2i& p_Node, const int& p_Type);
		void setType(const int& p_X, const int& p_Y, const int& p_Type);
		int getType(const sf::Vector2f& p_Tile);
		int getType(const int& p_X, const int& p_Y);
		Tile* getTile(const sf::Vector2f& p_Tile);
		Tile* getTile(const int& p_X, const int& p_Y);
		Node* getNode(const sf::Vector2f& p_Tile);
		Node* getNode(const int& p_X, const int& p_Y);
		bool nodeExists(const sf::Vector2i& p_Node);
		bool nodeExists(const int& p_X, const int& p_Y);
		float GetTileSize();
		//  A*
	public:
		std::vector<Node*> getPath(Node* p_Start, Node* p_Goal);
		std::vector<Node*> getNeighbours(Node* p_Node);
	private:
		//std::vector<Node*> getNeighbours(Node* p_Node);
		Node* getCheapestNode(std::vector<Node*> p_List);
		float getNodeCost(Node* p_Node, Node* p_Start);
		bool cheaperFound(std::vector<Node*> p_List, const float& p_F);
		void removeNodeFromList(std::vector<Node*> &p_List, Node* p_Node);
		float getTypeCost(const int& p_Type);
		float getEstimatedDistance(Node* p_Node, Node* p_Goal);
	public:
		bool directPathAvaliable(const sf::Vector2f& p_Start, const sf::Vector2f& p_Goal);
	private:
		bool rayIntersected(const sf::Vector2f& p_Start, const sf::Vector2f& p_Direction, const sf::Vector2i& p_Tile);
	
	private:
		std::vector<Node*> m_OpenList;
		std::vector<Node*> m_ClosedList;
		std::vector<Node*> m_Path;
		Node* m_Goal;
		//  /A*
	private:
		kartong::Window* m_Window;
		kartong::FontManager* m_FontManager;
	private:
		std::vector<Tile*> m_Tiles;
		sf::RectangleShape* m_TileRect;

		sf::Vector2f m_Position;
		sf::Vector2f m_EndPosition;
		sf::Vector2i m_GridSize;
		float m_TileSize;

	};
}