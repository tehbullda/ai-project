#include "stdafx.h"

#include "Engine.hpp"
#include "ServiceLocator.hpp"
#include "ExampleState.hpp"

//
#include "GameState.hpp"
#include "PauseState.hpp"
#include "LoadingState.hpp"
#include "MenuState.hpp"
#include "OptionsState.hpp"
#include "SelectionState.hpp"
#include "PostGameState.hpp"
//
using namespace kartong;

static bool engineRunning;

Engine::Engine()
{



};
Engine::~Engine()
{


};

bool Engine::initialize()
{
	engineRunning = true;
	m_Window = Window::Create("ProjectBob", sf::Style::Fullscreen, 1920, 1080, 60);
	m_Window->initialize();
	m_Window->setClearColor(sf::Color(16, 16, 16, 255));
	ServiceLocator<Window>::set_service(m_Window.get());

	m_DeltaTime = DeltaTime::Create();

	UnitManager::setUnitSize((float)m_Window->getWindow()->getSize().x, (float)20);

	m_Input = system::input::Input::Create();
	m_Input->initialize();
	ServiceLocator<system::input::Input>::set_service(m_Input.get());

	m_KeyBinds = system::input::KeyBindings::Create();
	ServiceLocator<system::input::KeyBindings>::set_service(m_KeyBinds.get());
	m_KeyBinds->saveBinds("../Data/Config/Input/KeyBindings.txt");

	m_StateManager = StateManager::Create();
	ServiceLocator<StateManager>::set_service(m_StateManager.get());

	m_TextureManager = TextureManager::Create("../Data/Textures/");
	ServiceLocator<TextureManager>::set_service(m_TextureManager.get());

	m_AudioManager = AudioManager::Create("../Data/Audio/");
	ServiceLocator<AudioManager>::set_service(m_AudioManager.get());

	m_FontManager = FontManager::Create("../Data/Fonts/");
	ServiceLocator<FontManager>::set_service(m_FontManager.get());

	m_ParticleManager = ParticleManager::Create();
	ServiceLocator<ParticleManager>::set_service(m_ParticleManager.get());

	//Remove
	m_PostGameStats = AIGame::PostGameStats::Create();
	ServiceLocator<AIGame::PostGameStats>::set_service(m_PostGameStats.get());
	//Remove

	//m_StateManager->attach("ExampleMenu", new ExampleState("ExampleMenu"));
	//m_StateManager->attach("ExampleGame", new ExampleState("ExampleGame"));
	//m_StateManager->activate("ExampleMenu");
	//m_StateManager->activate("ExampleGame");
	m_StateManager->attach("OptionsState", new AIGame::OptionsState("OptionsState"));
	m_StateManager->attach("MenuState", new AIGame::MenuState("MenuState"));
	m_StateManager->attach("PauseState", new AIGame::PauseState("PauseState"));
	m_StateManager->attach("LoadingState", new AIGame::LoadingState("LoadingState"));
	m_StateManager->attach("SelectionState", new AIGame::SelectionState("SelectionState"));
	m_StateManager->attach("GameState", new AIGame::GameState("GameState"));
	m_StateManager->attach("PostGameState", new AIGame::PostGameState("PostGameState"));
	
	m_StateManager->activate("LoadingState");
	//m_StateManager->activate("PauseState");

	debugInit();

	if (!m_Window || !m_Input || !m_KeyBinds || !m_StateManager || !m_TextureManager
		|| !m_AudioManager || !m_FontManager)
		return false;

	return true;
};
void Engine::run()
{
	while (engineRunning)
	{
		if (m_Input->getKey(sf::Keyboard::LAlt))
			if (m_Input->getKeyDown(sf::Keyboard::F4))
				engine_shut();

		m_DeltaTime->Update();
		m_Input->update();
		m_Window->updateRect();
		m_StateManager->update();
		//m_ParticleManager->update();
		m_Window->clear();
		m_StateManager->draw();

		debugUpdate();
		debugDraw();

		m_Window->display();
	}
	m_Window->close();
};
void Engine::draw()
{

};

void Engine::engine_shut()
{
	engineRunning = false;
	Debug::write(EDebugLevel::INFO, "Engine shutting down");
};
void Engine::debugInit()
{
	m_FPSGraph = FPSGraph::Create();

}
void Engine::debugUpdate()
{
	m_FPSGraph->update();
};
void Engine::debugDraw()
{
	m_FPSGraph->draw();
};