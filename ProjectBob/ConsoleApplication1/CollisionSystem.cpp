#include "stdafx.h"
#include "BoundingUnits.hpp"
#include "CollisionSystem.hpp"

using namespace kartong;


CollisionSystem::CollisionSystem()
{

};
CollisionSystem::~CollisionSystem()
{

};
CollisionSystem::Ptr CollisionSystem::Create()
{
	return CollisionSystem::Ptr(new CollisionSystem());
};
AxisAlignedBoundingBox* CollisionSystem::createAABB(const sf::Vector2f& p_Pos, const sf::Vector2f& p_Vol)
{
	AxisAlignedBoundingBox* box = new AxisAlignedBoundingBox(p_Pos, p_Vol);
	m_BoundingList.push_back(box);
	return box;
};
BoundingSphere* CollisionSystem::createBoundingSphere(const sf::Vector2f& p_Pos, const float& p_Rad)
{
	return new BoundingSphere(p_Pos, p_Rad);
};
bool CollisionSystem::checkCollision(BoundingUnit* p_Unit, BoundingUnit* p_Collided)
{
	if (p_Unit->GetType() == BoundingTypes::SPHERE && p_Collided->GetType() == BoundingTypes::SPHERE)
	{
		if (p_Unit->circleIntersects(p_Collided->GetPos(), static_cast<BoundingSphere*>(p_Collided)->GetRadius()))
			return true;
	}
	return false;
};
