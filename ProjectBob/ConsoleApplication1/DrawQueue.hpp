#pragma once
#include "Window.hpp"

namespace kartong
{
	class DrawQueue
	{
	private:
		DrawQueue();
	public:
		~DrawQueue();
		typedef std::unique_ptr<DrawQueue> Ptr;
		static Ptr Create();

		void draw();

		void add(const int& p_Layer, const int& p_Type, void* p_Thing);


	private:

	private:

		std::map<int, std::vector<std::pair<int, void*>*>*> m_Queue;
	};
}