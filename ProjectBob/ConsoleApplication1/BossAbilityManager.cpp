#include "stdafx.h"
#include "BossAbilityManager.hpp"
#include "Grid.hpp"
#include "Randomizer.hpp"
#include "ServiceLocator.hpp"
#include "AuraManager.hpp"
#include "SelectionSettings.hpp"
#include "GameObjectManager.hpp"
#include "ServiceLocator.hpp"
#include "PostGameStats.hpp"

namespace AIGame {
	BossAbility::BossAbility(Grid *grid, float cooldown, float casttime, int power, std::string name, bool appliesdebuff, Aura *aura) {
		m_grid = grid;
		m_appliesdebuff = appliesdebuff;
		m_cooldown = cooldown;
		m_name = name;
		m_aura = aura;
		m_casting = false;
		m_casttime = casttime;
		m_power = power;
		m_agents = kartong::ServiceLocator<GameObjectManager>::get_service()->GetAgentVector();
	}
	BossAbility::~BossAbility() {
		if (m_aura != nullptr) {
			delete m_aura;
			m_aura = nullptr;
		}
		m_grid = nullptr;
		m_agents.clear();
	}

	void BossAbility::UpdateAgentVector() {
		m_agents = kartong::ServiceLocator<GameObjectManager>::get_service()->GetAgentVector();
	}

	Smash::Smash(Grid *grid, float cooldown, float casttime, int power, std::string name, bool appliesdebuff, Aura *aura) : BossAbility(grid, cooldown, casttime, power, name, appliesdebuff, aura) {

	}
	Smash::~Smash()
	{
		BossAbility::~BossAbility();
	}
	bool Smash::Update() {
		m_currenttime = m_casting ? m_currenttime : m_currenttime + kartong::DeltaTime::getDeltaTime();
		if (m_currenttime > m_cooldown) {
			m_currenttime = m_casttime;
			return true;
		}
		if (m_casting) {
			m_currenttime -= kartong::DeltaTime::getDeltaTime();
			if (m_currenttime < 0.0f) {
				m_currenttime = 0.0f;
				return true;
			}
		}
		return false;
	}
	void Smash::StartCast() {
		sf::Vector2f bosspos = kartong::ServiceLocator<BlackBoard>::get_service()->GetBossPos();
		bosspos = (sf::Vector2f)m_grid->getGridPos(bosspos) + sf::Vector2f(1, 1);
		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				if (i == 0 && j == 0) {
					continue;
				}
				if (bosspos.x - i >= 0 && bosspos.x - i <= m_grid->getGridSize().y - 1 && bosspos.y - j >= 0 && bosspos.y - j <= m_grid->getGridSize().x - 1) {
					for (unsigned i = 0; i < m_agents.size(); i++) {
						if (m_grid->getGridPos(m_agents[i]->GetPos()) == sf::Vector2i((int)bosspos.x - i, (int)bosspos.y - j)) {
							if (m_grid->getNode(sf::Vector2f(bosspos.x - i, bosspos.y - j))->getType() == 0) {
								m_grid->getNode(sf::Vector2f(bosspos.x - i, bosspos.y - j))->setType(2);
								m_target = sf::Vector2i((int)bosspos.x - i, (int)bosspos.y - j);
								m_casting = true;
								return;
							}
						}
					}
				}
			}
		}
		std::cout << "No agents close enough or target obscured, aborting cast: Smash" << std::endl;
		m_currenttime = 0.0f;
		m_casting = false;
	}
	void Smash::Cast() {
		kartong::ServiceLocator<PostGameStats>::get_service()->reportSmashCast();
		m_grid->getNode((sf::Vector2f)m_target)->setType(0);
		for (unsigned i = 0; i < m_agents.size(); i++) {
			if (m_grid->getGridPos(m_agents[i]->GetPos()) == m_target) {
				m_agents[i]->TakeDamage(m_power);
			}
		}
		m_casting = false;
	}

	Cleave::Cleave(Grid *grid, float cooldown, float casttime, int power, std::string name, bool appliesdebuff, Aura *aura) : BossAbility(grid, cooldown, casttime, power, name, appliesdebuff, aura) {
	}

	Cleave::~Cleave()
	{
		BossAbility::~BossAbility();
	}

	bool Cleave::Update() {
		m_currenttime = m_casting ? m_currenttime : m_currenttime + kartong::DeltaTime::getDeltaTime();
		if (m_currenttime > m_cooldown) {
			m_currenttime = m_casttime;
			return true;
		}
		if (m_casting) {
			m_currenttime -= kartong::DeltaTime::getDeltaTime();
			if (m_currenttime < 0.0f) {
				m_currenttime = 0.0f;
				return true;
			}
		}
		return false;
	}
#define cleaverangeX 3
#define cleaverangeY 3
	void Cleave::StartCast() {
		sf::Vector2i bosspos = m_grid->getGridPos(kartong::ServiceLocator<BlackBoard>::get_service()->GetBossPos()) + sf::Vector2i(1, 1);
		m_dir = static_cast<EDirection>(kartong::Randomizer::GetRandomInt(0, EDirection::Count - 1));
		int xMin, xMax, yMin, yMax;
		switch (m_dir) {
		case Up:
			//-2 - 2, -2 - 0
			xMin = -cleaverangeX;
			xMax = cleaverangeX;
			yMin = -cleaverangeY;
			yMax = 0;
			break;
		case Down:
			//-2 - 2, 0 - 2
			xMin = -cleaverangeX;
			xMax = cleaverangeX;
			yMin = 0;
			yMax = cleaverangeY;
			break;
		case Left:
			//-2 - 0, -2 - 2
			xMin = -cleaverangeX;
			xMax = 0;
			yMin = -cleaverangeY;
			yMax = cleaverangeY;
			break;
		case Right:
			//0 - 2, -2 - 2
			xMin = 0;
			xMax = cleaverangeX;
			yMin = -cleaverangeY;
			yMax = cleaverangeY;
			break;
		}
		for (int x = xMin; x <= xMax; x++) {
			for (int y = yMin; y <= yMax; y++) {
				if (y == 0 && x == 0) {
					continue;
				}
				if (bosspos.x + x >= 0 && bosspos.x + x <= m_grid->getGridSize().y - 1 && bosspos.y - y >= 0 && bosspos.y - y <= m_grid->getGridSize().x - 1) {
					if (m_grid->getNode(sf::Vector2f((float)(bosspos.x + x), (float)(bosspos.y - y)))->getType() == 0) {
						m_grid->getNode(sf::Vector2f((float)(bosspos.x + x), (float)(bosspos.y - y)))->setType(2);
						m_affectedtiles.push_back(sf::Vector2i(bosspos.x + x, bosspos.y - y));
					}
				}
			}
		}
		m_casting = true;
	}
	void Cleave::Cast() {
		kartong::ServiceLocator<PostGameStats>::get_service()->reportCleaveCast();
		for (unsigned i = 0; i < m_affectedtiles.size(); i++) {
			for (unsigned j = 0; j < m_agents.size(); j++) {
				if (m_grid->getGridPos(m_agents[j]->GetPos()) == m_affectedtiles[i]) {
					m_agents[j]->TakeDamage(m_power);
				}
			}
			m_grid->getNode((sf::Vector2f)m_affectedtiles[i])->setType(0);
		}
		m_casting = false;
	}

	Plague::Plague(Grid *grid, float cooldown, float casttime, int power, std::string name, bool appliesdebuff, Aura *aura) : BossAbility(grid, cooldown, casttime, power, name, appliesdebuff, aura) {

	}

	Plague::~Plague()
	{
		BossAbility::~BossAbility();
	}

	bool Plague::Update() {
		m_currenttime = m_casting ? m_currenttime : m_currenttime + kartong::DeltaTime::getDeltaTime();
		if (m_currenttime > m_cooldown) {
			m_currenttime = m_casttime;
			return true;
		}
		if (m_casting) {
			m_currenttime -= kartong::DeltaTime::getDeltaTime();
			if (m_currenttime < 0.0f) {
				m_currenttime = 0.0f;
				return true;
			}
		}
		return false;
	}
	void Plague::StartCast() {
		m_casting = true;
	}
	void Plague::Cast() {
		kartong::ServiceLocator<PostGameStats>::get_service()->reportPlagueCast();
		SpreadPlague();
		m_casting = false;
	}
	void Plague::SpreadPlague() {
		kartong::ServiceLocator<AuraManager>::get_service()->AddAuraToAllAgents(m_aura);
	}
#define babaamradius 4
	Babaam::Babaam(Grid *grid, float cooldown, float casttime, int power, std::string name, bool appliesdebuff, Aura *aura) : BossAbility(grid, cooldown, casttime, power, name, appliesdebuff, aura) {

	}
	Babaam::~Babaam()
	{
		BossAbility::~BossAbility();
	}
	bool Babaam::Update() {
		m_currenttime = m_casting ? m_currenttime : m_currenttime + kartong::DeltaTime::getDeltaTime();
		if (m_currenttime > m_cooldown) {
			m_currenttime = m_casttime;
			return true;
		}
		if (m_casting) {
			m_currenttime -= kartong::DeltaTime::getDeltaTime();
			if (m_currenttime < 0.0f) {
				m_currenttime = 0.0f;
				return true;
			}
		}
		return false;
	}
	void Babaam::StartCast() {
		sf::Vector2i bosspos = m_grid->getGridPos(kartong::ServiceLocator<BlackBoard>::get_service()->GetBossPos()) + sf::Vector2i(1, 1);
		for (int i = -babaamradius; i <= babaamradius; i++) {
			for (int j = -babaamradius; j <= babaamradius; j++) {
				if ((i == -babaamradius || i == babaamradius) && (j == -babaamradius || j == babaamradius)) {
					continue;
				}
				if (bosspos.x - i >= 0 && bosspos.x - i <= m_grid->getGridSize().y - 1 && bosspos.y - j >= 0 && bosspos.y - j <= m_grid->getGridSize().x - 1) {
					if (m_grid->getNode((sf::Vector2f)(sf::Vector2i(bosspos.x - i, bosspos.y - j)))->getType() == 0) {
						m_grid->getNode((sf::Vector2f)(sf::Vector2i(bosspos.x - i, bosspos.y - j)))->setType(2);
						m_slowtiles.push_back(sf::Vector2f((float)(bosspos.x - i), (float)(bosspos.y - j)));
					}
				}
			}
		}
		m_casting = true;
	}
	void Babaam::Cast() {
		kartong::ServiceLocator<PostGameStats>::get_service()->reportBabaamCast();
		for (unsigned i = 0; i < m_slowtiles.size(); i++) {
			for (unsigned j = 0; j < m_agents.size(); j++) {
				if (m_grid->getGridPos(m_agents[j]->GetPos()) == (sf::Vector2i)m_slowtiles[i]) {
					kartong::ServiceLocator<AuraManager>::get_service()->AddAuraToAgent(m_agents[j], m_aura);
					m_agents[j]->TakeDamage(m_power);
				}
			}
			m_grid->getNode(m_slowtiles[i])->setType(0);
		}
		m_slowtiles.clear();
		m_casting = false;
	}

#define defaultlifetime 5.0f
#define ticktime 0.25f;
	SpreadLava::SpreadLava(Grid *grid, float cooldown, float casttime, int power, std::string name, bool appliesdebuff, Aura *aura) : BossAbility(grid, cooldown, casttime, power, name, appliesdebuff, aura) {
		m_lifetime = defaultlifetime;
		m_ticktime = ticktime;
	}
	SpreadLava::~SpreadLava() {
		m_lavatiles.clear();
		BossAbility::~BossAbility();
	}

	bool SpreadLava::Update() {
		m_currenttime = m_casting ? m_currenttime : m_currenttime + kartong::DeltaTime::getDeltaTime();

		if (m_currenttime > m_cooldown) {
			m_currenttime = m_casttime;
			return true;
		}
		if (m_casting) {
			m_currenttime -= kartong::DeltaTime::getDeltaTime();
			if (m_currenttime < 0.0f) {
				m_currenttime = 0.0f;
				return true;
			}
		}
		else if (m_lavatiles.size() > 0) {
			m_lifetime -= kartong::DeltaTime::getDeltaTime();
			m_ticktime -= kartong::DeltaTime::getDeltaTime();
			if (m_lifetime <= 0.0f) {
				ClearLava();
				m_lifetime = defaultlifetime;
			}
			else if (m_ticktime <= 0.0f) {
				CheckAgents();
				m_ticktime = ticktime;
			}
		}
		return false;
	}

	void SpreadLava::StartCast() {
		kartong::Randomizer::GetRandomDouble(0.0, 1.0) > 0.5
			? SpreadFromSetTile(m_grid->getNode(kartong::Randomizer::GetRandomInt(0, m_grid->getGridSize().y - 1), kartong::Randomizer::GetRandomInt(0, m_grid->getGridSize().x - 1)), 1) : SpreadOnRandomTiles();
		m_casting = true;
	}
	void SpreadLava::Cast() {
		kartong::ServiceLocator<PostGameStats>::get_service()->reportSpreadLavaCast();
		for (unsigned i = 0; i < m_lavatiles.size(); i++) {
			m_grid->getNode((sf::Vector2f)m_lavatiles[i])->setType(3);
		}
		m_casting = false;
	}
	void SpreadLava::SpreadOnRandomTiles(double chance) {
		for (int i = 0; i < m_grid->getGridSize().x; i++) {
			for (int j = 0; j < m_grid->getGridSize().y; j++) {
				if (kartong::Randomizer::GetRandomDouble(0.0, 1.0) < chance && m_grid->getNode(j, i)->getType() == 0) {
					m_grid->getNode(j, i)->setType(2);
					m_lavatiles.push_back(sf::Vector2i(j, i));
				}
			}
		}
	}

	void SpreadLava::SpreadFromSetTile(Node *target, int radius) {
		//target->setType(2);
		sf::Vector2i targetpos = target->getGridPosition();
		for (int i = -radius; i <= radius; i++) {
			for (int j = -radius; j <= radius; j++) {
				if (targetpos.x - i >= 0 && targetpos.x - i <= m_grid->getGridSize().y - 1 && targetpos.y - j >= 0 && targetpos.y - j <= m_grid->getGridSize().x - 1) {
					if (m_grid->getNode(sf::Vector2f((float)(targetpos.x - i), (float)(targetpos.y - j)))->getType() == 0) {
						m_grid->getNode(sf::Vector2f((float)(targetpos.x - i), (float)(targetpos.y - j)))->setType(2);
						m_lavatiles.push_back(sf::Vector2i(targetpos.x - i, targetpos.y - j));
					}
				}
			}
		}
	}

	void SpreadLava::ClearLava() {
		for (unsigned i = 0; i < m_lavatiles.size(); i++) {
			m_grid->getNode(sf::Vector2f(m_lavatiles[i]))->setType(0);
		}
		m_lavatiles.clear();
	}

	void SpreadLava::CheckAgents() {
		for (unsigned i = 0; i < m_lavatiles.size(); i++) {
			for (unsigned j = 0; j < m_agents.size(); j++) {
				if (m_grid->getGridPos(m_agents[j]->GetPos()) == (sf::Vector2i)m_lavatiles[i]) {
					m_agents[j]->TakeDamage(m_power);
				}
			}
		}
	}

	JumpAway::JumpAway(Grid *grid, float cooldown, float casttime, int power, std::string name, bool appliesdebuff, Aura *aura) : BossAbility(grid, cooldown, casttime, power, name, appliesdebuff, aura) {

	}

	JumpAway::~JumpAway()
	{
		BossAbility::~BossAbility();
	}
	bool JumpAway::Update() {
		m_currenttime = m_casting ? m_currenttime : m_currenttime + kartong::DeltaTime::getDeltaTime();
		if (m_currenttime > m_cooldown) {
			m_currenttime = m_casttime;
			return true;
		}
		if (m_casting) {
			m_currenttime -= kartong::DeltaTime::getDeltaTime();
			if (m_currenttime < 0.0f) {
				m_currenttime = 0.0f;
				return true;
			}
		}
		return false;
	}

	void JumpAway::StartCast() {
		m_target = sf::Vector2i(kartong::Randomizer::GetRandomInt(1, m_grid->getGridSize().y - 1), kartong::Randomizer::GetRandomInt(1, m_grid->getGridSize().x - 1));
		while (m_grid->getNode((sf::Vector2f)m_target)->getType() == 1) {
			m_target = sf::Vector2i(kartong::Randomizer::GetRandomInt(1, m_grid->getGridSize().y - 1), kartong::Randomizer::GetRandomInt(1, m_grid->getGridSize().x - 1));
		}
		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				if (m_target.x - i >= 0 && m_target.x - i <= m_grid->getGridSize().y - 1 && m_target.y - j >= 0 && m_target.y - j <= m_grid->getGridSize().x - 1) {
					if (m_grid->getNode(sf::Vector2f((float)(m_target.x - i), (float)(m_target.y - j)))->getType() != 1) {
						m_grid->getNode(sf::Vector2f((float)(m_target.x - i), (float)(m_target.y - j)))->setType(2);
					}
				}
			}
		}
		m_casting = true;
	}
	void JumpAway::Cast() {
		kartong::ServiceLocator<PostGameStats>::get_service()->reportJumpAwayCast();
		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				if (m_target.x - i >= 0 && m_target.x - i <= m_grid->getGridSize().y - 1 && m_target.y - j >= 0 && m_target.y - j <= m_grid->getGridSize().x - 1) {
					m_grid->getNode(sf::Vector2f((float)(m_target.x - i), (float)(m_target.y - j)))->setType(0);
					for (unsigned k = 0; k < m_agents.size(); k++) {
						if (m_grid->getGridPos(m_agents[k]->GetPos()) == (sf::Vector2i(m_target.x - i, m_target.y - j))) {
							m_agents[k]->TakeDamage(m_power);
						}
					}
				}
			}
		}
		kartong::ServiceLocator<GameObjectManager>::get_service()->SetBossPos((sf::Vector2i)m_grid->getPixelPos(m_grid->getNode(sf::Vector2f((float)(m_target.x - 1), (float)(m_target.y - 1)))));
		m_casting = false;
	}

	BossAbilityManager::BossAbilityManager(Grid *grid) {
		m_grid = grid;
		AddDefaultAbilities();
		if (kartong::ServiceLocator<SelectionSettings>::get_service()->isDefault()) {
			SetDefaultAsActive();
		}
		else {
			std::vector<std::string> abilities = kartong::ServiceLocator<SelectionSettings>::get_service()->getAbilities();
			for (unsigned i = 0; i < abilities.size(); i++) {
				AddAbility(StringToAbility(abilities[i]), kartong::ServiceLocator<SelectionSettings>::get_service()->getAbilityDMG(abilities[i]));
			}
		}
	}
	BossAbilityManager::~BossAbilityManager() {
		/*bool isdefault = false;
		for (unsigned i = 0; i < m_activeabilities.size(); i++) {
			for (unsigned j = 0; j < m_defaultabilities.size(); j++) {
				if (m_activeabilities[i] == m_defaultabilities[j]) {
					m_activeabilities[i] = nullptr;
					isdefault = true;
				}
			}
			if (!isdefault) {
				delete m_activeabilities[i];
				m_activeabilities[i] = nullptr;
			}
			isdefault = false;
		}*/

		m_activeabilities.clear();

		for (unsigned i = 0; i < m_defaultabilities.size(); i++) {
			if (m_defaultabilities[i] != nullptr) {
				delete m_defaultabilities[i];
				m_defaultabilities[i] = nullptr;
			}
		}
		m_defaultabilities.clear();
	}

	bool BossAbilityManager::Update() {
		for (unsigned i = 0; i < m_activeabilities.size(); i++) {
			m_activeabilities[i]->UpdateAgentVector();
			if (m_activeabilities[i]->Update()) {
				if (!m_activeabilities[i]->Casting()) {
					m_activeabilities[i]->StartCast();
					kartong::Debug::write(kartong::EDebugLevel::INFO, "Starting cast: " + m_activeabilities[i]->GetName());
					kartong::ServiceLocator<BlackBoard>::get_service()->AddMessage("Boss starting cast: " + m_activeabilities[i]->GetName());
				}
				else {
					m_activeabilities[i]->Cast();
					kartong::Debug::write(kartong::EDebugLevel::INFO, "Casting: " + m_activeabilities[i]->GetName());
					kartong::ServiceLocator<BlackBoard>::get_service()->AddMessage("Boss casting: " + m_activeabilities[i]->GetName());
				}
			}
		}
		return true;
	}

	void BossAbilityManager::Cast(const std::string &name) {
		for (unsigned i = 0; i < m_activeabilities.size(); i++) {
			if (name == "Random") { // Inside the loop so it only does this if atleast one ability exists
				m_activeabilities[kartong::Randomizer::GetRandomInt(0, m_activeabilities.size() - 1)]->StartCast();
				break;
			}
			if (m_activeabilities[i]->GetName() == name) {
				m_activeabilities[i]->StartCast();
				break; //Incase more than one ability with that name exists
			}
		}
	}

	void BossAbilityManager::AddAbility(BossAbility* ability, float damagemult) {
		ability->SetPower((int)(ability->GetPower() * damagemult));
		if (ability != nullptr)
			m_activeabilities.push_back(ability);
	}

	void BossAbilityManager::AddDefaultAbilities() {
		m_defaultabilities.push_back(new Smash(m_grid, 5.0f, 1.0f));
		m_defaultabilities.push_back(new Cleave(m_grid, 8.0f, 2.0f));
		m_defaultabilities.push_back(new Plague(m_grid, 15.0f, 0.0f, 1, "Plague", true,
			new Aura("Plague", AuraManager::StringToAuraType("Debuff"), AuraManager::StringToEffectType("LinearDamageOverTime"), "Deals 10 damage per second", 1U, true, 3.0f, 0.0f)));
		m_defaultabilities.push_back(new Babaam(m_grid, 12.0f, 4.0f, 20, "Babaam", true,
			new Aura("Slow", AuraManager::StringToAuraType("Debuff"), AuraManager::StringToEffectType("Slow"), "Slows a unit for 20% movespeed", 20U, true, 2.0f, 0.0f)));
		m_defaultabilities.push_back(new SpreadLava(m_grid, 10.0f, 2.5f));
		m_defaultabilities.push_back(new JumpAway(m_grid, 15.0f, 2.0f, 0, "JumpAway", true,
			new Aura("Stun", AuraManager::StringToAuraType("Debuff"), AuraManager::StringToEffectType("Stun"), "Stuns a unit for 2 seconds", 0U, true, 2.0f, 0.0f)));
	}

	void BossAbilityManager::SetDefaultAsActive() {
		for (unsigned i = 0; i < m_activeabilities.size(); i++) {
			delete m_activeabilities[i];
			m_activeabilities[i] = nullptr;
		}
		for (unsigned i = 0; i < m_defaultabilities.size(); i++) {
			m_activeabilities.push_back(m_defaultabilities[i]);
		}
	}

	BossAbility* BossAbilityManager::StringToAbility(const std::string &name) {
		for (unsigned i = 0; i < m_defaultabilities.size(); i++) {
			if (m_defaultabilities[i]->GetName() == name) {
				return m_defaultabilities[i];
			}
		}
		return nullptr;
	}
}