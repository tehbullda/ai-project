#include "stdafx.h"
#include "FontManager.hpp"

using namespace kartong;

#define defaultfont "AdobeGothicStd-Bold.otf"

FontManager::FontManager(const std::string& p_Directory)
{
	preLoadFont(p_Directory + defaultfont);
	m_Directory = p_Directory;
	//AdobeFangsongStd-Regular.ott
};
FontManager::~FontManager()
{
	auto it = m_Fonts.begin();
	while (it != m_Fonts.end())
	{
		if (it->second){
			delete it->second;
			it->second = nullptr;
		}
		it++;
	}
	//m_Fonts.clear;
};
FontManager::Ptr FontManager::Create(const std::string& p_Directory)
{
	return FontManager::Ptr(new FontManager(p_Directory));
};

void FontManager::preLoadFont(const std::string& p_File)
{
	auto it = m_Fonts.begin();
	while (it != m_Fonts.end()){
		if (it->first == p_File){
			Debug::write(EDebugLevel::INFO, "Font already loaded: " + p_File);
			return;
		}
		it++;
	}
	Debug::write(EDebugLevel::INFO, "Preloading font: " + p_File);
	sf::Font* font = new sf::Font;
	font->loadFromFile(m_Directory + p_File);
	m_Fonts.insert(std::pair<std::string, sf::Font*>(p_File, font));
	font = nullptr;
};
sf::Font* FontManager::loadFont(const std::string& p_File)
{
	std::string file = m_Directory + p_File;
	auto it = m_Fonts.find(file);
	if (it == m_Fonts.end())
	{
		Debug::write(EDebugLevel::INFO, "Loading Font: " + p_File);
		sf::Font* font = new sf::Font;
		if (!font->loadFromFile(file)){
			Debug::write(EDebugLevel::ERROR, "Could not load font: " + p_File);
			return nullptr;
		}
		m_Fonts.insert(std::pair<std::string, sf::Font*>(file, font));
		font = nullptr;
		it = m_Fonts.find(file);
	}
	return it->second;
};

sf::Font* FontManager::getDefaultFont() {
	if (m_Fonts.find(m_Directory+ defaultfont) != m_Fonts.end()) {
		return m_Fonts.at(m_Directory + defaultfont);
	}
	else {
		loadFont(m_Directory + defaultfont);
		return m_Fonts.at(m_Directory + defaultfont);
	}
}