#include "stdafx.h"

#include "PauseState.hpp"
#include "ServiceLocator.hpp"
#include "Randomizer.hpp"

using namespace kartong;
using namespace AIGame;

PauseState::PauseState(const std::string& p_Name)
{
	m_StateManager = ServiceLocator<kartong::StateManager>::get_service();
	m_TextureManager = ServiceLocator<TextureManager>::get_service();
	m_FontManager = ServiceLocator<FontManager>::get_service();
	m_Input = ServiceLocator<system::input::Input>::get_service();
	m_Window = ServiceLocator<Window>::get_service();
	m_Name = p_Name;
	m_UpdateRequitement = StateRequirement::FIRST;
	m_DrawRequitement = StateRequirement::FIRST;
	//////////

};
PauseState::~PauseState()
{
	delete m_Buttons;
	m_Buttons = nullptr;

	delete m_Background;
	m_Background = nullptr;

	m_Input = nullptr;

};

bool PauseState::Initialize()
{
	m_Background = new sf::RectangleShape(m_Window->getSize());
	m_Background->setFillColor(sf::Color(0, 0, 0, 180));
	m_Buttons = new PauseStateButtons();

	return true;
};
bool PauseState::Update()
{
	m_Buttons->update();

	handleStates();
	return true;
};
void PauseState::Draw()
{
	m_Window->getWindow()->draw(*m_Background);
	m_Buttons->draw();
};

void PauseState::Exit()
{
	delete m_Background;
	m_Background = nullptr;
	delete m_Buttons;
	m_Buttons = nullptr;
};

std::string PauseState::getName()
{
	return m_Name;
};
int PauseState::getUpdateRequirement()
{
	return m_UpdateRequitement;
};
int PauseState::getDrawRequirement()
{
	return m_DrawRequitement;
};
void PauseState::handleStates()
{
	
};

//////////////
PauseStateButtons::PauseStateButtons()
{
	m_StateManager = ServiceLocator<kartong::StateManager>::get_service();
	m_TextureManager = ServiceLocator<TextureManager>::get_service();

	
	m_Resume = new Premade::PremadeButton(m_TextureManager->loadTexture("GUIHUD/Resume.png"));
	m_Options = new Premade::PremadeButton(m_TextureManager->loadTexture("GUIHUD/Options.png"));
	m_Menu = new Premade::PremadeButton(m_TextureManager->loadTexture("GUIHUD/Menu.png"));
	//m_Quit = new Premade::PremadeButton(m_TextureManager->loadTexture(""));
	m_Resume->setPosition(30, 30);
	m_Options->setPosition(30, 120);
	m_Menu->setPosition(30, 210);
	
};
PauseStateButtons::~PauseStateButtons()
{
	delete m_Resume;
	m_Resume = nullptr;
	delete m_Options;
	m_Options = nullptr;
	delete m_Menu;
	m_Menu = nullptr;
};


void PauseStateButtons::update()
{
	m_Resume->update();
	m_Options->update();
	m_Menu->update();
	if (m_Resume->isClicked()){
		m_StateManager->deactivate("PauseState");
	}
	else if (m_Menu->isClicked()) {
		m_StateManager->activate("MenuState");
		m_StateManager->deactivate("GameState");
		m_StateManager->deactivate("PauseState");
	}
};

void PauseStateButtons::draw()
{
	m_Resume->draw();
	m_Options->draw();
	m_Menu->draw();
};
