#pragma once

namespace AIGame {
	class Icon;
	class Aura {
	public:
		enum AuraType {
			Debuff,
			Buff,
			Global
		};
		enum EffectType {
			DamageOverTime,
			LinearDamageOverTime,
			ExponentialDamageOverTime,
			HealingOverTime,
			DamageMultiplier,
			DamageIncrease,
			DamageDecrease,
			DamageBlock,
			HPIncrease,
			HPDecrease,
			Stun,
			Slow,
			Count
		};
	private:
		struct Effect {
			EffectType m_type;
			std::string m_desc;
		};

	public:
		Aura(const std::string &identifier, const AuraType &type, const bool &dispellable, const float &lifetime = 0.0f, const float &radius = 5.0f);
		Aura(const std::string &identifier, AuraType auratype, EffectType effecttype, const std::string &effectdesc, const unsigned int &power, const bool &dispellable, const float &lifetime = 0.0f, const float &radius = 5.0f);
		~Aura();

		bool Update();
		void DrawIcon();
		// Does nothing
		void DrawEffect();

		void SetIcon(const std::string &filename);
		void SetIconPos(const sf::Vector2f &pos);
		void SetIconTooltip(const std::string &text);

		std::string GetIdentifier();
		AuraType GetAuraType();
		std::string GetAuraTypeAsString();
		Effect GetEffect();
		std::string GetEffectTypeAsString();
		std::string GetEffectDesc();
		unsigned int GetPower();
		float GetLifetime();
		float GetMaxLifetime();
		float GetRadius();
		bool GetDispellable();
		bool Ticked();

		void SetDispellable(const bool &status);
		void SetAuraType(const AuraType &type);
		void SetEffect(const EffectType &type, const std::string &text);
		void SetEffectDesc(const std::string &text);
		void SetPower(unsigned int &power);

	private:
		std::string m_identifier;
		unsigned int m_power;
		bool m_dispellable, m_tick;
		float m_lifetime, m_maxlifetime, m_radius, m_ticktimer;
		AuraType m_auratype;
		Effect m_effect;
		Icon *m_icon;
	};
}