//Projectile.cpp

#include "stdafx.h"
#include "Projectile.hpp"
#include "ServiceLocator.hpp"

#define SPEED 150.0f
#define TEXTUREPATH "particles/snowballmini.png"	

using namespace AIGame;
using namespace kartong;

Projectile::Projectile(sf::Vector2f p_xStartPos, sf::Vector2f p_xTargetPos, float p_fDamage) : GameObject(p_xStartPos, GameObject::Type_Projectile)
{
	m_xCollider = new kartong::BoundingSphere(p_xStartPos, 16.0f, sf::Vector2f(16.0f, 16.0f));
	m_xVel = SetVel(p_xStartPos, p_xTargetPos);
	SetSprite(TEXTUREPATH);
	m_xSprite->setPosition(p_xStartPos);
	m_fDamage = p_fDamage;

	//m_Emitter = ServiceLocator<ParticleManager>::get_service()->createEmitter("particles/particlesq.png");
	//m_Emitter->setLifeTime(2.1f, 2.3f);
	//m_Emitter->setMovement(MovementState::TOWARD);
	//m_Emitter->setColor(sf::Color::White, sf::Color::Cyan);
	//m_Emitter->setMoveSpeed(0, 0);
	//m_Emitter->setSpawnRadius(50, 50);
	//m_Emitter->setSpawnRate(0.001);
	//m_Emitter->initialize();
	
}

sf::Vector2f Projectile::SetVel(sf::Vector2f p_xStartPos, sf::Vector2f p_xTargetPos)
{
	sf::Vector2f tempSF = p_xTargetPos - p_xStartPos;
	kartong::Vector2 tempKartong;
	tempKartong.m_x = tempSF.x;
	tempKartong.m_y = tempSF.y;
	tempKartong.normalize();
	tempSF.x = tempKartong.m_x;
	tempSF.y = tempKartong.m_y;

	return tempSF;
}

Projectile::~Projectile()
{

	GameObject::~GameObject();
}

bool Projectile::Update()
{
	GameObject::Update(); //do first in update to get correct deltatime
	//m_Emitter->setPosition(sf::Vector2f(m_xPos.x + m_xSprite->getGlobalBounds().width / 2, m_xPos.y +m_xSprite->getGlobalBounds().height / 2));
	//m_Emitter->update();
	ChangePos((m_xVel*m_fDeltaTime*SPEED));
	return true;
}

void Projectile::UpdateBB()
{

}

void Projectile::DrawParticles()
{
	//m_Emitter->draw();
}


float Projectile::GetDamage()
{
	return m_fDamage;
}
