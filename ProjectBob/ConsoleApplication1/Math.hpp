#pragma once

#include "stdafx.h"

namespace kartong
{
		class Math
		{
		public:
			static const float PI;

			static float sqrt(float value);
			static float sin(float radian);
			static float cos(float radian);
			static float tan(float radian);
			static float to_rad(float degrees);
			static float to_deg(float radians);
			static float distance(sf::Vector2f p_A, sf::Vector2f p_B);

			template <typename T>
			static T max(T a, T b)
			{
				return a > b ? a : b;
			}

			template <typename T>
			static T min(T a, T b)
			{
				return a < b ? a : b;
			}

			template <typename T>
			static T abs(T a)
			{
				return a < 0 ? -a : a;
			}

			template <typename T>
			static T clamp(T v, T a, T b)
			{
				return v < a ? a : v > b ? b : v;
			}

			template <typename T>
			static bool are_equal(const T& a, const T& b) { return a == b; }
			template <>
			static bool are_equal<float>(const float& a, const float& b);
			template <>
			static bool are_equal<double>(const double& a, const double& b);

			static float dot(const sf::Vector2f& p_Vec1, const sf::Vector2f& p_Vec2);
			static sf::Vector2f normalize(const sf::Vector2f& p_Vec);
			static sf::Vector2f normalize(const sf::Vector2i& p_Vec);
			static float length(const sf::Vector2f& p_Vec);
			static float length(const sf::Vector2i& p_Vec);
			static sf::Vector2f getDirection(const sf::Vector2f& p_Point1, const sf::Vector2f& p_Point2);
		};
		class Vector2
		{
		public:
			Vector2();
			Vector2(float p_X, float p_Y);
			Vector2(const Vector2& p_Vec);

			Vector2& operator=(const Vector2& p_Vec);
			Vector2& operator+=(const Vector2& p_Vec);
			Vector2& operator-=(const Vector2& p_Vec);
			Vector2& operator*=(const float p_Val);
			Vector2& operator/=(const float p_Val);
			Vector2& operator+(const Vector2& p_Vec);
			Vector2& operator-(const Vector2& p_Vec);
			Vector2& operator*(const float p_Val);
			Vector2& operator/(const float p_Val);


			sf::Vector2f getSfVector2f() const;
			sf::Vector2i getSfVector2i() const;
			sf::Vector2u getSfVector2u() const;


			float length() const;
			float length_squared() const;
			float dot(const Vector2& p_Vec) const;
			void normalize();

			float m_x;
			float m_y;
		};
	}
