#pragma once

#include "stdafx.h"

namespace kartong
{
	enum StateRequirement
	{
		NEVER,
		FIRST,
		ALWAYS,
	};

	class State
	{
	public:
		virtual ~State(){};
		

		virtual bool Initialize() = 0;
		virtual bool Update() = 0;
		virtual void Draw() = 0;

		virtual void Exit() = 0;

		virtual std::string getName() = 0;
		virtual int getUpdateRequirement() = 0;
		virtual int getDrawRequirement() = 0;

	};
}