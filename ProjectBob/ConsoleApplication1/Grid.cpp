#include "stdafx.h"
#include "Grid.hpp"
#include "ServiceLocator.hpp"

using namespace kartong;
using namespace AIGame;

Node::Node(const sf::Vector2f p_Pos)
{
	m_Grid = ServiceLocator<Grid>::get_service();
	m_GridPos = m_Grid->getGridPos(p_Pos);
	m_Type = NODETYPE::EMPTY;
	m_Parent = nullptr;
	m_sOccupiedBy = "";
};
Node::~Node()
{

};
sf::Vector2i Node::getGridPosition()
{
	return m_GridPos;
};
int Node::getType()
{
	return m_Type;
};
void Node::setType(const int& p_Type)
{
	m_Type = p_Type;
};
void Node::setParent(Node* p_Parent)
{
	m_Parent = p_Parent;
};
Node* Node::getParent()
{
	return m_Parent;
};

/////////////

Tile::Tile(sf::Vector2f p_Pos){
	m_FontManager = ServiceLocator<FontManager>::get_service();
	m_Position = p_Pos;
	m_Node = new Node(m_Position);
	m_Text = new sf::Text(std::to_string(m_Node->getGridPosition().x) + ", " + std::to_string(m_Node->getGridPosition().y), *m_FontManager->loadFont("../Fonts/AdobeGothicStd-Bold.otf"), 10);
	m_Text->setColor(sf::Color::Red);
	m_Text->setPosition(sf::Vector2f(m_Position.x + 2, m_Position.y + 2));
};
Tile::~Tile(){
	delete m_Node;
	m_Node = nullptr;
	delete m_Text;
	m_Text = nullptr;
	m_FontManager = nullptr;
};

//////////////
///--------///
//////////////
Grid::Grid(const sf::Vector2i& p_GridSize, const float& p_TileSize)
{
	m_Window = ServiceLocator<Window>::get_service();
	m_FontManager = ServiceLocator<kartong::FontManager>::get_service();
	m_TileSize = p_TileSize;
	m_GridSize = p_GridSize;


};

Grid::~Grid()
{
	for (unsigned i = 0; i < m_Tiles.size(); i++){
		delete m_Tiles[i];
		m_Tiles[i] = nullptr;
	}
	delete m_TileRect;
	m_TileRect = nullptr;
};

Grid::Ptr Grid::Create(const sf::Vector2i& p_GridSize, const float& p_TileSize)
{
	return Grid::Ptr(new Grid(p_GridSize, p_TileSize));
};

void Grid::initialize()
{
	m_TileRect = new sf::RectangleShape();
	m_TileRect->setSize(sf::Vector2f(m_TileSize - 1, m_TileSize - 1));

	for (int i = 0; i < m_GridSize.y; i++)
	{
		for (int j = 0; j < m_GridSize.x; j++)
		{
			Tile* tile;
			tile = new Tile(sf::Vector2f(m_Position.x + i * m_TileSize, m_Position.y + j * m_TileSize));
			m_Tiles.push_back(tile);

		}
	}
};
void Grid::update()
{

};
void Grid::draw()
{
	for (unsigned i = 0; i < m_Tiles.size(); i++){
		m_TileRect->setPosition(m_Tiles[i]->m_Position);
		m_TileRect->setFillColor(getColor(m_Tiles[i]->m_Node->getType()));
		m_Window->getWindow()->draw(*m_TileRect);
		//m_Window->getWindow()->draw(*m_Tiles[i]->m_Text);
	}
};


sf::Color Grid::getColor(const int& p_Type)
{
	switch (p_Type)
	{
	case NODETYPE::EMPTY:
		return sf::Color(12, 12, 12, 230);
	case NODETYPE::WALL:
		return sf::Color::Black;
	case NODETYPE::INCOMINGHAZARD:
		return sf::Color(255, 255, 30, 150);
	case NODETYPE::HAZARDS:
		return sf::Color(255, 30, 30, 150);
	case NODETYPE::PATHTEST:
		return sf::Color(50, 50, 50, 100);
	case NODETYPE::OPENLIST:
		return sf::Color(0, 50, 0, 100);
	case NODETYPE::CLOSEDLIST:
		return sf::Color(50, 0, 0, 100);
	default:
		return sf::Color::Magenta;
	}
};

sf::Vector2i Grid::getGridPos(const sf::Vector2f& p_Pos)
{

	int x = (int)((p_Pos.x - m_Position.x) / m_TileSize);
	int y = (int)((p_Pos.y - m_Position.y) / m_TileSize);
	return sf::Vector2i(x, y);
};
sf::Vector2i Grid::getGridPos(const float& p_X, const float& p_Y)
{
	int x = (int)(p_X / m_TileSize);
	int y = (int)(p_Y / m_TileSize);
	return sf::Vector2i(x, y);
};
sf::Vector2f Grid::getPixelPos(const sf::Vector2i& p_Pos)
{
	return sf::Vector2f(p_Pos.x * m_TileSize + (m_TileSize / 2), p_Pos.y * m_TileSize + (m_TileSize / 2));
};
sf::Vector2f Grid::getPixelPos(const int& p_X, const int& p_Y)
{
	return sf::Vector2f(p_X * m_TileSize + (m_TileSize / 2), p_Y * m_TileSize + (m_TileSize / 2));
};
sf::Vector2f Grid::getPixelPos(Node* p_xNode)
{
	return sf::Vector2f(p_xNode->getGridPosition().x *m_TileSize, p_xNode->getGridPosition().y *m_TileSize);
}
sf::Vector2i Grid::getGridSize()
{
	return m_GridSize;
}
void Grid::setPosition(const sf::Vector2f& p_Pos)
{
	m_Position = p_Pos;
};
void Grid::setPosition(const float& p_X, const float& p_Y)
{
	m_Position = sf::Vector2f(p_X, p_Y);
};
void Grid::setType(const sf::Vector2i& p_Node, const int& p_Type)
{
	getTile(p_Node.x, p_Node.y)->m_Node->setType(p_Type);
};
void Grid::setType(const int& p_X, const int& p_Y, const int& p_Type)
{
	getTile(p_X, p_Y)->m_Node->setType(p_Type);
};
int Grid::getType(const sf::Vector2f& p_Tile)
{
	return getTile(p_Tile)->m_Node->getType();
};
int Grid::getType(const int& p_X, const int& p_Y)
{
	return getTile(p_X, p_Y)->m_Node->getType();
};
Tile* Grid::getTile(const sf::Vector2f& p_Tile)
{
	return m_Tiles[((unsigned int)p_Tile.x * m_GridSize.x) + (unsigned int)p_Tile.y];
};
Tile* Grid::getTile(const int& p_X, const int& p_Y)
{
	return m_Tiles[(p_X * m_GridSize.x) + p_Y];
};
Node* Grid::getNode(const sf::Vector2f& p_Tile)
{
	return getTile(p_Tile)->m_Node;
};
Node* Grid::getNode(const int& p_X, const int& p_Y)
{
	return getTile(p_X, p_Y)->m_Node;
};
bool Grid::nodeExists(const sf::Vector2i& p_Node)
{
	if (p_Node.x < 0 || p_Node.x > m_GridSize.x || p_Node.y < 0 || p_Node.y > m_GridSize.y)
		return false;
	return true;
};
bool Grid::nodeExists(const int& p_X, const int& p_Y)
{
	if (p_X < 0 || p_X >= m_GridSize.y || p_Y < 0 || p_Y >= m_GridSize.x)
		return false;
	return true;
};

void Grid::RemoveAllParents()
{
	auto itr = m_Tiles.begin();
	while (itr != m_Tiles.end())
	{
		(*itr)->m_Node->setParent(nullptr);
		++itr;
	}

	/*auto closeditr = m_ClosedList.begin();
	while (closeditr != m_ClosedList.end())
	{
		(*closeditr)->setParent(nullptr);
		++closeditr;
	}

	auto openitr = m_OpenList.begin();
	while (openitr != m_OpenList.end())
	{
		(*openitr)->setParent(nullptr);
		++openitr;
	}*/

	m_OpenList.clear();
	m_ClosedList.clear();
}

//A*
std::vector<Node*> Grid::getPath(Node* p_Start, Node* p_Goal)
{
	RemoveAllParents();

	m_Goal = nullptr;
	m_OpenList.push_back(p_Start);
	Node* currNode;
	while (!m_OpenList.empty()){
		currNode = getCheapestNode(m_OpenList);
		std::vector<Node*> neighbours = getNeighbours(currNode);
		removeNodeFromList(m_OpenList, currNode);
		for (unsigned i = 0; i < neighbours.size(); i++){
			if (neighbours[i] == p_Goal){
				m_Goal = neighbours[i];
				goto goalfound;
			}
			
			if (neighbours[i]->getType() == NODETYPE::WALL) {
				continue;
			}

			neighbours[i]->g = currNode->g + getNodeCost(neighbours[i], currNode);
			neighbours[i]->h = getEstimatedDistance(neighbours[i], p_Goal);
			neighbours[i]->f = neighbours[i]->g + neighbours[i]->h;

			/*Debug::write(EDebugLevel::INFO, std::to_string(neighbours[i]->getGridPosition().x) + ", " + std::to_string(neighbours[i]->getGridPosition().y));
			Debug::write(EDebugLevel::INFO, "G: " + std::to_string(neighbours[i]->g));
			Debug::write(EDebugLevel::INFO, "H: " + std::to_string(neighbours[i]->h));
			Debug::write(EDebugLevel::INFO, "F: " + std::to_string(neighbours[i]->f));*/

		}
		for (unsigned i = 0; i < neighbours.size(); i++){

			if (neighbours[i]->getType() == NODETYPE::WALL) {
				continue;
			}

			if (!cheaperFound(m_OpenList, neighbours[i]->f) && !cheaperFound(m_ClosedList, neighbours[i]->f))
			{
				if (neighbours[i] != m_Goal){
					m_OpenList.push_back(neighbours[i]);
					//neighbours[i]->setType(NODETYPE::OPENLIST);
				}
			}
		}
		m_ClosedList.push_back(currNode);
		//currNode->setType(NODETYPE::CLOSEDLIST);
		for (unsigned i = 0; i < neighbours.size(); i++){
			neighbours[i] = nullptr;
		}
		neighbours.clear();
	}
goalfound:
	m_Path.clear();
	if (m_Goal){
		Node* iter = m_Goal;
		auto pathIter = m_Path.begin();
		bool broken = false;
		while (true){
			//iter->setType(NODETYPE::PATHTEST);
			pathIter = m_Path.begin();
			while (pathIter != m_Path.end())
			{
				if (iter == (*pathIter))
				{
					broken = true;
					break;
				}
				++pathIter;
			}

			if (iter == p_Start)
			{
				break;
			}

			m_Path.push_back(iter);

			if (iter->getParent() && !broken){
				iter = iter->getParent();
			}
			else
				break;

			//if (iter->getParent())
			//{
			//	if (iter->getParent()->getParent())
			//	{
			//		if (iter == iter->getParent()->getParent()){
			//			break;
			//		}
			//		if (iter->getParent()->getParent()->getParent())
			//		{
			//			if (iter == iter->getParent()->getParent()->getParent())
			//				break;
			//		}
			//	}
			//}

		}
		iter = nullptr;
	}
	/*for (unsigned i = 0; i < m_Path.size(); i++){
		Debug::write(EDebugLevel::INFO, std::to_string(m_Path[i]->getGridPosition().x) + ", " + std::to_string(m_Path[i]->getGridPosition().y));
	}*/
	return m_Path;
};

std::vector<Node*> Grid::getNeighbours(Node* p_Node)
{
	//int i = 0;
	std::vector<Node*> neighbours;
	sf::Vector2i parentPos = p_Node->getGridPosition();
	for (int i = -1; i < 2; i++){
		for (int j = -1; j < 2; j++){
			//int temp = i;
			//i = j;
			//j = temp;
			if (i == 0 && j == 0){
				continue;
			};

			if (nodeExists(p_Node->getGridPosition().x + i, p_Node->getGridPosition().y + j)){
				neighbours.push_back(getNode(parentPos.x + i, parentPos.y + j));
				if (!neighbours[neighbours.size() - 1]->getParent())
					neighbours[neighbours.size() - 1]->setParent(p_Node);
			}
		}
	}
	return neighbours;
};

Node* Grid::getCheapestNode(std::vector<Node*> p_List)
{
	Node* cheapest = p_List[0];
	for (unsigned i = 0; i < p_List.size(); i++){
		if (p_List[i]->f < cheapest->f)
			cheapest = p_List[i];
	}
	return cheapest;
};

float Grid::getNodeCost(Node* p_Node, Node* p_Start)
{
	float nodecost = 0;
	if (p_Node->getGridPosition().x - p_Start->getGridPosition().x == 0 || p_Node->getGridPosition().y - p_Start->getGridPosition().y == 0)
		nodecost = 1.4f;
	else
		nodecost = 1.0f;
	return nodecost * getTypeCost(p_Node->getType());
};

bool Grid::cheaperFound(std::vector<Node*> p_List, const float& p_F)
{
	for (unsigned i = 0; i < p_List.size(); i++){
		if (p_List[i]->f < p_F)
			return true;
	}
	return false;
};

void Grid::removeNodeFromList(std::vector<Node*> &p_List, Node* p_Node)
{
	for (unsigned i = 0; i < p_List.size(); i++){
		if (p_List[i] == p_Node){
			p_List.erase(p_List.begin() + i);
			return;
		}
	}
	Debug::write(EDebugLevel::ERROR, "Error: Node removal failed");
};

float Grid::getTypeCost(const int& p_Type)
{
	switch (p_Type)
	{
	case NODETYPE::EMPTY:
		return 10;
	case NODETYPE::WALL:
		return std::numeric_limits<float>::max();
	case NODETYPE::INCOMINGHAZARD:
		return 20;
	case NODETYPE::HAZARDS:
		return 30;
		/*case NODETYPE::OCCUPIED:
			return 20;*/
	case NODETYPE::PATHTEST:
		return 10;
	case NODETYPE::OPENLIST:
		return 10;
	case NODETYPE::CLOSEDLIST:
		return 10;
	default:
		return 10;
	}
};
float Grid::getEstimatedDistance(Node* p_Node, Node* p_Goal)
{
	return((Math::abs(p_Node->getGridPosition().x - p_Goal->getGridPosition().x) + Math::abs(p_Node->getGridPosition().y - p_Goal->getGridPosition().y))) * m_TileSize;
};
float Grid::GetTileSize()
{
	return m_TileSize;
}

bool directPathAvaliable(const sf::Vector2f& p_Start, const sf::Vector2f& p_Goal)
{

	return false;
};

bool rayIntersected(const sf::Vector2f& p_Start, const sf::Vector2f& p_Direction, const sf::Vector2i& p_Tile)
{
	sf::Vector2f vec;
	vec.x = p_Tile.x - p_Start.x;
	vec.x = p_Tile.y - p_Start.y;
	float fD = Math::dot(p_Direction, vec);
	if (fD < 0.0f)
		return false;
	sf::Vector2f closestPoint;
	closestPoint.x = p_Start.x + (p_Direction.x * fD);
	closestPoint.y = p_Start.y + (p_Direction.y * fD);
	return false;
};