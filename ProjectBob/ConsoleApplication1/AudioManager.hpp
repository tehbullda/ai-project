#pragma once

#include "AudioListener.hpp"

#define DEFAULT_VOLUME 1

namespace kartong
{
	enum AudioType
	{
		MASTER,
		EFFECTS,
		AMBIENT,
		BACKGROUND,
		SPEECH
	};
	class AudioManager
	{
	private:
		AudioManager(const std::string& p_Directory);
	public:
		~AudioManager();

		typedef std::unique_ptr<AudioManager> Ptr;
		static Ptr Create(const std::string& p_Directory);

		void setAudioListener(AudioListener* p_Listener);

		void preLoadSound(const std::string& p_File);
		void playClip(const std::string& p_Clip);
		void playClipAtPoint(const std::string& p_Clip, const sf::Vector2f& p_Pos);

		void BGM_attach(const std::string& p_Path, const int& p_Identifier, const bool& p_Looping);
		void BGM_play(int& p_Identifier);
		void BGM_stop(int& p_Identifier);
		void BGM_pause(int& p_Identifier);
		void BGM_reset(int& p_Identifier);
		bool BGM_isPlaying(int& p_Identifier);


		void stopAll();
	private:
		bool loadVolume(const std::string& p_File);
		void saveVolume(const std::string& p_File);

		std::map<int, sf::SoundStream*> m_BGM;
		sf::SoundSource* m_Source;
		AudioListener* m_Listener;
		std::map<std::string, sf::Sound*> m_Sounds;
		std::map<std::string, sf::SoundBuffer*> m_Buffers;
		std::string m_Directory;

		struct Volumes
		{
			float m_Master;
			float m_Effects;
			float m_Ambient;
			float m_Background;
			float m_Speech;
		} m_Volume;

	};


}