#include "stdafx.h"
#include "Particle.hpp"
#include "ServiceLocator.hpp"

using namespace kartong;

Particle::Particle(const std::string& p_File, const float& p_LifeTime, const float& p_MoveSpeed, const sf::Vector2f& p_Pos,
	const sf::Color& p_Color, const sf::Vector2f& p_Direction, const float& p_StartRotation, const float& p_RotationSpeed)
{
	m_TextureManager = ServiceLocator<TextureManager>::get_service();
	m_Stats.m_Sprite = new Sprite(m_TextureManager->loadTexture(p_File));
	m_Stats.m_LifeTime = p_LifeTime;
	m_Stats.m_MoveSpeed = p_MoveSpeed;
	m_Stats.m_Position = p_Pos;
	m_Stats.m_Color = p_Color;
	m_Stats.m_Direction = p_Direction;
	m_Stats.m_Rotation = p_StartRotation;
	m_Stats.m_RotationSpeed = p_RotationSpeed;

	m_Stats.m_Sprite->setPosition(m_Stats.m_Position);
	m_Stats.m_Sprite->setColor(m_Stats.m_Color);
	m_Stats.m_Sprite->setRotation(m_Stats.m_Rotation);


	m_Stats.m_Alpha = m_Stats.m_Color.a;
	m_Stats.m_EndAlpha = 0.0f;
	m_Stats.m_StartFadePercent = 0.9f;
	m_Stats.m_FadeSpeed = 300.0f;

	m_Stats.m_WhenStartFade = m_Stats.m_LifeTime * m_Stats.m_StartFadePercent;

};
Particle::~Particle()
{
	delete m_Stats.m_Sprite;
	m_Stats.m_Sprite = nullptr;
};
bool Particle::update()
{
	m_Stats.m_LifeTime -= DeltaTime::getDeltaTime();
	if (m_Stats.m_LifeTime < 0.0f){
		return false;
	}
	//Pos
	sf::Vector2f pos = m_Stats.m_Sprite->getPosition();
	m_Stats.m_Sprite->setPosition(pos +=
		m_Stats.m_MoveSpeed * m_Stats.m_Direction * DeltaTime::getDeltaTime());
	//Fade
	if (m_Stats.m_WhenStartFade > m_Stats.m_LifeTime){
		m_Stats.m_Alpha -= m_Stats.m_FadeSpeed * DeltaTime::getDeltaTime();
		if (m_Stats.m_Alpha < m_Stats.m_EndAlpha)
			m_Stats.m_Alpha = m_Stats.m_EndAlpha;
		m_Stats.m_Color.a = (sf::Uint8)m_Stats.m_Alpha;
		m_Stats.m_Sprite->setColor(m_Stats.m_Color);
	}
	//Rotation
	m_Stats.m_Rotation += m_Stats.m_RotationSpeed * DeltaTime::getDeltaTime();
	m_Stats.m_Sprite->setRotation(m_Stats.m_Rotation);

	return true;
};
void Particle::draw()
{
	m_Stats.m_Sprite->draw();
};

