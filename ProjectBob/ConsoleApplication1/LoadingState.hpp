#pragma once

#include "stdafx.h"
#include "TextureManager.hpp"
#include "FontManager.hpp"





namespace AIGame
{
	class LoadingState : public kartong::State
	{
	public:
		LoadingState(const std::string& p_Name);
		~LoadingState();


		bool Initialize();
		bool Update();
		void Draw();
		void Exit();


		std::string getName();
		int getUpdateRequirement();
		int getDrawRequirement();

	private:
		void handleStates();
	private:
		std::string m_Name;
		int m_DrawRequitement;
		int m_UpdateRequitement;
	private:
		kartong::StateManager* m_StateManager;
		kartong::TextureManager* m_TextureManager;
		kartong::FontManager* m_FontManager;


	private:

	};
}