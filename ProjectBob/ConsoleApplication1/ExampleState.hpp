#pragma once

#include "stdafx.h"
#include "TextureManager.hpp"
#include "Sprite.hpp"
#include "ParticleEmitter.hpp"
#include "Input.hpp"
#include "PremadeButton.hpp"
namespace kartong
{
	class ExampleState : public State
	{
	public:
		ExampleState(const std::string& p_Name);
		~ExampleState();


		bool Initialize();
		bool Update();
		void Draw();

		void Exit();

		std::string getName();
		int getUpdateRequirement();
		int getDrawRequirement();

	private:
		std::string m_Name;
		int m_DrawRequitement;
		int m_UpdateRequitement;
	private:
		TextureManager* m_TextureManager;
		FontManager* m_FontManager;
		system::input::Input* m_Input;
		Window* m_Window;
	private:

		int test;
		Sprite* m_TestOwn;
		ParticleEmitter* m_TestEmitter;
		Premade::PremadeButton* m_TestButton;
		sf::Text* m_TestText;
	};
}