#pragma once

#include "stdafx.h"
#include "Sprite.hpp"
#include "TextureManager.hpp"
#include "AudioManager.hpp"
#include "Window.hpp"
#include "Input.hpp"
namespace kartong
{
	namespace Premade
	{
		enum ButtonState
		{
			IDLE,
			HOVER,
			HELD,
			CLICKED
		};

		class PremadeButton
		{
		public:
			PremadeButton(sf::Texture* p_Texture, const std::string p_Identifier = "Default");
			~PremadeButton();

			bool isHovering();
			bool isClicked();
			bool isHeld();

			void update();
			void draw();

			void setPosition(const sf::Vector2f& p_Pos);
			void setPosition(const float& p_X, const float& p_Y); 
			sf::Vector2f getPosition();
			std::string getIdentifer();
			sf::Vector2f getSize();

		private:
			void updateRect();
		private:
			TextureManager* m_TextureManager;
			AudioManager* m_AudioManager;
			system::input::Input* m_Input;

			Sprite* m_Sprite;
			sf::IntRect m_Rect;
			float m_Width;
		private:
			int m_CurrentState;
			sf::Vector2f m_Position;
			std::string m_Identifer;

		};
	}
}