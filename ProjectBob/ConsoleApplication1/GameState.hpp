#pragma once

#include "stdafx.h"
#include "TextureManager.hpp"
#include "Sprite.hpp"
#include "ParticleEmitter.hpp"
#include "Input.hpp"
#include "TextChat.hpp"
#include "Grid.hpp"
#include "GameObjectManager.hpp"
#include "AuraManager.hpp"
#include "BlackBoard.hpp"

//
#include "GraphWindow.hpp"
#include "Boss.hpp"
#include "NameHandler.hpp"
#include "PremadeSlider.hpp"
#include "PremadeTickbox.hpp"
#include"StateTransition.hpp"
#include "ParticleEmitter.hpp"
#include "ParticleManager.hpp"
#include "AudioManager.hpp"
#include "AudioListener.hpp"

namespace AIGame
{
	class GameState : public kartong::State
	{
	public:
		GameState(const std::string& p_Name);
		~GameState();


		bool Initialize();
		bool Update();
		void Draw();
		void Exit();

		bool malinTest();
		bool labanTest();
		bool willeTest();


		std::string getName();
		int getUpdateRequirement();
		int getDrawRequirement();

	private:
		void handleStates();
	private:
		std::string m_Name;
		int m_DrawRequirement;
		int m_UpdateRequirement;
	private:
		AIGame::GameObjectManager::Ptr m_xGameObjectManager;
		BlackBoard::Ptr m_xBlackBoard;
		
		AuraManager *m_AuraManager;

		kartong::StateManager* m_StateManager;
		kartong::TextureManager* m_TextureManager;
		kartong::FontManager* m_FontManager;
		kartong::system::input::Input* m_Input;
		kartong::Window* m_Window;
		
		

		int bob;
	private:
		TextChat::Ptr m_TextChat;
		kartong::Sprite* m_Backgroundtest;
		Grid::Ptr m_Grid;
		NameHandler::Ptr m_NameHandler;
		kartong::Premade::PremadeSlider* m_SliderTest;
		kartong::Premade::PremadeTickbox* m_TickBoxTest;

		StateTransition::Ptr m_StateTrans;
		sf::View* m_View;
		kartong::ParticleEmitter* m_Emitter;
		float m_GameTimer;

		kartong::AudioManager* m_AudioManager;
		kartong::AudioListener* m_Listener;
		
	private:

	};
}