#include "stdafx.h"
#pragma once

namespace AIGame
{
	class PostGameStats
	{
	private:
		PostGameStats();
	public:
		~PostGameStats();
		typedef std::unique_ptr<PostGameStats> Ptr;
		static Ptr Create();

		void reset();

		void reportDeath(const std::string& p_Name, const float& p_TimeAlive);
		void reportDamageTaken(const std::string& p_Name, const float& p_Val);
		void reportDamageGiven(const std::string& p_Name, const float& p_Val);
		void reportDistanceWalked(const std::string& p_Name, const float& p_Val);
		void reportProjectilesFired(const std::string& p_Name, const float& p_Val);


		int getDeathNum();
		int getTotalDamageTaken();
		int getTotalDamageGiven();
		float getTotalLifetime();
		int getTotalDistanceWalked();
		int getTotalProjectilesFired();

		std::pair<std::string, int> getHighestAgentDamageGiven();
		std::pair<std::string, int> getHighestAgentDamageTaken();
		std::pair<std::string, float> getHighestAgentLifetime();
		std::pair<std::string, float> getLowestAgentLifetime();
		std::pair<std::string, int> getHighestAgentProjectilesFired();

		void reportSmashCast();
		void reportCleaveCast();
		void reportPlagueCast();
		void reportSpreadLavaCast();
		void reportBabaamCast();
		void reportJumpAwayCast();

		int getTotalAbilitiesCasted();
		int getSmashCast();
		int getCleaveCast();
		int getPlagueCast();
		int getBabaamCast();
		int getSpreadLavaCast();
		int getJumpAwayCast();

		void reportPlayTime(const float& p_Time);
		float getPlayTime();
		void setWon(const bool& p_Won);
		bool haveWon();

	private:
		void reportLifeTime(const std::string& p_Name, const float& p_Val);

		std::map<std::string, int> m_DeathCounts;
		std::map<std::string, float> m_Lifetimes;
		std::map<std::string, int> m_DamageTaken;
		std::map<std::string, int> m_DamageGiven;
		std::map<std::string, int> m_ProjectilesFired;
		std::map<std::string, int> m_DistanceWalked;

		float m_PlayTime;

		
		struct  Boss
		{
			int SmashCasted;
			int PlagueCasted;
			int BabaamCasted;
			int SpreadLavaCasted;
			int JumpAwayCasted;
			int CleaveCasted;;

		} m_BossStats;

		bool m_Won;
	};
}