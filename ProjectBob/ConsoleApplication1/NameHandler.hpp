#pragma once

#include "stdafx.h"
#include <fstream>
#include <sstream>

namespace AIGame
{
	class NameTag
	{
	public:
		NameTag(const std::string& p_Name);
		~NameTag();

		void setPosition(const sf::Vector2f& p_Pos);
		std::string getName();

		void update();
		void draw();

		void updateHP(const float& p_Max, const float& p_Curr);

		float test;
		bool m_done;
		
	private:
		bool m_Dirty;
		std::string m_Name;
		kartong::Window* m_Window;
		sf::RectangleShape m_Back;
		sf::Text* m_Text;
		sf::Vector2f m_Position;
	private:
		float m_MaxHP;
		float m_CurrentHP;
		sf::RectangleShape m_HPBack;
		sf::RectangleShape m_HPBar;
	};

	class BossNameTag
	{
	public:
		BossNameTag();
		~BossNameTag();

		void draw();

		void updateHP(const float& p_Max, const float& p_Curr);
		

	private:
		std::string m_Name;
		kartong::Window* m_Window;
		sf::RectangleShape m_Back;
		sf::Text* m_Text;
		sf::Vector2f m_Position;

		float m_MaxHP;
		float m_CurrentHP;
		sf::RectangleShape m_HPBack;
		sf::RectangleShape m_HPBar;
	};

	class NameHandler
	{
	private:
		NameHandler();
	public:
		~NameHandler();
		typedef std::unique_ptr<NameHandler> Ptr;
		static Ptr Create();

		std::string requestName();
		NameTag* getNameTag(const std::string& p_Name);
		NameTag* getNameTag();
		BossNameTag* getBossNameTag();

		void updateTags();
		void drawTags();
	private:
		int getAvailableNames();

		std::vector<std::string> m_Names;
		std::vector<NameTag*> m_Tags;
		BossNameTag* m_BossTag;
	};
}