#pragma once
#include "stdafx.h"

namespace AIGame
{
	class StateTransition
	{
	private:
		StateTransition();
	public:
		~StateTransition();

		typedef std::unique_ptr<StateTransition> Ptr;
		static Ptr Create();

		void reset();
		void startTransition();
		void update();
		void draw();

		bool isDone();
		bool isRunning();
	private:
		sf::Clock* m_Timer;
		sf::RectangleShape* m_Screencolor;
		bool m_Running;
		bool m_Paused;
		bool m_Done;
		float m_Fade;
		float m_Whitefade;

	};
}