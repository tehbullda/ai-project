#include "stdafx.h"
#include "Text.hpp"
namespace AIGame {
	class Tooltip : public Text {
	public:
		Tooltip(const std::string &text, const sf::Vector2f &pos = sf::Vector2f(0, 0), const uint32_t &charactersize = 12, const sf::Color fillcolor = sf::Color::Black, const bool &followsmouse = false);
		~Tooltip();

		void SetText(const std::string &text);

		void Update();

		void SetTriggerArea(const sf::IntRect area);
		void SetMaxRowlength(const uint32_t &pixels);
		void SetVisibility(const bool &status);
		void SetMouseFollow(const bool &status);
		void SetRectFillColor(const sf::Color &color);
		void SetRectOutlineColor(const sf::Color &color);
		void SetRectOutlineThickness(const float &thickness);

		bool GetVisibility();
		bool GetMouseFollow();

		void Draw();

	private:
		void AdjustText();
		void UpdateRect();
	private:
		sf::RectangleShape m_boundingrect;
		sf::IntRect m_triggerarea;
		uint32_t m_maxrowlength;
		bool m_followsmouse;
		bool m_visible;
	};
}