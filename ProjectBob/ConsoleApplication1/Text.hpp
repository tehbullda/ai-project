#include "stdafx.h"

namespace AIGame {
	class Text {
	public:
		Text();
		Text(const std::string &text, const sf::Vector2f &pos, const uint32_t &charactersize = 12);
		~Text();

		void SetString(const std::string &text);
		void SetPosition(const sf::Vector2f &pos);
		void SetCharacterSize(const uint32_t &size);
		void SetTextColor(const sf::Color &color);

		void AdjustTextsize(const int &adjustment);

		sf::Vector2f GetPosition();
		sf::FloatRect GetRect();

		void Draw();

	protected:
		sf::Text m_text;
		//kartong::FontManager* m_font_manager; //don't need this, servicelocator too stronk
	};
}