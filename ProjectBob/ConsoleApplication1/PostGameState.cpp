#include "stdafx.h"

#include "GameState.hpp"
#include "PostGameState.hpp"
#include "ServiceLocator.hpp"
#include "Randomizer.hpp"

#define FONT "tahoma.ttf"
#define FONTBOLD "tahomabd.ttf"
#define PADDING 27
#define TOPPADDING 50
#define TEXTSIZE 24

using namespace kartong;
using namespace AIGame;

PostGameState::PostGameState(const std::string& p_Name)
{
	m_PostStats = ServiceLocator<AIGame::PostGameStats>::get_service();
	m_StateManager = ServiceLocator<kartong::StateManager>::get_service();
	m_TextureManager = ServiceLocator<TextureManager>::get_service();
	m_FontManager = ServiceLocator<FontManager>::get_service();
	m_Input = ServiceLocator<system::input::Input>::get_service();
	m_Window = ServiceLocator<Window>::get_service();
	m_Name = p_Name;
	m_UpdateRequirement = StateRequirement::FIRST;
	m_DrawRequirement = StateRequirement::ALWAYS;
	//////////
};
PostGameState::~PostGameState()
{
	Exit();
};

bool PostGameState::Initialize()
{
	m_MenuButton = new kartong::Premade::PremadeButton(m_TextureManager->loadTexture("GUIHUD/Menu.png"));
	m_MenuButton->setPosition(sf::Vector2f(m_Window->getSize().x - m_MenuButton->getSize().x - TOPPADDING, m_Window->getSize().y - m_MenuButton->getSize().y - TOPPADDING));


	

	if (m_PostStats->haveWon())
		m_Texts.push_back(new sf::Text("Agents Won! :D", *m_FontManager->loadFont(FONTBOLD), TEXTSIZE + 5));
	else
		m_Texts.push_back(new sf::Text("Agents Lost :( ", *m_FontManager->loadFont(FONTBOLD), TEXTSIZE + 5));

	m_Texts.push_back(new sf::Text(" ", *m_FontManager->loadFont(FONT), TEXTSIZE));

	m_Texts.push_back(new sf::Text(std::to_string((int)m_PostStats->getPlayTime()) + "s", *m_FontManager->loadFont(FONTBOLD), TEXTSIZE + 5));

	m_Texts.push_back(new sf::Text(" ", *m_FontManager->loadFont(FONT), TEXTSIZE));
	m_Texts.push_back(new sf::Text("Total Agents: " + std::to_string(m_PostStats->getDeathNum()), *m_FontManager->loadFont(FONT), TEXTSIZE));
	m_Texts.push_back(new sf::Text("Total Damage Dealt: " + std::to_string(m_PostStats->getTotalDamageGiven()), *m_FontManager->loadFont(FONT), TEXTSIZE));
	m_Texts.push_back(new sf::Text("Total Damage Taken: " + std::to_string(m_PostStats->getTotalDamageTaken()), *m_FontManager->loadFont(FONT), TEXTSIZE));
	m_Texts.push_back(new sf::Text("Total Projectiles Fired: " + std::to_string(m_PostStats->getTotalProjectilesFired()), *m_FontManager->loadFont(FONT), TEXTSIZE));
	m_Texts.push_back(new sf::Text("~~~~~~", *m_FontManager->loadFont(FONT), TEXTSIZE));
	m_Texts.push_back(new sf::Text("Notable Agents:", *m_FontManager->loadFont(FONT), TEXTSIZE));
	m_Texts.push_back(new sf::Text("Most Damage Dealt: " + m_PostStats->getHighestAgentDamageGiven().first + ", " + std::to_string(m_PostStats->getHighestAgentDamageGiven().second), *m_FontManager->loadFont(FONT), TEXTSIZE));
	m_Texts.push_back(new sf::Text("Most Damage Taken: " + m_PostStats->getHighestAgentDamageTaken().first + ", " + std::to_string(m_PostStats->getHighestAgentDamageTaken().second), *m_FontManager->loadFont(FONT), TEXTSIZE));
	m_Texts.push_back(new sf::Text("Longest Lifetime: " + m_PostStats->getHighestAgentLifetime().first + ", " + std::to_string((int)m_PostStats->getHighestAgentLifetime().second) + "s", *m_FontManager->loadFont(FONT), TEXTSIZE));
	m_Texts.push_back(new sf::Text("Shortest Lifetime: " + m_PostStats->getLowestAgentLifetime().first + ", " + std::to_string((int)m_PostStats->getLowestAgentLifetime().second) + "s", *m_FontManager->loadFont(FONT), TEXTSIZE));
	m_Texts.push_back(new sf::Text("Triggerhappy: " + m_PostStats->getHighestAgentProjectilesFired().first + ", " + std::to_string(m_PostStats->getHighestAgentProjectilesFired().second), *m_FontManager->loadFont(FONT), TEXTSIZE));
	m_Texts.push_back(new sf::Text("~~~~~~", *m_FontManager->loadFont(FONT), TEXTSIZE));
	m_Texts.push_back(new sf::Text("BossStats", *m_FontManager->loadFont(FONT), TEXTSIZE));
	m_Texts.push_back(new sf::Text("Abilities Cast: " + std::to_string(m_PostStats->getTotalAbilitiesCasted()), *m_FontManager->loadFont(FONT), TEXTSIZE));
	m_Texts.push_back(new sf::Text("Smash Casted: " + std::to_string(m_PostStats->getSmashCast()), *m_FontManager->loadFont(FONT), TEXTSIZE));
	m_Texts.push_back(new sf::Text("Cleave Casted: " + std::to_string(m_PostStats->getCleaveCast()), *m_FontManager->loadFont(FONT), TEXTSIZE));
	m_Texts.push_back(new sf::Text("Babaam Casted: " + std::to_string(m_PostStats->getBabaamCast()), *m_FontManager->loadFont(FONT), TEXTSIZE));
	m_Texts.push_back(new sf::Text("Plague Casted: " + std::to_string(m_PostStats->getPlagueCast()), *m_FontManager->loadFont(FONT), TEXTSIZE));
	m_Texts.push_back(new sf::Text("SpreadLava Casted: " + std::to_string(m_PostStats->getSpreadLavaCast()), *m_FontManager->loadFont(FONT), TEXTSIZE));
	m_Texts.push_back(new sf::Text("JumpAway Casted: " + std::to_string(m_PostStats->getJumpAwayCast()), *m_FontManager->loadFont(FONT), TEXTSIZE));

	for (unsigned i = 0; i < m_Texts.size(); i++){
		m_Texts[i]->setOrigin(sf::Vector2f(m_Texts[i]->getGlobalBounds().width / 2, m_Texts[i]->getGlobalBounds().height / 2));
		m_Texts[i]->setPosition(sf::Vector2f(m_Window->getSize().x / 2, (float)(TOPPADDING + (PADDING * i))));
	}
	if (m_PostStats->haveWon())
	m_Texts[0]->setColor(sf::Color::Green);
	else
	m_Texts[0]->setColor(sf::Color::Red);


	m_PostStats->reset();
	return true;
};
bool PostGameState::Update()
{


	handleStates();
	return true;
};
void PostGameState::Draw()
{
	for (unsigned i = 0; i < m_Texts.size(); i++){
		m_Window->getWindow()->draw(*m_Texts[i]);
	}
	m_MenuButton->draw();
};

void PostGameState::Exit()
{
	for (unsigned i = 0; i < m_Texts.size(); i++){
		delete m_Texts[i];
		m_Texts[i] = nullptr;
	}
	m_Texts.clear();
	delete m_MenuButton;
	m_MenuButton = nullptr;
};

std::string PostGameState::getName()
{
	return m_Name;
};
int PostGameState::getUpdateRequirement()
{
	return m_UpdateRequirement;
};
int PostGameState::getDrawRequirement()
{
	return m_DrawRequirement;
};

void PostGameState::handleStates()
{
	m_MenuButton->update();
	if (m_MenuButton->isClicked()){
		m_StateManager->activate("MenuState");
		m_StateManager->deactivate(m_Name);
	}
};

