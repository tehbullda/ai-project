#pragma once
#include "stdafx.h"
#include "GraphWindow.hpp"
#include "ServiceLocator.hpp"
#include "Randomizer.hpp"
using namespace kartong;


GraphWindow::GraphWindow()
{
	m_Window = ServiceLocator<Window>::get_service();
	m_FontManager = ServiceLocator<FontManager>::get_service();
	m_Input = ServiceLocator<system::input::Input>::get_service();

	m_Low = new sf::Text("0", *m_FontManager->loadFont("AdobeGothicStd-Bold.otf"), 10);
	m_High = new sf::Text("60", *m_FontManager->loadFont("AdobeGothicStd-Bold.otf"), 10);
	m_Low->setColor(sf::Color::Black);
	m_High->setColor(sf::Color::Black);

	m_Background = new sf::RectangleShape();
	m_GraphBack = new sf::RectangleShape();
	m_Point = new sf::RectangleShape();
	m_Point->setSize(sf::Vector2f(2, 2));
	m_Point->setFillColor(sf::Color::Black);
	m_Size = sf::Vector2f(150, 80);
	m_Background->setSize(m_Size);
	m_Background->setFillColor(sf::Color(255, 255, 255, 150));
	m_Background->setOutlineThickness(1);
	m_Background->setOutlineColor(sf::Color::Black);

	m_Padding = 10;
	m_GraphBack->setSize(sf::Vector2f(m_Size.x - m_Padding * 2, m_Size.y - m_Padding * 2));
	m_GraphBack->setFillColor(sf::Color(255, 255, 255, 200));
	

};

GraphWindow::~GraphWindow()
{
	delete m_Background;
	m_Background = nullptr;
	delete m_GraphBack;
	m_GraphBack = nullptr;
	delete m_Point;
	m_Point = nullptr;
	delete m_High;
	m_High = nullptr;
	delete m_Low;
	m_Low = nullptr;
};

GraphWindow::Ptr GraphWindow::Create()
{
	return GraphWindow::Ptr(new GraphWindow());
};

void GraphWindow::setPosition(const sf::Vector2f& p_Pos)
{
	m_Position = p_Pos;
	m_Background->setPosition(p_Pos);
	m_GraphBack->setPosition(sf::Vector2f(p_Pos.x + m_Padding, p_Pos.y + m_Padding));
	m_High->setPosition(sf::Vector2f(p_Pos.x + 1, p_Pos.y));
	m_Low->setPosition(sf::Vector2f(p_Pos.x + 1, p_Pos.y + 69));
};


void GraphWindow::update()
{
	if (m_Input->getKey(sf::Keyboard::Space))
		temptester += 1;
	if (m_Input->getKey(sf::Keyboard::Return))
		temptester -= 1;

	//m_Timer += DeltaTime::getDeltaTime();
	//if (m_Timer > m_RefreshRate){
	//	addValue(temptester);
	//	m_Timer = 0.0f;
	//}
};
void GraphWindow::draw()
{
	m_Window->getWindow()->draw(*m_Background);
	m_Window->getWindow()->draw(*m_GraphBack);
	m_Window->getWindow()->draw(*m_High);
	m_Window->getWindow()->draw(*m_Low);
	drawGraph();
};
void GraphWindow::drawGraph()
{



	for (unsigned i = 0; i < m_DotPositions.size(); i++){
		m_Point->setPosition(getVecPos(m_DotPositions[i], m_Highest, (float)i));
		m_Window->getWindow()->draw(*m_Point);
	}
};
void GraphWindow::updatePositions()
{


};
void GraphWindow::addValue(const float& p_Val)
{


	m_Highest = (float)retroGetHighest();
	m_High->setString(std::to_string((int)m_Highest));

	if (m_DotPositions.size() > (m_Size.x / 2) - 11){
			
		m_DotPositions.erase(m_DotPositions.begin());

	}
	m_DotPositions.push_back(p_Val);
};

sf::Vector2f GraphWindow::getVecPos(const float& p_Val, const float& p_Max, const float& p_Iter)
{
	float rel = p_Val / p_Max;
	sf::Vector2f pos;
	if (rel < 0.0f)
		rel = 0.0f;
	pos.x = m_Position.x + (p_Iter * 2) + m_Padding;
	pos.y = m_Position.y - (m_Size.y - m_Padding*2) * rel + m_Size.y - m_Padding;
	return pos;
};

int GraphWindow::retroGetHighest(){
	int ret = 0;
	for (unsigned i = 0; i < m_DotPositions.size(); i++){
		if (ret < m_DotPositions[i])
			ret = (int)m_DotPositions[i];
	}
	return ret;
};

sf::Vector2f GraphWindow::getSize()
{
	return m_Size;
};



