#pragma once

#include "stdafx.h"
#include "BoundingUnits.hpp"

namespace kartong
{
	class CollisionSystem
	{
	private:
		CollisionSystem();
	public:
		~CollisionSystem();

		typedef std::unique_ptr<CollisionSystem> Ptr;
		static Ptr Create();

		AxisAlignedBoundingBox* createAABB(const sf::Vector2f& p_Pos, const sf::Vector2f& p_Vol);
		BoundingSphere* createBoundingSphere(const sf::Vector2f& p_Pos, const float& p_Rad);
		
		bool checkCollision(BoundingUnit* p_Unit, BoundingUnit* p_Collided);

	private:
		std::vector<BoundingUnit*> m_BoundingList;
	};
}