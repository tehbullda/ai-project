#include "stdafx.h"

#include "GameState.hpp"
#include "ServiceLocator.hpp"
#include "Randomizer.hpp"
#include "PostGameStats.hpp"


using namespace kartong;
using namespace AIGame;

GameState::GameState(const std::string& p_Name)
{
	m_StateManager = ServiceLocator<kartong::StateManager>::get_service();
	m_TextureManager = ServiceLocator<TextureManager>::get_service();
	m_FontManager = ServiceLocator<FontManager>::get_service();
	m_Input = ServiceLocator<system::input::Input>::get_service();
	m_Window = ServiceLocator<Window>::get_service();
	m_Name = p_Name;
	m_UpdateRequirement = StateRequirement::FIRST;
	m_DrawRequirement = StateRequirement::ALWAYS;
	m_StateTrans = StateTransition::Create();
	ServiceLocator<StateTransition>::set_service(m_StateTrans.get());


	m_xGameObjectManager = AIGame::GameObjectManager::Create();
	ServiceLocator<AIGame::GameObjectManager>::set_service(m_xGameObjectManager.get());

//	m_View = new sf::View;
	
	//m_View->setSize(m_Window->getSize().x / 2, m_Window->getSize().y / 2);
	//m_View->setRotation(10);
	
	
	
	m_Emitter = new ParticleEmitter("particles/particlesq.png");
	m_Emitter->setMoveSpeed(150, 150);
	m_Emitter->setSpawnRate(0.01);
	m_Emitter->setLifeTime(0.5f, 1.2f);
	m_Emitter->setPosition(sf::Vector2f(500.0f, 300.0f));
	m_Emitter->setColor(sf::Color(255, 0, 0, 255), sf::Color(255, 255, 255, 255));
	m_Emitter->setSpawnRadius(0, 100);
	m_Emitter->setSetStartRotation(45, 45);
	m_Emitter->setMovement(MovementState::AWAY);
	//m_TestEmitter->setRotationSpeed(-500, 500);	
	m_Emitter->setBurst(true, 30.0f);
	m_Emitter->initialize();

	m_Listener = new AudioListener();
	m_Listener->setPosition(sf::Vector2f(m_Window->getSize().x / 2, m_Window->getSize().y / 2));
	m_Listener->setRange(2000);
	m_AudioManager = ServiceLocator<AudioManager>::get_service();
	m_AudioManager->setAudioListener(m_Listener);

	m_AudioManager->preLoadSound("song.wav");
	m_AudioManager->preLoadSound("sword.wav");
	//////////
};
GameState::~GameState()
{
	delete m_Backgroundtest;
	m_Backgroundtest = nullptr;

	delete m_AuraManager;
	delete m_Emitter;
	m_Emitter = nullptr;
	m_Input = nullptr;

	delete m_Listener;
	m_Listener = nullptr;
};

bool GameState::Initialize()
{
	m_AudioManager->playClip("song.wav");
	m_GameTimer = 0.0f;
	m_Emitter->clear();
	m_Emitter->stop();
	m_AuraManager = new AuraManager(m_xGameObjectManager->GetAgentVector());
	ServiceLocator<AuraManager>::set_service(m_AuraManager);

	m_xBlackBoard = BlackBoard::Create();
	ServiceLocator<BlackBoard>::set_service(m_xBlackBoard.get());


	m_StateManager = ServiceLocator<kartong::StateManager>::get_service();
	m_TextureManager = ServiceLocator<TextureManager>::get_service();
	m_FontManager = ServiceLocator<FontManager>::get_service();
	m_Input = ServiceLocator<system::input::Input>::get_service();
	m_Window = ServiceLocator<Window>::get_service();

	


	m_TextChat = TextChat::Create();
	ServiceLocator<TextChat>::set_service(m_TextChat.get());
	m_TextChat->setMaxMessages(8);
	/*m_TextChat->sendMessage("iblo", "noob uninstall");
	m_TextChat->sendMessage("ibilo", "go play lol");
	m_TextChat->sendMessage("ibkklo", "cyka");*/
	m_Backgroundtest = new Sprite(m_TextureManager->loadTexture("ff62F1o.jpg"));

	m_Grid = Grid::Create(sf::Vector2i(21, 40), 48);
	ServiceLocator<Grid>::set_service(m_Grid.get());
	m_Grid->setPosition(0, 0);
	m_Grid->initialize();
	m_Grid->setType(sf::Vector2i(6, 5), NODETYPE::WALL);
	m_Grid->setType(sf::Vector2i(5, 5), NODETYPE::WALL);
	m_Grid->setType(sf::Vector2i(4, 5), NODETYPE::WALL);
	m_Grid->setType(sf::Vector2i(3, 5), NODETYPE::WALL);
	m_Grid->setType(sf::Vector2i(6, 4), NODETYPE::WALL);
	m_Grid->setType(sf::Vector2i(6, 4), NODETYPE::WALL);
	m_Grid->setType(sf::Vector2i(7, 8), NODETYPE::WALL);
	m_Grid->setType(sf::Vector2i(8, 8), NODETYPE::WALL);
	m_Grid->setType(sf::Vector2i(9, 8), NODETYPE::WALL);
	m_Grid->setType(sf::Vector2i(10, 8), NODETYPE::WALL);

	m_Grid->setType(sf::Vector2i(15, 5), NODETYPE::WALL);
	m_Grid->setType(sf::Vector2i(16, 5), NODETYPE::WALL);
	m_Grid->setType(sf::Vector2i(17, 5), NODETYPE::WALL);
	m_Grid->setType(sf::Vector2i(18, 5), NODETYPE::WALL);
	m_Grid->setType(sf::Vector2i(18, 6), NODETYPE::WALL);

	m_Grid->setType(sf::Vector2i(30, 15), NODETYPE::WALL);
	m_Grid->setType(sf::Vector2i(30, 16), NODETYPE::WALL);
	m_Grid->setType(sf::Vector2i(30, 14), NODETYPE::WALL);
	m_Grid->setType(sf::Vector2i(30, 17), NODETYPE::WALL);
	m_Grid->setType(sf::Vector2i(30, 18), NODETYPE::WALL);


	m_NameHandler = NameHandler::Create();
	//m_SliderTest = new kartong::Premade::PremadeSlider("", 1);
	ServiceLocator<NameHandler>::set_service(m_NameHandler.get());
	//m_SliderTest = new kartong::Premade::PremadeSlider("", 1);
	// = new kartong::Premade::PremadeTickbox("Spawn nametags every frame");

	for (int i = 0; i < ServiceLocator<SelectionSettings>::get_service()->getSetting(SettingTypes::AgentNum); i++)
	{
		m_xGameObjectManager->CreateObject(AIGame::GameObject::ObjectType::Type_Agent);
	}

	m_xGameObjectManager->CreateObject(AIGame::GameObject::ObjectType::Type_Boss);

	m_AuraManager->SetAgentVector(m_xGameObjectManager->GetAgentVector());

	m_StateTrans->reset();
	bob = 0;
	return true;
};
bool GameState::Update()
{
	m_GameTimer += DeltaTime::getDeltaTime();
	m_Emitter->setPosition(sf::Vector2f(m_xBlackBoard->GetBossPos().x + 64, m_xBlackBoard->GetBossPos().y + 64));
	m_Emitter->update();
	if (m_StateTrans->isRunning()){
		m_View->setCenter(sf::Vector2f(m_xBlackBoard->GetBossPos().x + 64, m_xBlackBoard->GetBossPos().y + 64));
		m_Emitter->play();
		//m_Window->setView(*m_View);
	}
	m_StateTrans->update();

	m_Grid->update();

	malinTest();
	willeTest();
	labanTest();

	/*if (bob == 0)
		m_Grid->getPath(m_Grid->getNode(0, 0), m_Grid->getNode(9, 9));
		bob = 1;*/

	m_TextChat->update();
	m_xGameObjectManager->Update();
	handleStates();
	return true;
};
void GameState::Draw()
{
	//if (!m_StateTrans->isRunning())
		m_Backgroundtest->draw();
	if (!m_StateTrans->isRunning())
		m_Grid->draw();
	m_xGameObjectManager->DrawObjects();
	if (!m_StateTrans->isRunning())
		m_TextChat->draw();
	if (!m_StateTrans->isRunning())
		m_NameHandler->drawTags();
	
	m_Emitter->draw();
	m_StateTrans->draw();
};

void GameState::Exit()
{
	ServiceLocator<AIGame::PostGameStats>::get_service()->reportPlayTime(m_GameTimer);
	delete m_Backgroundtest;
	m_Backgroundtest = nullptr;
	delete m_AuraManager;
	m_AuraManager = nullptr;
	delete m_TickBoxTest;
	m_TickBoxTest = nullptr;
	delete m_SliderTest;
	m_SliderTest = nullptr;
	m_xGameObjectManager->Reset();
};

std::string GameState::getName()
{
	return m_Name;
};
int GameState::getUpdateRequirement()
{
	return m_UpdateRequirement;
};
int GameState::getDrawRequirement()
{
	return m_DrawRequirement;
};

void GameState::handleStates()
{
	if (m_Input->getKeyDown(sf::Keyboard::Escape))
		m_StateManager->activate("PauseState");
	if (m_Input->getKeyDown(sf::Keyboard::Return)){
		m_StateManager->deactivate(m_Name);
		m_StateManager->activate("PostGameState");
	}
	if (m_StateTrans->isDone()){
		m_StateTrans->reset();
		m_StateManager->deactivate(m_Name);
		m_StateManager->activate("PostGameState");
		m_Window->setDefaultView();
	}
};

bool GameState::malinTest()
{
	return true;
};
bool GameState::willeTest()
{
	m_AuraManager->UpdateAuras();
	return true;
};
bool GameState::labanTest()
{
	m_NameHandler->updateTags();
	return false;
};
