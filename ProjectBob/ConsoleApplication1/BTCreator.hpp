//BTCreator.hpp

#include "stdafx.h"
#pragma once
#include "BTNode.hpp"

class BTCreator{
public:
	BTCreator();
	~BTCreator();

	BTNode* CreateAgentTree(AIGame::Agent* p_xOwner);

private:
	std::vector<BTNode*> m_xNodesToBeDeltetd;
};