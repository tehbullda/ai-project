#include "stdafx.h"

using namespace kartong;


AudioListener::AudioListener()
{
	m_Position = sf::Vector2f(0.0f, 0.0f);
};
AudioListener::~AudioListener()
{

};


AudioListener::Ptr AudioListener::Create()
{
	return AudioListener::Ptr(new AudioListener());
};
sf::Vector2f AudioListener::getPosition()
{
	return m_Position;
};
void AudioListener::setPosition(const sf::Vector2f& p_Pos)
{
	m_Position = p_Pos;
};
float AudioListener::getRange()
{
	return m_Range;
};
void AudioListener::setRange(const float& p_Range)
{
	m_Range = p_Range;
};

