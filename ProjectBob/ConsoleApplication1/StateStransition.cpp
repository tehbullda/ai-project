#include  "stdafx.h"
#include "StateTransition.hpp"
#include "ServiceLocator.hpp"
using namespace AIGame;

#define FLASHTIME 0.1f
#define PAUSETIME 1.0f
#define FADESPEED 2.0f

StateTransition::StateTransition()
{
	m_Timer = new sf::Clock();
	m_Screencolor = new sf::RectangleShape;
	m_Screencolor->setSize(kartong::ServiceLocator<kartong::Window>::get_service()->getSize());
	m_Screencolor->setFillColor(sf::Color::Transparent);
	m_Running = false;
	m_Fade = 0.0f;
	m_Timer->restart();
	m_Whitefade = 255.0f;
};

StateTransition::~StateTransition()
{
	delete m_Screencolor;
	m_Screencolor = nullptr;
	delete m_Timer;
	m_Timer = nullptr;
};

StateTransition::Ptr StateTransition::Create()
{
	return StateTransition::Ptr(new StateTransition());
};

void StateTransition::reset()
{
	m_Screencolor->setFillColor(sf::Color::Transparent);
	m_Running = false;
	m_Done = false;
	m_Paused = false;
	m_Fade = 0.0f;
	m_Whitefade = 255.0f;

};
void StateTransition::startTransition()
{
	if (m_Running)
		return;
	m_Running = true;
	m_Screencolor->setFillColor(sf::Color::White);
	m_Timer->restart();
	m_Whitefade = 255.0f;
};
void StateTransition::update()
{
	if (!m_Running)
		return;

	kartong::DeltaTime::setDeltaTime(0.0f);

	float time = m_Timer->getElapsedTime().asSeconds();
	if (time >= FLASHTIME){
		m_Screencolor->setFillColor(sf::Color(255, 255, 255, m_Whitefade));
		m_Whitefade -= 5;
		if (m_Whitefade <= 0){
			m_Whitefade = 0.0f;
		}
		if (time >= PAUSETIME)
		{
			m_Screencolor->setFillColor(sf::Color(0, 0, 0, m_Fade));
			m_Fade += FADESPEED;
			kartong::Debug::write(kartong::EDebugLevel::INFO, std::to_string(m_Fade));
			if (m_Fade >= 255.0f)
			{
				m_Done = true;
			}
		}
	}

};
void StateTransition::draw()
{
	kartong::ServiceLocator<kartong::Window>::get_service()->getWindow()->draw(*m_Screencolor);
};
bool StateTransition::isDone()
{
	return m_Done;
};
bool StateTransition::isRunning()
{
	return m_Running;
};

