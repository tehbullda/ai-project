#include "stdafx.h"
#include "TextChat.hpp"
#include "ServiceLocator.hpp"
#include "Randomizer.hpp"
#include <locale>
#include <fstream>

using namespace kartong;
using namespace AIGame;

TextChat::TextChat()
{
	m_Window = ServiceLocator<Window>::get_service();
	m_Input = ServiceLocator<system::input::Input>::get_service();
	m_FontManager = ServiceLocator<FontManager>::get_service();
	m_TextureManager = ServiceLocator<TextureManager>::get_service();

	m_Background = new sf::RectangleShape();
	m_Background->setSize(sf::Vector2f(300, 200));
	m_Background->setOutlineThickness(1);
	m_Background->setOutlineColor(sf::Color::Black);
	m_Background->setFillColor(sf::Color(150, 150, 150, 200));
	m_Background->setPosition(sf::Vector2f(10, 720 - m_Background->getGlobalBounds().height - 10));
	m_TopBar = new sf::RectangleShape();
	m_TopBar->setFillColor(sf::Color(sf::Color(150, 70, 255, 70)));
	m_TopBar->setOutlineThickness(1);
	m_TopBar->setOutlineColor(sf::Color::Black);
	m_TopBar->setSize(sf::Vector2f(m_Background->getLocalBounds().width - 2, 25));
	m_TopBar->setPosition(m_Background->getPosition());

	m_ScrollBack = new sf::RectangleShape();
	m_ScrollBack->setOutlineThickness(1);
	m_ScrollBack->setOutlineColor(sf::Color::Black);
	m_ScrollBack->setFillColor(sf::Color(255, 255, 255, 30));
	m_ScrollBack->setSize(sf::Vector2f(15, m_Background->getSize().y - 26));

	m_Scroller = new sf::RectangleShape();
	m_Scroller->setFillColor(sf::Color(150, 70, 255, 70));
	m_Scroller->setOutlineColor(sf::Color::Black);
	m_Scroller->setOutlineThickness(1);
	m_Scroller->setSize(sf::Vector2f(m_ScrollBack->getSize().x, m_ScrollBack->getSize().x));

	m_UpButton = new Premade::PremadeButton(m_TextureManager->loadTexture("GUIHUD/TextChat_Button.png"));
	m_UpButton->setPosition(m_ScrollBack->getPosition().x - 1, m_ScrollBack->getPosition().y - 1);
	m_EdgeSnap = 5;
	m_ChatMessageSpacing = 20;

	m_Unlocked = false;
	m_Position = sf::Vector2f(m_EdgeSnap, m_Window->getSize().y - m_Background->getGlobalBounds().height - m_EdgeSnap);
	m_Background->setPosition(sf::Vector2f(m_EdgeSnap, m_Window->getSize().y - m_Background->getGlobalBounds().height - m_EdgeSnap));

	loadLines();
};


TextChat::Ptr TextChat::Create()
{
	return TextChat::Ptr(new TextChat());
};
TextChat::~TextChat()
{
	for (unsigned i = 0; i < m_DisplayText.size(); i++){
		delete m_DisplayText[i];
		m_DisplayText[i] = nullptr;
	}
	delete m_Background;
	m_Background = nullptr;

	delete m_TopBar;
	m_TopBar = nullptr;

	delete m_ScrollBack;
	m_ScrollBack = nullptr;

	delete m_Scroller;
	m_Scroller = nullptr;

	delete m_UpButton;
	m_UpButton = nullptr;
};

void TextChat::update()
{
	moveBox();
	m_TopBar->setPosition(m_Position);
	m_Background->setPosition(m_Position);
	m_ScrollBack->setPosition(sf::Vector2f(m_Position.x + m_Background->getSize().x - 15, m_Position.y + 26));
	m_Scroller->setPosition(sf::Vector2f(m_ScrollBack->getPosition().x, m_ScrollBack->getPosition().y + 30));
	m_UpButton->setPosition(m_ScrollBack->getPosition().x - 1, m_ScrollBack->getPosition().y - 1);
	m_UpButton->update();
	updateTextPos();
};
void TextChat::draw()
{
	m_Window->getWindow()->draw(*m_Background);
	m_Window->getWindow()->draw(*m_TopBar);
	m_Window->getWindow()->draw(*m_ScrollBack);
	m_Window->getWindow()->draw(*m_Scroller);
	m_UpButton->draw();
	for (unsigned i = 0; i < m_DisplayText.size(); i++){
		m_Window->getWindow()->draw(*m_DisplayText[i]);
	}
};

void TextChat::setPosition(const sf::Vector2f& p_Pos)
{
	m_Position = p_Pos;
};
void TextChat::setPosition(const float& p_X, const float& p_Y)
{
	m_Position = sf::Vector2f(p_X, p_Y);
};


void TextChat::setMaxMessages(const unsigned& p_Num)
{
	m_MaxMessages = p_Num;
};
void TextChat::sendMessage(const std::string& p_Name, const std::string& p_Message)
{
	if (m_Messages.size() >= m_MaxMessages){
		m_Messages.erase(m_Messages.begin());
		removeTextContent();
	}
	m_Messages.push_back(p_Name + ": " + p_Message);
	addTextContent(p_Name + ": " + p_Message);
};
void TextChat::getMessage()
{

};

void TextChat::moveBox()
{
	toggleLock();

	if (m_Unlocked){
		setPosition(m_Input->getMousePos().x - (m_TopBar->getGlobalBounds().width / 2), m_Input->getMousePos().y - (m_TopBar->getGlobalBounds().height / 2));
	}
	else{
		if (m_Position.x + (m_Background->getGlobalBounds().width / 2) < m_Window->getSize().x / 2)
			m_Position.x = m_EdgeSnap;
		else
			m_Position.x = m_Window->getSize().x - m_Background->getGlobalBounds().width - m_EdgeSnap;
		if (m_Position.y + (m_Background->getGlobalBounds().height / 2) < m_Window->getSize().y / 2)
			m_Position.y = m_EdgeSnap;
		else
			m_Position.y = m_Window->getSize().y - m_Background->getGlobalBounds().height - m_EdgeSnap;
	}


};
bool TextChat::pointInside(const sf::Vector2f& p_Point, const sf::Vector2f& p_Pos, const sf::Vector2f& p_Vol)
{
	if (p_Point.x >= p_Pos.x && p_Point.x <= p_Pos.x + p_Vol.x &&
		p_Point.y >= p_Pos.y && p_Point.y <= p_Pos.y + p_Vol.y)
		return true;
	return false;
};
void TextChat::updateTextPos()
{
	for (unsigned i = 0; i < m_DisplayText.size(); i++){

		m_DisplayText[i]->setPosition(sf::Vector2f(
			m_Background->getPosition().x + 15,
			m_Background->getPosition().y + m_TopBar->getSize().y + 5 + (m_ChatMessageSpacing * i)));
	}
};
void TextChat::addTextContent(const std::string& p_Content)
{

	m_DisplayText.push_back(new sf::Text(p_Content, *m_FontManager->loadFont("AdobeGothicStd-Bold.otf"), 14));
	m_DisplayText[m_DisplayText.size() - 1]->setColor(sf::Color::Black);
};
void TextChat::removeTextContent()
{
	delete m_DisplayText[0];
	m_DisplayText[0] = nullptr;

	m_DisplayText.erase(m_DisplayText.begin());
};

void TextChat::toggleLock()
{
	if (!m_Unlocked){
		if (pointInside(m_Input->getMousePos(), m_TopBar->getPosition(), sf::Vector2f(m_TopBar->getGlobalBounds().width, m_TopBar->getGlobalBounds().height))){
			if (m_Input->getMouseDown(sf::Mouse::Left)){
				m_Unlocked = true;
			}
		}
	}
	else{
		if (m_Input->getMouseReleased(sf::Mouse::Left)){
			m_Unlocked = false;
		}
	}
};

void TextChat::loadLines()
{
	{
		std::ifstream file("../Data/Config/Lines/Spawn.txt");
		if (file.is_open()){
			while (!file.eof()){
				std::string name;
				std::getline(file, name);
				m_Lines_Spawn.push_back(name);
			}
		}
	}
	{
		std::ifstream file("../Data/Config/Lines/Death.txt");
		if (file.is_open()){
			while (!file.eof()){
				std::string name;
				std::getline(file, name);
				m_Lines_Death.push_back(name);
			}
		}
	}
	{
		std::ifstream file("../Data/Config/Lines/Insult.txt");
		if (file.is_open()){
			while (!file.eof()){
				std::string name;
				std::getline(file, name);
				m_Lines_Insult.push_back(name);
			}
		}
	}
	{
		std::ifstream file("../Data/Config/Lines/Motivation.txt");
		if (file.is_open()){
			while (!file.eof()){
				std::string name;
				std::getline(file, name);
				m_Lines_Motivation.push_back(name);
			}
		}
	}

};
std::string TextChat::getLine(const int& p_Type)
{
	if (p_Type == LineTypes::Line_Spawn)
		return m_Lines_Spawn[kartong::Randomizer::GetRandomInt(0, m_Lines_Spawn.size() - 1)];
	else if (p_Type == LineTypes::Line_Death)
		return m_Lines_Death[kartong::Randomizer::GetRandomInt(0, m_Lines_Death.size() - 1)];
	else if (p_Type == LineTypes::Line_Inslut)
		return m_Lines_Insult[kartong::Randomizer::GetRandomInt(0, m_Lines_Insult.size() - 1)];
	else if (p_Type == LineTypes::Line_Motivation)
		return m_Lines_Motivation[kartong::Randomizer::GetRandomInt(0, m_Lines_Motivation.size() - 1)];
	else
		return "I have nothing to say";
	
};
