#pragma once

#include "stdafx.h"

namespace kartong
{
	class Window
	{
	private:
		Window(const std::string& p_WinName, const int& p_Style, float p_Width, const float& p_Height, const int& p_FPS);
	public:
		~Window();

		typedef std::unique_ptr<Window> Ptr;
		static Ptr Create(const std::string& p_WinName, const int& p_Style, float p_Width, const float& p_Height, const int& p_FPS);

		sf::RenderWindow* getWindow();
		bool initialize();
		void updateRect();
		void display();
		void clear();
		void close();

		void setClearColor(const sf::Color& p_Color);
		void setView(sf::View& p_View);
		sf::Vector2f getPos();
		sf::Vector2f getSize();

		sf::FloatRect* getFloatRect();
		
		void setDefaultView();

	private:
		sf::RenderWindow* m_Window;
		sf::FloatRect* m_WindowRect;
		sf::Image* m_Icon;
		sf::View* m_DefaultView;

		sf::Vector2f m_Position;
		sf::Vector2f m_Size;
		sf::Color m_ClearColor;

	};
}