#pragma once

#include "stdafx.h"


namespace kartong
{
	class Sprite
	{
	public:
		Sprite(sf::Texture* p_Tex);
		Sprite(const std::string& p_File);
		~Sprite();

		
		void update();
		void draw();

		sf::FloatRect getGlobalBounds();
		sf::FloatRect getLocalBounds();

		void setPosition(const sf::Vector2f& p_Pos);
		void setPosition(const float& p_X, const float& p_Y);
		sf::Vector2f getPosition();

		void setOrigin(const sf::Vector2f& p_Point);
		void setOrigin(const float& p_X, const float& p_Y);

		void setRotation(const float& p_Rot);
		float getRotation();

		void setScale(const float& p_X, const float& p_Y);

		void setColor(sf::Color p_Color);
		void setColor(float p_R, float p_B, float p_G, float p_A);
		sf::Color getColor();

		void SetTextureRect(const sf::IntRect& p_Rect);

	private:
		Window* m_Window;
		TextureManager* m_TextureManager;
		sf::Sprite* m_Sprite;
		sf::IntRect m_Rect;
		bool m_Animated;

		std::vector<Vector2*> m_Size;
		std::vector<Vector2*> m_Pos;
		int m_Frames;
		int m_CurrentFrame;
		float m_Duration;
		float m_Timer;

	};
}