#pragma once
#include "stdafx.h"
#include "PremadeButton.hpp"


namespace kartong
{
	namespace Premade
	{
		class PremadeTickbox
		{
		public:
			PremadeTickbox(const sf::String& p_Text);
			~PremadeTickbox();

			void update();
			void draw();

			bool getState();
			void setPosition(const sf::Vector2f& p_Pos);

		private:
			Window* m_Window;

			PremadeButton* m_InvisButton;
			sf::RectangleShape m_Box;
			sf::RectangleShape m_Back;
			sf::Text* m_Text;

		private:
			sf::Vector2f m_Position;
			bool m_State;
		};
	}
};