#include "stdafx.h"

#include "TextureManager.hpp"

using namespace kartong;

TextureManager::TextureManager(const std::string& p_Directory)
{
	m_Directory = p_Directory;

	sf::Texture* tex = new sf::Texture();
	tex->loadFromFile("../Data/textures/engine/missing.png");
	m_Textures.insert(std::pair<std::string, sf::Texture*>("../Data/textures/engine/missing.png", tex));
	tex = nullptr;
};
TextureManager::~TextureManager()
{
	auto it = m_Textures.begin();
	while (it != m_Textures.end())
	{
		if (it->second)	{
			delete it->second;
			it->second = nullptr;
		}
		it++;
	}
	m_Textures.clear();
};


TextureManager::Ptr TextureManager::Create(const std::string& p_Directory)
{
	return TextureManager::Ptr(new TextureManager(p_Directory));
};

void TextureManager::preloadTexture(const std::string& p_File)
{
	auto it = m_Textures.begin();
	while (it != m_Textures.end()){
		if (it->first == p_File){
			Debug::write(EDebugLevel::INFO, "Texture already loaded: " + p_File);
			return;
		}
		it++;
	}
	sf::Texture* tex = new sf::Texture;
	if (!tex->loadFromFile(m_Directory + p_File)){
		Debug::write(EDebugLevel::ERROR, "Could not load texture: " + p_File);
		return;
	}
	Debug::write(EDebugLevel::INFO, "Preloading texture: " + p_File);
	m_Textures.insert(std::pair<std::string, sf::Texture*>(m_Directory + p_File, tex));
	tex = nullptr;
};
sf::Texture* TextureManager::loadTexture(const std::string& p_File)
{
	std::string file = m_Directory + p_File;
	auto it = m_Textures.find(file);
	if (it == m_Textures.end())
	{
		Debug::write(EDebugLevel::INFO, "Loading texture: " + p_File);
		sf::Texture* tex = new sf::Texture;
		if (!tex->loadFromFile(file)){
			Debug::write(EDebugLevel::ERROR, "Error: Could not load texture: " + p_File);
			return m_Textures.find("../Data/textures/engine/missing.png")->second;
		}
		m_Textures.insert(std::pair<std::string, sf::Texture*>(file, tex));
		tex = nullptr;
		it = m_Textures.find(file);
	}	
	return it->second;
};
sf::Texture* TextureManager::getMissingTexture()
{
	return m_Textures.find("../Data/textures/engine/missing.png")->second;
};