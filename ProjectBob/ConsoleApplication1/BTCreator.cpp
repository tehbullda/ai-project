//BTCreator.cpp

#include "stdafx.h"
#include "BTCreator.hpp"
#include "Selector.hpp"
#include "Sequencer.hpp"
#include "Decorator.hpp"
#include "Tasks.hpp"

BTCreator::BTCreator()
{

}

BTCreator::~BTCreator()
{
	auto itr = m_xNodesToBeDeltetd.begin();
	while (itr != m_xNodesToBeDeltetd.end())
	{
		delete (*itr);
		(*itr) = nullptr;
		itr = m_xNodesToBeDeltetd.erase(itr);
	}
	m_xNodesToBeDeltetd.clear();
}

BTNode* BTCreator::CreateAgentTree(AIGame::Agent* p_xOwner)
{	
	WalkinRangeOfBoss* walkToBoss = new WalkinRangeOfBoss(p_xOwner);
	AttackBoss* attackBoss = new AttackBoss(p_xOwner);
	m_xNodesToBeDeltetd.push_back(walkToBoss);
	m_xNodesToBeDeltetd.push_back(attackBoss);

	Dispell* dispell = new Dispell(p_xOwner);
	m_xNodesToBeDeltetd.push_back(dispell);

	MoveAway* moveAway = new MoveAway(p_xOwner);
	m_xNodesToBeDeltetd.push_back(moveAway);

	Sequencer* bossSequencer = new Sequencer(p_xOwner);
	m_xNodesToBeDeltetd.push_back(bossSequencer);

	bossSequencer->AddChild(walkToBoss);
	bossSequencer->AddChild(attackBoss);

	Selector* root = new Selector(p_xOwner);
	m_xNodesToBeDeltetd.push_back(root);

	root->AddChild(moveAway);
	root->AddChild(dispell);
	root->AddChild(bossSequencer);

	return root;
}