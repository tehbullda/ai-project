//Projectile.hpp

#pragma once
#include "stdafx.h"
#include "GameObject.hpp"
#include "Math.hpp"
#include "ParticleEmitter.hpp"

namespace AIGame
{
	class Projectile : public GameObject{
	public:
		Projectile(sf::Vector2f p_xStartPos, sf::Vector2f p_xTargetPos, float p_fDamage);
		~Projectile();

		bool Update();
		void UpdateBB();


		void DrawParticles();

		float GetDamage();

	private:
		sf::Vector2f SetVel(sf::Vector2f p_xStartPos, sf::Vector2f p_xTargetPos);

	private:
		sf::Vector2f m_xVel;
		kartong::ParticleEmitter* m_Emitter;

		bool m_Stopped;

		float m_fDamage;
	};
}