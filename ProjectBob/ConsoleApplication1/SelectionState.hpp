#pragma once

#include "stdafx.h"
#include "TextureManager.hpp"
#include "Sprite.hpp"
#include "ParticleEmitter.hpp"
#include "Input.hpp"
#include "TextChat.hpp"
#include "Grid.hpp"
#include "GameObjectManager.hpp"
#include "AuraManager.hpp"
#include "BlackBoard.hpp"

//
#include "GraphWindow.hpp"
#include "Boss.hpp"
#include "NameHandler.hpp"
#include "PremadeSlider.hpp"
#include "PremadeTickbox.hpp"
#include "SelectionSettings.hpp"


using namespace kartong;

namespace AIGame
{
	class SelectionState : public kartong::State
	{
	public:
		SelectionState(const std::string& p_Name);
		~SelectionState();


		bool Initialize();
		bool Update();
		void Draw();
		void Exit();

		void updateAbilities();
		void updateSliders();

		void setSettings();

		std::string getName();
		int getUpdateRequirement();
		int getDrawRequirement();

	private:
		void handleStates();
	private:
		std::string m_Name;
		int m_DrawRequirement;
		int m_UpdateRequirement;
	private:
		AIGame::GameObjectManager::Ptr m_xGameObjectManager;
		BlackBoard::Ptr m_xBlackBoard;

		AuraManager *m_AuraManager;

		kartong::StateManager* m_StateManager;
		kartong::TextureManager* m_TextureManager;
		kartong::FontManager* m_FontManager;
		kartong::system::input::Input* m_Input;
		kartong::Window* m_Window;

		SelectionSettings::Ptr m_Settings;

	private:
		Premade::PremadeButton* m_Start;
		Premade::PremadeButton* m_StartDefaults;

		Premade::PremadeTickbox* m_AutoSpawn;
		//Premade::

		Premade::PremadeTickbox* m_Ability_1;
		Premade::PremadeTickbox* m_Ability_2;
		Premade::PremadeTickbox* m_Ability_3;
		Premade::PremadeTickbox* m_Ability_4;
		Premade::PremadeTickbox* m_Ability_5;
		Premade::PremadeTickbox* m_Ability_6;

		Premade::PremadeSlider* m_DMG_Ability_1;
		Premade::PremadeSlider* m_DMG_Ability_2;
		Premade::PremadeSlider* m_DMG_Ability_3;
		Premade::PremadeSlider* m_DMG_Ability_4;
		Premade::PremadeSlider* m_DMG_Ability_5;
		Premade::PremadeSlider* m_DMG_Ability_6;

		Premade::PremadeSlider* m_Slider_HP_Agent;
		Premade::PremadeSlider* m_Slider_HP_Boss;
		//Premade::PremadeSlider* m_Slider_AI_Tick;
		Premade::PremadeSlider* m_Slider_Agents_Num;
		Premade::PremadeSlider* m_Slider_BossDMG;
		Premade::PremadeSlider* m_Slider_AgentDMG;
		//Premade::PremadeSlider* m_Slider_AgentSpeed;
		//Premade::PremadeSlider* m_Slider_BossSpeed;

	private:

	};
}