#include "stdafx.h"
#include "PremadeButton.hpp"
#include "ServiceLocator.hpp"

using namespace kartong::Premade;

PremadeButton::PremadeButton(sf::Texture* p_Texture, const std::string p_Identifer)
{
	m_AudioManager = ServiceLocator<AudioManager>::get_service();
	m_TextureManager = ServiceLocator<TextureManager>::get_service();
	m_Input = ServiceLocator<system::input::Input>::get_service();
	m_Sprite = new Sprite(p_Texture);
	m_CurrentState = ButtonState::IDLE;
	m_Identifer = p_Identifer;

	m_Rect.width = (int)m_Sprite->getGlobalBounds().width / 3;
	m_Rect.height = (int)m_Sprite->getGlobalBounds().height;
	m_Rect.left = (int)m_Sprite->getGlobalBounds().width / 3;
	m_Width = m_Sprite->getGlobalBounds().width / 3;
	m_Sprite->SetTextureRect(m_Rect);
};
PremadeButton::~PremadeButton()
{
	m_AudioManager = nullptr;
	m_TextureManager = nullptr;
	m_Input = nullptr;
	if (m_Sprite){
		delete m_Sprite;
		m_Sprite = nullptr;
	}
};

bool PremadeButton::isHovering()
{
	return (m_CurrentState == ButtonState::HOVER);
};
bool PremadeButton::isClicked()
{
	return (m_CurrentState == ButtonState::CLICKED) ? true : false;
};
bool PremadeButton::isHeld()
{
	return (m_CurrentState == ButtonState::HELD);
};
sf::Vector2f PremadeButton::getPosition()
{
	return m_Position;
};

void PremadeButton::update()
{
	sf::Vector2f mpos = m_Input->getMousePos();

	if (m_Input->getMouse(sf::Mouse::Left)) {
		if (m_Sprite->getGlobalBounds().contains(mpos)) {
			if (m_Input->getMouseDown(sf::Mouse::Left)){
				m_CurrentState = ButtonState::HELD;
			}

		}
	}
	else if (m_Sprite->getGlobalBounds().contains(mpos)) {
		if (m_CurrentState == ButtonState::HELD) {
			m_CurrentState = ButtonState::CLICKED;
		}
		else {
			m_CurrentState = ButtonState::HOVER;
		}
	}
	else {
		m_CurrentState = ButtonState::IDLE;
	}
	updateRect();
	m_Sprite->setPosition(m_Position);


}
void PremadeButton::draw()
{
	m_Sprite->draw();
};
void PremadeButton::setPosition(const sf::Vector2f& p_Pos)
{
	m_Position = p_Pos;
	m_Sprite->setPosition(m_Position);
};
void PremadeButton::setPosition(const float& p_X, const float& p_Y)
{
	m_Position = sf::Vector2f(p_X, p_Y);
};

std::string PremadeButton::getIdentifer() {
	return m_Identifer;
}
void PremadeButton::updateRect()
{
	if (m_CurrentState == ButtonState::IDLE)
		m_Rect.left = 0;
	else if (m_CurrentState == ButtonState::HOVER)
		m_Rect.left = (int)m_Width;
	else if (m_CurrentState == ButtonState::HELD)
		m_Rect.left = (int)m_Width * 2;
	m_Sprite->SetTextureRect(m_Rect);

}
sf::Vector2f PremadeButton::getSize()
{
	return sf::Vector2f(m_Sprite->getGlobalBounds().width, m_Sprite->getGlobalBounds().height);
};