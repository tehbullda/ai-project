//Tasks.hpp

#include "stdafx.h"
#pragma once

#include "Aura.hpp"
#include "BTNode.hpp"
#include "Grid.hpp"

class WalkinRangeOfBoss : public BTNode{
public:
	WalkinRangeOfBoss(AIGame::Agent* p_xOwner);
	~WalkinRangeOfBoss();

protected:
	StateType Update();

	void OnInitialize();
	void OnTerminate(StateType p_eState);
private:
	bool IsInRange();

private:
	sf::Vector2f m_xTargetLocation;
	sf::Vector2f m_xBossPrevPos;
	sf::Vector2i m_xTargetGridLocation;

	sf::Vector2i m_xCurrentGridLocation;

	std::vector<AIGame::Node*> m_xPath;
	std::vector<AIGame::Node*>::iterator m_xPath_iter;
};

class AttackBoss : public BTNode{
public:
	AttackBoss(AIGame::Agent* p_xOwner);
	~AttackBoss();

protected:
	StateType Update();

	void OnInitialize();
	void OnTerminate(StateType p_eState);

private:
	sf::Vector2f m_xBossPos;
};

class MoveAway : public BTNode{
public:
	MoveAway(AIGame::Agent* p_xOwner);
	~MoveAway();

protected:
	StateType Update();

	void OnInitialize();
	void OnTerminate(StateType p_eState);

private:
	AIGame::Grid* m_xGrid;
	sf::Vector2i m_xGridSize;
	float m_fTileSize;
};

class Dispell : public BTNode{
public:
	Dispell(AIGame::Agent* p_xOwner);
	~Dispell(); 
protected:
	StateType Update();

	void OnInitialize();
	void OnTerminate(StateType p_eState);
private:
	std::vector<AIGame::Aura*> m_xAurasToDispel;

};