// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>

#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>
#include <SFML\System.hpp>
#include <SFML\Window.hpp>

#include <queue>
#include <memory>
#include <vector>
#include <map>
#include <unordered_map>
#include <string>
#include <iostream>

#include "Engine.hpp"
#include "Debug.hpp"
#include "Utils.hpp"
#include "Math.hpp"
#include "UnitManager.hpp"
#include "DeltaTime.hpp"
#include "DrawQueue.hpp"
// TODO: reference additional headers your program requires here
