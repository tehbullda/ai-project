#pragma once

#include "stdafx.h"
#include "Sprite.hpp"

namespace kartong
{
	class Particle
	{
	public:
		Particle(const std::string& p_File, const float& p_LifeTime, const float& p_MoveSpeed, const sf::Vector2f& p_Pos,
			const sf::Color& p_Color, const sf::Vector2f& p_Direction, const float& p_StartRotation, const float& p_RotationSpeed);
		~Particle();

		bool update();
		void draw();

	private:
		TextureManager* m_TextureManager;
		struct Stats
		{
			Sprite* m_Sprite;
			sf::Vector2f m_Position;
			sf::Vector2f m_Direction;
			sf::Color m_Color;
			float m_MoveSpeed;
			float m_LifeTime;
			float m_Rotation;
			float m_RotationSpeed;
			float m_EndAlpha;
			float m_FadeSpeed;
			float m_StartFadePercent;
			float m_WhenStartFade;
			float m_Alpha;

		} m_Stats;
	};
}