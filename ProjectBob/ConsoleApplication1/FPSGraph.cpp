#include "stdafx.h"
#include "FPSGraph.hpp"
#include "GraphWindow.hpp"
#include "ServiceLocator.hpp"
using namespace kartong;

FPSGraph::FPSGraph(){
	m_FontManager = ServiceLocator<FontManager>::get_service();
	m_Input = ServiceLocator<system::input::Input>::get_service();
	m_Window = ServiceLocator<Window>::get_service();
	m_Graph = GraphWindow::Create();
	m_Graph->setPosition(sf::Vector2f(m_Window->getSize().x - m_Graph->getSize().x - 15, 15));

	m_FPStext = new sf::Text("", *m_FontManager->loadFont("AdobeGothicStd-Bold.otf"), 10);
	m_FPStext->setPosition(sf::Vector2f(m_Window->getSize().x - m_Graph->getSize().x + 40, 84));
	m_FPStext->setColor(sf::Color::Black);
	m_RefreshRate = 0.25;
	m_Timer = 0.0f;
}

FPSGraph::~FPSGraph()
{
	delete m_FPStext;
	m_FPStext = nullptr;
};

FPSGraph::Ptr FPSGraph::Create()
{
	return FPSGraph::Ptr(new FPSGraph());
};

void FPSGraph::update()
{
	if (m_Input->getKey(sf::Keyboard::LAlt))
		if (m_Input->getKeyDown(sf::Keyboard::F))
			toggleActive();
	if (!m_Active)
		return;

	m_Timer += DeltaTime::getDeltaTime();
	m_Values.push_back(1.0f / DeltaTime::getDeltaTime());
	if (m_Timer >= m_RefreshRate)
	{
		m_fps = (int)getAverage();
		m_Graph->addValue((float)m_fps);
		m_Graph->update();
		m_FPStext->setString("Current: " + std::to_string(m_fps));
		m_Timer = 0.0f;
	}
};
void FPSGraph::draw()
{
	if (!m_Active)
		return;
	m_Graph->draw();
	m_Window->getWindow()->draw(*m_FPStext);
};
float FPSGraph::getAverage()
{
	float val = 0;
	for (unsigned i = 0; i < m_Values.size(); i++)
		val += m_Values[i];
	val = val / m_Values.size();
	m_Values.clear();
	return val;
};
void FPSGraph::toggleActive()
{
	m_Active = !m_Active;
};