#pragma once
#include <random>


namespace kartong
{
	namespace Randomizer
	{
		int GetRandomInt(int p_min = 0, int p_max = INT_MAX);
		double GetRandomDouble(double p_min = 0.0, double p_max = 0.0);
	}
}