#pragma once
#include "stdafx.h"
#include "Tooltip.hpp"
namespace AIGame {
	class Icon
	{
	public:
		Icon(const std::string &texturepath, const sf::Vector2f &pos, const std::string &tooltip = "");
		~Icon();

		void Update();
		void Draw();

		void SetPosition(const sf::Vector2f &pos);
		void SetTooltip(const std::string &text);

		sf::Vector2f GetPosition();
		sf::FloatRect GetGlobalRect();
		sf::FloatRect GetLocalRect();
		Tooltip& GetEditableTooltip();

	private:
		Tooltip *m_tooltip;
		sf::Sprite m_sprite;
	};
}