//Boss.hpp

#pragma once
#include "stdafx.h"
#include "GameObject.hpp"
#include "Grid.hpp"
#include "BossAbilityManager.hpp"
#include "NameHandler.hpp"

namespace AIGame
{
	class Boss : public GameObject {
	public:
		Boss(sf::Vector2f p_xPos);
		~Boss();

		bool Update();
		void UpdateBB();

		void Draw();

		void TakeDamage(float p_fAmount);
		void Die();

		void SetSettings(float p_fHP);

	private:
		Grid* m_Grid;
	private:
		BossAbilityManager *m_abilitymanager;
		bool m_casting;
		float m_fHP;
		float m_fMaxHP;
		BossNameTag* m_xNameTag;
	};
}