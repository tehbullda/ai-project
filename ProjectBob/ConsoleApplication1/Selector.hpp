//Selector.hpp
// do a, b or c

#pragma once
#include "stdafx.h"
#include "BTNode.hpp"

class Selector : public BTNode{
public:
	Selector(AIGame::Agent* p_xOwner);
	~Selector();

	void AddChild(BTNode* p_xNode);

protected:
	virtual StateType Update();

	virtual void OnInitialize();
	virtual void OnTerminate(StateType p_eState);

private:
	std::vector<BTNode*>::iterator m_xCurrentChild;
	std::vector<BTNode*> m_xChildren;
};

/*Different selectors*/
