#include "stdafx.h"
#include "PostGameStats.hpp"

using namespace AIGame;



PostGameStats::PostGameStats()
{

};

PostGameStats::~PostGameStats()
{

};

PostGameStats::Ptr PostGameStats::Create()
{
	return PostGameStats::Ptr(new PostGameStats());
};

void PostGameStats::reset()
{
	m_DamageGiven.clear();
	m_DamageTaken.clear();
	m_DeathCounts.clear();
	m_DistanceWalked.clear();
	m_Lifetimes.clear();
	m_ProjectilesFired.clear();

	m_BossStats.BabaamCasted = 0;
	m_BossStats.CleaveCasted = 0;
	m_BossStats.JumpAwayCasted = 0;
	m_BossStats.PlagueCasted = 0;
	m_BossStats.SmashCasted = 0;
	m_BossStats.SpreadLavaCasted = 0;
};

void PostGameStats::reportDeath(const std::string& p_Name, const float& p_TimeAlive)
{

	auto it = m_DeathCounts.begin();
	while (it != m_DeathCounts.end()){
		if (it->first == p_Name){
			it->second++;
			reportLifeTime(p_Name, p_TimeAlive);
			return;
		}
		it++;
	}
	m_DeathCounts.insert(std::pair<std::string, int>(p_Name, 1));
	reportLifeTime(p_Name, p_TimeAlive);

};
void PostGameStats::reportLifeTime(const std::string& p_Name, const float& p_Val)
{
	auto it = m_Lifetimes.begin();
	while (it != m_Lifetimes.end())
	{
		if (it->first == p_Name){
			it->second = p_Val;
			return;
		}
		it++;
	}
	m_Lifetimes.insert(std::pair<std::string, float>(p_Name, p_Val));
};
void PostGameStats::reportDamageTaken(const std::string& p_Name, const float& p_Val)
{
	auto it = m_DamageTaken.begin();
	while (it != m_DamageTaken.end())
	{
		if (it->first == p_Name){
			it->second = (int)p_Val;
			return;
		}
		it++;
	}
	m_DamageTaken.insert(std::pair<std::string, float>(p_Name, p_Val));
};
void PostGameStats::reportDamageGiven(const std::string& p_Name, const float& p_Val)
{
	auto it = m_DamageGiven.begin();
	while (it != m_DamageGiven.end())
	{
		if (it->first == p_Name){
			it->second = (int)p_Val;
			return;
		}
		it++;
	}
	m_DamageGiven.insert(std::pair<std::string, float>(p_Name, p_Val));
};
void PostGameStats::reportDistanceWalked(const std::string& p_Name, const float& p_Val)
{
	auto it = m_DistanceWalked.begin();
	while (it != m_DistanceWalked.end())
	{
		if (it->first == p_Name){
			it->second = (int)p_Val;
			return;
		}
		it++;
	}
	m_DistanceWalked.insert(std::pair<std::string, float>(p_Name, p_Val));
};
void PostGameStats::reportProjectilesFired(const std::string& p_Name, const float& p_Val)
{
	auto it = m_ProjectilesFired.begin();
	while (it != m_ProjectilesFired.end())
	{
		if (it->first == p_Name){
			it->second = (int)p_Val;
			return;
		}
		it++;
	}
	m_ProjectilesFired.insert(std::pair<std::string, float>(p_Name, p_Val));
};


int PostGameStats::getDeathNum()
{
	int deaths = 0;
	auto it = m_DeathCounts.begin();
	while (it != m_DeathCounts.end()){
		deaths += it->second;
		it++;
	}
	return deaths;
};
int PostGameStats::getTotalDamageTaken()
{
	int dmgtaken = 0;
	auto it = m_DamageTaken.begin();
	while (it != m_DamageTaken.end()){
		dmgtaken += it->second;
		it++;
	}
	return dmgtaken;
};
int PostGameStats::getTotalDamageGiven()
{
	int dmggiven = 0;
	auto it = m_DamageGiven.begin();
	while (it != m_DamageGiven.end()){
		dmggiven += it->second;
		it++;
	}
	return dmggiven;
};
float PostGameStats::getTotalLifetime()
{
	float lifetime = 0;
	auto it = m_Lifetimes.begin();
	while (it != m_Lifetimes.end()){
		lifetime += it->second;
		it++;
	}
	return lifetime;
};
int PostGameStats::getTotalDistanceWalked()
{
	int distance = 0;
	auto it = m_DistanceWalked.begin();
	while (it != m_DistanceWalked.end()){
		distance += it->second;
		it++;
	}
	return distance;
};
int PostGameStats::getTotalProjectilesFired()
{
	int projectiles = 0;
	auto it = m_ProjectilesFired.begin();
	while (it != m_ProjectilesFired.end()){
		projectiles += it->second;
		it++;
	}
	return projectiles;
};

std::pair<std::string, int> PostGameStats::getHighestAgentDamageGiven()
{
	std::string name = "";
	int highest = 0;
	auto it = m_DamageGiven.begin();
	while (it != m_DamageGiven.end()){
		if (it->second > highest){
			name = it->first;
			highest = it->second;
		}
		it++;
	}
	return std::pair<std::string, int>(name, highest);
};
std::pair<std::string, int> PostGameStats::getHighestAgentDamageTaken()
{
	std::string name = "";
	int highest = 0;
	auto it = m_DamageTaken.begin();
	while (it != m_DamageTaken.end()){
		if (it->second > highest){
			name = it->first;
			highest = it->second;
		}
		it++;
	}
	return std::pair<std::string, int>(name, highest);
};
std::pair<std::string, float> PostGameStats::getHighestAgentLifetime()
{
	std::string name = "";
	int highest = 0;
	auto it = m_Lifetimes.begin();
	while (it != m_Lifetimes.end()){
		if (it->second > highest){
			name = it->first;
			highest = (int)it->second;
		}
		it++;
	}
	return std::pair<std::string, int>(name, highest);
};
std::pair<std::string, float> PostGameStats::getLowestAgentLifetime()
{
	std::string name = "";
	int lowest = std::numeric_limits<int>::max();
	auto it = m_Lifetimes.begin();
	while (it != m_Lifetimes.end()){
		if (it->second < lowest){
			name = it->first;
			lowest = (int)it->second;
		}
		it++;
	}
	return std::pair<std::string, int>(name, lowest);
};
std::pair<std::string, int> PostGameStats::getHighestAgentProjectilesFired()
{
	std::string name = "";
	int highest = 0;
	auto it = m_ProjectilesFired.begin();
	while (it != m_ProjectilesFired.end()){
		if (it->second > highest){
			name = it->first;
			highest = it->second;
		}
		it++;
	}
	return std::pair<std::string, int>(name, highest);
};

void PostGameStats::reportPlayTime(const float& p_Time)
{
	m_PlayTime = p_Time;
};
float PostGameStats::getPlayTime()
{
	return m_PlayTime;
};
int PostGameStats::getTotalAbilitiesCasted(){
	return m_BossStats.BabaamCasted + m_BossStats.CleaveCasted + m_BossStats.JumpAwayCasted + m_BossStats.PlagueCasted + m_BossStats.SmashCasted + m_BossStats.SpreadLavaCasted;
};
void PostGameStats::setWon(const bool& p_Won)
{
	m_Won = p_Won;
};
bool PostGameStats::haveWon()
{
	return m_Won;
};


void PostGameStats::reportSmashCast(){ m_BossStats.SmashCasted++; };
void PostGameStats::reportCleaveCast(){ m_BossStats.CleaveCasted++; };
void PostGameStats::reportPlagueCast(){ m_BossStats.PlagueCasted++; };
void PostGameStats::reportSpreadLavaCast(){ m_BossStats.SpreadLavaCasted++; };
void PostGameStats::reportBabaamCast(){ m_BossStats.BabaamCasted++; };
void PostGameStats::reportJumpAwayCast(){ m_BossStats.JumpAwayCasted++; };

int PostGameStats::getSmashCast(){ return m_BossStats.SmashCasted; };
int PostGameStats::getCleaveCast(){ return m_BossStats.CleaveCasted; };
int PostGameStats::getPlagueCast(){ return m_BossStats.PlagueCasted; };
int PostGameStats::getBabaamCast(){ return m_BossStats.BabaamCasted; };
int PostGameStats::getSpreadLavaCast(){ return m_BossStats.SpreadLavaCasted; };
int PostGameStats::getJumpAwayCast(){ return m_BossStats.JumpAwayCasted; };