//GameObjectManager.cpp

#include "stdafx.h"
#include "GameObjectManager.hpp"
#include "CollisionSystem.hpp"
#include <random>
#include "AuraManager.hpp"
#include "NameHandler.hpp"
#include "StateTransition.hpp"
using namespace kartong;
using namespace AIGame;

unsigned int GameObjectManager::m_uiNewPlayerID = 0;

GameObjectManager::GameObjectManager()
{
	m_xBTCreator = new BTCreator;
	m_xSettings = ServiceLocator<SelectionSettings>::get_service();
}

GameObjectManager::Ptr GameObjectManager::Create()
{
	return GameObjectManager::Ptr(new GameObjectManager());
};

GameObjectManager::~GameObjectManager()
{
	auto itr = m_xObjectList.begin();
	while (itr != m_xObjectList.end())
	{
		delete (*itr);
		(*itr) = nullptr;
		itr = m_xObjectList.erase(itr);
	}
	m_xObjectList.clear();

	auto projItr = m_xProjectiles.begin();
	while (projItr != m_xProjectiles.end())
	{
		delete (*projItr);
		(*projItr) = nullptr;
		++projItr;
	}
	m_xProjectiles.clear();

	auto agentItr = m_xAgents.begin();
	while (agentItr != m_xAgents.end())
	{
		delete (*agentItr);
		(*agentItr) = nullptr;
		++agentItr;
	}
	m_xAgents.clear();

	delete m_xBoss;
	m_xBoss = nullptr;

	delete m_xBTCreator;
	m_xBTCreator = nullptr;
}

void GameObjectManager::Update()
{
	if (m_xAgents.size() <= 0){
		kartong::ServiceLocator<StateManager>::get_service()->activate("PostGameState");
		kartong::ServiceLocator<StateManager>::get_service()->deactivate("GameState");
		kartong::ServiceLocator<AIGame::PostGameStats>::get_service()->setWon(false);
		return;
	}

	auto itr = m_xObjectList.begin();
	while (itr != m_xObjectList.end())
	{
		(*itr)->Update();
		(*itr)->UpdateBB();
		++itr;
	}

	m_xBoss->Update();
	m_xBoss->UpdateBB();

	auto projItr = m_xProjectiles.begin();
	while (projItr != m_xProjectiles.end())
	{
		(*projItr)->Update();
		(*projItr)->UpdateBB();
		++projItr;
	}

	auto agentItr = m_xAgents.begin();
	while (agentItr != m_xAgents.end())
	{
		if (!(*agentItr)->Update()){
			ServiceLocator<AuraManager>::get_service()->AgentRemoved(*agentItr);
			delete *agentItr;
			agentItr = m_xAgents.erase(agentItr);


			continue;
		}
		(*agentItr)->UpdateBB();
		++agentItr;
	}

	if (m_xSettings->getAutoSpawn())
	{
		while (m_xAgents.size() < m_xSettings->getSetting(AgentNum))
		{
			CreateObject(AIGame::GameObject::ObjectType::Type_Agent);
		}
	}

	CheckCollisionsProjBoss();
}

void GameObjectManager::DrawObjects()
{
	auto itr = m_xObjectList.begin();
	while (itr != m_xObjectList.end())
	{
		(*itr)->GetSprite()->draw();
		++itr;
	}

	m_xBoss->Draw();

	auto projItr = m_xProjectiles.begin();
	while (projItr != m_xProjectiles.end())
	{
		(*projItr)->DrawParticles();
		(*projItr)->GetSprite()->draw();
		++projItr;
	}

	auto agentItr = m_xAgents.begin();
	while (agentItr != m_xAgents.end())
	{
		(*agentItr)->GetSprite()->draw();
		++agentItr;
	}
}

void GameObjectManager::CreateObject(GameObject::ObjectType p_xType)
{
	if (p_xType == GameObject::Type_Agent)
	{ 
		float posX = (float)(rand() % 24);
		posX *= 48;

		//Agent* newAgent = new Agent(sf::Vector2f(912.0f, 576.0f));
		Agent* newAgent = new Agent(sf::Vector2f(posX, 12 * 48));
		newAgent->AddBTRoot(m_xBTCreator->CreateAgentTree(newAgent));
		newAgent->SetSettings(m_xSettings->getSetting(SettingTypes::AgentHP), m_xSettings->getSetting(SettingTypes::AgentDMG), GetNewPlayerID(), ServiceLocator<NameHandler>::get_service()->getNameTag());
		newAgent->SetPosAndOccupied(sf::Vector2f(posX, 12 * 48));
		m_xAgents.push_back(newAgent);
	}
	else if (p_xType == GameObject::Type_Boss)
	{
		if (!m_xBoss)
		{
			m_xBoss = new Boss(sf::Vector2f(500.0f, 200.0f));
			m_xBoss->SetSettings(m_xSettings->getSetting(SettingTypes::BossHP));
		}
	}
	else if (p_xType == GameObject::Type_Object)
	{
		m_xObjectList.push_back(new GameObject(sf::Vector2f(0.0f, 0.0f)));
	}
}

void GameObjectManager::CreateProjectile(sf::Vector2f p_xStartPos, sf::Vector2f p_xEndPos, float p_fDamage)
{
	m_xProjectiles.push_back(new Projectile(p_xStartPos, p_xEndPos, p_fDamage));
}

std::vector<Agent*> GameObjectManager::GetAgentVector() {

	return m_xAgents;
}
void GameObjectManager::Reset()
{
	for (unsigned i = 0; i < m_xAgents.size(); i++){
		delete m_xAgents[i];
		m_xAgents[i] = nullptr;
	}
	m_xAgents.clear();

	for (unsigned i = 0; i < m_xObjectList.size(); i++){
		delete m_xObjectList[i];
		m_xObjectList[i] = nullptr;
	}
	m_xObjectList.clear();

	for (unsigned i = 0; i < m_xProjectiles.size(); i++){
		delete m_xProjectiles[i];
		m_xProjectiles[i] = nullptr;
	}
	m_xProjectiles.clear();

	delete m_xBoss;
	m_xBoss = nullptr;
	
};

void GameObjectManager::CheckCollisionsProjBoss()
{
	if (!m_xProjectiles.empty())
	{
		auto itr = m_xProjectiles.begin();
		while (itr != m_xProjectiles.end())
		{
			if (ServiceLocator<kartong::CollisionSystem>::get_service()->checkCollision(
				(*itr)->GetCollider(), m_xBoss->GetCollider()))
			{
				//change boss hp
				m_xBoss->TakeDamage((*itr)->GetDamage());

				//delete proj
				delete (*itr);
				(*itr) = nullptr;
				itr = m_xProjectiles.erase(itr);
			}
			else if ((*itr)->GetPos().x > ServiceLocator<Window>::get_service()->getSize().x ||
				(*itr)->GetPos().y > ServiceLocator<Window>::get_service()->getSize().y ||
				(*itr)->GetPos().x < 10.0f || (*itr)->GetPos().y < 10.0f)
			{
				delete (*itr);
				(*itr) = nullptr;
				itr = m_xProjectiles.erase(itr);
			}
			else
				++itr;
		}
	}
}

void GameObjectManager::SetBossPos(sf::Vector2i pos) {
	m_xBoss->SetPos(sf::Vector2f(pos));
}