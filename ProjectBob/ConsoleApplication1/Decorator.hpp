//Decorator.hpp
//can change results, repeat etc

#pragma once
#include "stdafx.h"
#include "BTNode.hpp"

class Decorator : public BTNode{
public:
	Decorator(AIGame::Agent* p_xOwner);
	~Decorator();

	//repeat a number of times
	//repeat until false/true returns

	//always send back success
	//change the result

	void SetChild(BTNode* p_xNode);

protected:
	StateType Update();

	void OnInitialize();
	void OnTerminate(StateType p_eState);

private:
	BTNode* m_xChild;
};