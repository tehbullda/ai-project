#include "stdafx.h"
#include "Icon.hpp"
#include "ServiceLocator.hpp"
//#include "Tooltip.hpp"
namespace AIGame {
	Icon::Icon(const std::string &texturepath, const sf::Vector2f &pos, const std::string &tooltip)
	{
		m_sprite.setTexture(*kartong::ServiceLocator<kartong::TextureManager>::get_service()->loadTexture(texturepath));
		m_sprite.setPosition(pos);
		m_tooltip = new Tooltip(tooltip, pos, 12, sf::Color::Black, true);
		m_tooltip->SetTriggerArea(sf::IntRect(m_sprite.getGlobalBounds()));
	}

	Icon::~Icon()
	{
		if (m_tooltip != nullptr) {
			delete m_tooltip;
			m_tooltip = nullptr;
		}
	}

	void Icon::Update() {
		m_tooltip->Update();
	}

	void Icon::Draw() {
		kartong::ServiceLocator<kartong::Window>::get_service()->getWindow()->draw(m_sprite);
		m_tooltip->Draw();
	}

	void Icon::SetPosition(const sf::Vector2f &pos) {
		m_sprite.setPosition(pos);
	}

	void Icon::SetTooltip(const std::string &text) {
		m_tooltip->SetText(text);
	}

	sf::Vector2f Icon::GetPosition() {
		return m_sprite.getPosition();
	}

	sf::FloatRect Icon::GetGlobalRect() {
		return m_sprite.getGlobalBounds();
	}

	sf::FloatRect Icon::GetLocalRect() {
		return m_sprite.getLocalBounds();
	}

	Tooltip& Icon::GetEditableTooltip() {
		return *m_tooltip;
	}
}