#include "stdafx.h"
#include "ParticleEmitter.hpp"
#include "Particle.hpp"
#include "Randomizer.hpp"
#include "ServiceLocator.hpp"

using namespace kartong;

ParticleEmitter::ParticleEmitter(const std::string& p_Texture)
{
	m_Stats.p_File = p_Texture;
	m_EStats.m_Burst = false;
	m_Stopped = false;
};
ParticleEmitter::~ParticleEmitter()
{
	for (unsigned int i = 0; i < m_Particles.size(); i++)
	{
		if (m_Particles[i])
		{
			delete m_Particles[i];
			m_Particles[i] = nullptr;
		}
	}
	m_Particles.clear();
};

void ParticleEmitter::initialize()
{
	m_EStats.m_Timer = 0.0f;
	m_EStats.m_PlayTimer = 0.0f;
	m_EStats.m_Playing = !m_EStats.m_Burst;
};
void ParticleEmitter::update()
{


	if (m_EStats.m_Burst)
	{
		m_EStats.m_PlayTimer += DeltaTime::getDeltaTime();
		if (m_EStats.m_PlayTimer >= m_EStats.m_PlayTime)
			m_EStats.m_Playing = false;
	}

	m_EStats.m_Timer += DeltaTime::getDeltaTime();
	//Creating
	if (m_EStats.m_Playing && !m_Stopped){
		if (m_EStats.m_Timer >= m_EStats.m_SpawnRate){
			sf::Color usecolor;
			usecolor.r = Randomizer::GetRandomInt(m_Stats.m_MinColor.r, m_Stats.m_MaxColor.r);
			usecolor.g = Randomizer::GetRandomInt(m_Stats.m_MinColor.g, m_Stats.m_MaxColor.g);
			usecolor.b = Randomizer::GetRandomInt(m_Stats.m_MinColor.b, m_Stats.m_MaxColor.b);
			usecolor.a = Randomizer::GetRandomInt(m_Stats.m_MinColor.a, m_Stats.m_MaxColor.a);

			float radius = (float)Randomizer::GetRandomDouble(m_EStats.m_MinSpawnRadius, m_EStats.m_MaxSpawnRadius);

			Vector2 startpos;
			/*sf::Vector2f temppos = m_Stats.m_Position.getSfVector2f() + radius *
				sf::Vector2f(Math::sin((float)Randomizer::GetRandomDouble(-100.0f, 100.0f)),
				Math::cos((float)Randomizer::GetRandomDouble(-100.0f, 100.0f)));
				startpos.m_x = temppos.x;
				startpos.m_y = temppos.y;*/

			Vector2 direction = Vector2(Math::sin((float)Randomizer::GetRandomDouble(-Math::PI, Math::PI)),
				Math::cos((float)Randomizer::GetRandomDouble(-Math::PI, Math::PI)));

			direction.normalize();
			Vector2 temp;//
			temp.m_x = direction.m_x * radius;
			temp.m_y = direction.m_y * radius;
			startpos.m_x = m_Stats.m_Position.m_x + temp.m_x;
			startpos.m_y = m_Stats.m_Position.m_y + temp.m_y;
			if (m_EStats.m_Movement == MovementState::TOWARD){
				direction.m_x = -direction.m_x;
				direction.m_y = -direction.m_y;
			}
			else if (m_EStats.m_Movement == MovementState::RANDOM)
				direction = Vector2(Math::sin((float)Randomizer::GetRandomDouble(-Math::PI, Math::PI)),
				Math::cos((float)Randomizer::GetRandomDouble(-Math::PI, Math::PI)));




			m_Particles.push_back(new Particle(
				m_Stats.p_File,
				(float)Randomizer::GetRandomDouble(m_Stats.m_MinLifeTime, m_Stats.m_MaxLifeTime),
				(float)Randomizer::GetRandomDouble(m_Stats.m_MinMoveSpeed, m_Stats.m_MaxMoveSpeed),
				startpos.getSfVector2f(),
				usecolor,
				direction.getSfVector2f(),
				//sf::Vector2f(Math::sin((float)Randomizer::GetRandomDouble(-100, 100)),
				//Math::cos((float)Randomizer::GetRandomDouble(-100, 100))),
				(float)Randomizer::GetRandomDouble(m_Stats.m_MinStartRotation, m_Stats.m_MaxStartRotation),
				(float)Randomizer::GetRandomDouble(m_Stats.m_MinRotationSpeed, m_Stats.m_MaxRotationSpeed)));
			m_EStats.m_Timer = 0.0f;
		}
	}
	for (unsigned i = 0; i < m_Particles.size(); i++){
		if (!m_Particles[i]->update())
		{
			delete m_Particles[i];
			m_Particles.erase(m_Particles.begin() + i);
		}
	}
};
void ParticleEmitter::draw()
{
	for (unsigned int i = 0; i < m_Particles.size(); i++){
		if (m_Particles[i])
			m_Particles[i]->draw();

	}
};

void ParticleEmitter::play()
{
	m_EStats.m_Playing = true;
	m_EStats.m_PlayTimer = 0.0f;
};
void ParticleEmitter::stop()
{
	m_EStats.m_Playing = false;
};
void ParticleEmitter::clear()
{
	for (unsigned i = 0; i < m_Particles.size(); i++){
		delete m_Particles[i];
		m_Particles.erase(m_Particles.begin() + i);
	}
};

void ParticleEmitter::setPosition(const sf::Vector2f& p_Pos)
{
	m_Stats.m_Position = Vector2(p_Pos.x, p_Pos.y);
};
void ParticleEmitter::setSpawnRadius(const float& p_Min, const float& p_Max)
{
	m_EStats.m_MinSpawnRadius = p_Min;
	m_EStats.m_MaxSpawnRadius = p_Max;
}
void ParticleEmitter::setSpawnRate(const float& p_SpawnRate)
{
	m_EStats.m_SpawnRate = p_SpawnRate;
};
void ParticleEmitter::setMovement(const unsigned& p_Movement)
{
	m_EStats.m_Movement = p_Movement;
};
void ParticleEmitter::setLifeTime(const float& p_Min, const float& p_Max)
{
	m_Stats.m_MinLifeTime = p_Min;
	m_Stats.m_MaxLifeTime = p_Max;
};
void ParticleEmitter::setMoveSpeed(const float& p_Min, const float& p_Max)
{
	m_Stats.m_MinMoveSpeed = p_Min;
	m_Stats.m_MaxMoveSpeed = p_Max;
};
void ParticleEmitter::setSetStartRotation(const float& p_Min, const float& p_Max)
{
	m_Stats.m_MinStartRotation = p_Min;
	m_Stats.m_MaxStartRotation = p_Max;
};
void ParticleEmitter::setRotationSpeed(const float& p_Min, const float& p_Max)
{
	m_Stats.m_MinRotationSpeed = p_Min;
	m_Stats.m_MaxRotationSpeed = p_Max;
}
void ParticleEmitter::setBurst(const bool& p_Burst, const float& p_PlayTime)
{
	m_EStats.m_Burst = p_Burst;
	m_EStats.m_PlayTime = p_PlayTime;
	m_EStats.m_Playing = false;
};
void ParticleEmitter::setColor(const sf::Color& p_Min, const sf::Color& p_Max)
{
	m_Stats.m_MinColor = p_Min;
	m_Stats.m_MaxColor = p_Max;
};
int ParticleEmitter::getActiveParticles()
{
	return m_Particles.size();
};