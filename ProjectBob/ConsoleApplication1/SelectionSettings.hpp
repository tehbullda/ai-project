#pragma once

#include "stdafx.h"

namespace AIGame
{
	enum SettingTypes
	{
		BossDMG,
		BossHP,
		BossSpeed,
		AgentDMG,
		AgentHP,
		AgentSpeed,
		AgentNum,
		AITicks,
		Size
	};





	class SelectionSettings
	{
	private:
		SelectionSettings();
	public:
		~SelectionSettings();
		typedef std::unique_ptr<SelectionSettings> Ptr;
		static Ptr Create();

		std::vector<std::string> getAbilities();
		float getSetting(const SettingTypes& p_Setting);
		float getAbilityDMG(const std::string& p_Ability);

		void addAbility(const std::string& p_Ability);
		void setSetting(const SettingTypes& p_Setting, const float& p_Value);
		void setAbilityDMG(const std::string& p_Ability, const float& p_Value);

		void setDefaults();
		bool isDefault();
		bool getAutoSpawn();
		void setAutoSpawn(const bool& p_Auto);

		void setWon(const bool& p_Won);
		bool getWon();

		void setGameTime(const float& p_Time);
		float getGameTime();

	private:
		std::vector<std::string> m_Abilities;
		std::map<SettingTypes, float> m_Settings;
		std::map<std::string, float> m_AbilityDMG;
		bool m_Default;
		bool m_AutoSpawn;
		bool m_Won;
		float m_Time;
	};

}