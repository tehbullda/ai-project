#include "stdafx.h"

#include "ExampleState.hpp"
#include "ServiceLocator.hpp"
#include "ParticleManager.hpp"

using namespace kartong;

ExampleState::ExampleState(const std::string& p_Name)
{
	m_TextureManager = ServiceLocator<TextureManager>::get_service();
	m_FontManager = ServiceLocator<FontManager>::get_service();
	m_Input = ServiceLocator<system::input::Input>::get_service();
	m_Window = ServiceLocator<Window>::get_service();
	m_Name = p_Name;
	m_UpdateRequitement = StateRequirement::FIRST;
	m_DrawRequitement = StateRequirement::FIRST;
	test = 0;
	
	m_TestEmitter = ServiceLocator<ParticleManager>::get_service()->createEmitter("particles/particlesq.png");
	m_TestEmitter->setMoveSpeed(50, 50);
	m_TestEmitter->setSpawnRate(0.01f);
	m_TestEmitter->setLifeTime(0.5f, 1.2f);
	m_TestEmitter->setPosition(sf::Vector2f(500.0f, 300.0f));
	m_TestEmitter->setColor(sf::Color(0, 255, 255, 255), sf::Color(255, 255, 255, 255));
	m_TestEmitter->setSpawnRadius(75, 75);	
	m_TestEmitter->setSetStartRotation(45, 45);
	m_TestEmitter->setMovement(MovementState::AWAY);
	//m_TestEmitter->setRotationSpeed(-500, 500);	
	//m_TestEmitter->setBurst(true, 0.5f);				
	m_TestEmitter->initialize();

	m_TestOwn = new Sprite(m_TextureManager->loadTexture("Tests/SNES.png"));

	m_TestButton = new Premade::PremadeButton(m_TextureManager->loadTexture("Tests/testbutton.png"));
	m_TestButton->setPosition(400, 300);

	m_TestText = new sf::Text("I CAN WRITE TEXT NOW", *m_FontManager->loadFont("AdobeGothicStd-Bold.otf"), 30);
	m_TestText->setColor(sf::Color::Red);
	
};
ExampleState::~ExampleState()
{
	delete m_TestEmitter;
	m_TestEmitter->draw();
	delete m_TestOwn;
	m_TestOwn = nullptr;
	delete m_TestButton;
	m_TestButton = nullptr;
	delete m_TestText;
	m_TestText = nullptr;
	m_Input = nullptr;

};

bool ExampleState::Initialize()
{

	return false;
};
bool ExampleState::Update()
{
	m_TestOwn->update();
	if (m_Input->getKeyDown(sf::Keyboard::Space))
		m_TestEmitter->play();


	m_TestButton->update();
	if (m_TestButton->isClicked())
		Debug::write(EDebugLevel::INFO, "Yoo");


	m_TestEmitter->update();
	return true;
};
void ExampleState::Draw()
{
	m_TestOwn->draw();
	m_TestButton->draw();
	m_Window->getWindow()->draw(*m_TestText);
	m_TestEmitter->draw();
};

void ExampleState::Exit()
{

};

std::string ExampleState::getName()
{
	return m_Name;
};
int ExampleState::getUpdateRequirement()
{
	return m_UpdateRequitement;
};
int ExampleState::getDrawRequirement()
{
	return m_DrawRequitement;
};