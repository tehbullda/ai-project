#pragma once

#include "stdafx.h"
#include "PremadeButton.hpp"
#include "Input.hpp"

namespace kartong
{
	namespace Premade
	{
		class PremadeSlider
		{
		public:
			PremadeSlider(const std::string& p_Text, const float& p_Max);
			~PremadeSlider();

			void update();
			void draw();

			void setPosition(const sf::Vector2f& p_Pos);

			int getPercentage();
			float getValue();
		private:
			void updatePercent();
		private:
			Window* m_Window;
			system::input::Input* m_Input;
		private:
			sf::RectangleShape m_Back;
			sf::RectangleShape m_Track;
			PremadeButton* m_Slider;
			sf::Text* m_Text;
			sf::Text* m_Num;
		private:
			int m_Value;
			sf::Vector2f m_Position;
			float m_MinPos;
			float m_MaxPos;
			bool m_Dirty;
			float m_maxValue;

		};
	}
}