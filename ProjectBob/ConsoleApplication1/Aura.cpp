#include "stdafx.h"
#include "Aura.hpp"
#include "Icon.hpp"

namespace AIGame {
#define ticktime 1.0f
#define lineardamageincrease 5
#define exponentialdamageincrease 1.5f
	Aura::Aura(const std::string &identifier, const AuraType &type, const bool &dispellable, const float &lifetime, const float &radius)
	{
		m_identifier = identifier;
		SetAuraType(type);
		m_lifetime = m_maxlifetime = lifetime;
		m_radius = radius;
		m_dispellable = dispellable;
		m_power = 0;
		m_ticktimer = 0.0f;
	}

	Aura::Aura(const std::string &identifier, AuraType auratype, EffectType effecttype, const std::string &effectdesc, const unsigned int &power,
		const bool &dispellable, const float &lifetime, const float &radius) {
		m_identifier = identifier;
		m_auratype = auratype;
		m_effect.m_type = effecttype;
		m_effect.m_desc = effectdesc;
		m_dispellable = dispellable;
		m_lifetime = m_maxlifetime = lifetime;
		m_radius = radius;
		m_power = power;
		m_ticktimer = 0.0f;
	}

	Aura::~Aura()
	{
		if (m_icon) {
			delete m_icon;
			m_icon = nullptr;
		}
	}

	bool Aura::Update() {
		m_ticktimer += kartong::DeltaTime::getDeltaTime();
		if (m_ticktimer >= ticktime) {
			m_tick = true;
			m_ticktimer = 0.0f;
			if (m_effect.m_type == LinearDamageOverTime) {
				m_power += lineardamageincrease;
			}
			else if (m_effect.m_type == ExponentialDamageOverTime) {
				m_power = static_cast<unsigned int>((double)m_power * exponentialdamageincrease);
			}
		}
		if (m_auratype != AuraType::Global) {
			if (m_lifetime <= 0.0f) {
				return true;
			}
			else {
				m_lifetime -= kartong::DeltaTime::getDeltaTime();
			}
		}
		return false;
	}

	void Aura::DrawIcon() {
		m_icon->Draw();
	}
	
	void Aura::DrawEffect() {

	}

	void Aura::SetIcon(const std::string &filename) {
		if (m_icon) {
			delete m_icon;
		}
		m_icon = new Icon(filename, sf::Vector2f(0, 0));
		SetIconTooltip(m_effect.m_desc);
	}

	void Aura::SetIconPos(const sf::Vector2f &pos) {
		if (m_icon == nullptr) {
			return;
		}
		m_icon->SetPosition(pos);
		m_icon->GetEditableTooltip().SetTriggerArea((sf::IntRect(m_icon->GetGlobalRect())));
	}

	void Aura::SetIconTooltip(const std::string &text) {
		m_icon->SetTooltip(text);
	}

	std::string Aura::GetIdentifier() {
		return m_identifier;
	}

	Aura::AuraType Aura::GetAuraType() {
		return m_auratype;
	}

	std::string Aura::GetAuraTypeAsString() {
		switch (m_auratype) {
		case Debuff: return "Debuff";
		case Buff: return "Buff";
		}
		return "Global";
	}

	Aura::Effect Aura::GetEffect() {
		return m_effect;
	}

	std::string Aura::GetEffectTypeAsString() {
		switch (m_effect.m_type) {
		case DamageOverTime: return "DamageOverTime";
		case LinearDamageOverTime: return "DamageOverTime";
		case ExponentialDamageOverTime: return "ExponentialDamageOverTime";
		case HealingOverTime: return "HealingOverTime";
		case DamageMultiplier: return "DamageMultiplier";
		case DamageIncrease: return "DamageIncrease";
		case DamageDecrease: return "DamageDecrease";
		case DamageBlock: return "DamageBlock";
		case HPIncrease: return "HPIncrease";
		case HPDecrease: return "HPDecrease";
		case Stun: return "Stun";
		case Slow: return "Slow";
		default: return "Default";
		}
	}

	std::string Aura::GetEffectDesc() {
		return m_effect.m_desc;
	}

	unsigned int Aura::GetPower() {
		return m_power;
	}

	float Aura::GetLifetime() {
		return m_lifetime;
	}

	float Aura::GetMaxLifetime() {
		return m_maxlifetime;
	}

	float Aura::GetRadius() {
		return m_radius;
	}

	bool Aura::GetDispellable() {
		return m_dispellable;
	}

	bool Aura::Ticked() {
		if (m_tick) {
			m_tick = false;
			return true;
		}
		return false;
	}

	void Aura::SetDispellable(const bool &status) {
		m_dispellable = status;
	}

	void Aura::SetAuraType(const AuraType &type) {
		m_auratype = type;
	}

	void Aura::SetEffect(const EffectType &type, const std::string &text) {
		m_effect.m_type = type;
		m_effect.m_desc = text;
	}

	void Aura::SetEffectDesc(const std::string &text) {
		m_effect.m_desc = text;
	}

	void Aura::SetPower(unsigned int &power) {
		m_power = power;
	}
}