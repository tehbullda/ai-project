#include "stdafx.h"

#include "LoadingState.hpp"
#include "ServiceLocator.hpp"
#include "Randomizer.hpp"

using namespace kartong;
using namespace AIGame;
LoadingState::LoadingState(const std::string& p_Name)
{
	m_StateManager = ServiceLocator<kartong::StateManager>::get_service();
	m_TextureManager = ServiceLocator<TextureManager>::get_service();
	m_FontManager = ServiceLocator<FontManager>::get_service();

	m_Name = p_Name;
	m_UpdateRequitement = StateRequirement::ALWAYS;
	m_DrawRequitement = StateRequirement::ALWAYS;
};
LoadingState::~LoadingState()
{
	m_TextureManager = nullptr;
	m_FontManager = nullptr;
	m_StateManager = nullptr;

};

bool LoadingState::Initialize()
{
	
	return true;
};
bool LoadingState::Update()
{
	m_TextureManager->preloadTexture("GUIHUD/Resume.png");
	m_TextureManager->preloadTexture("GUIHUD/Options.png");
	m_TextureManager->preloadTexture("GUIHUD/Menu.png");
	m_TextureManager->preloadTexture("GUIHUD/TextChat_Button.png");
	
	handleStates();
	return true;
};
void LoadingState::Draw()
{

};

void LoadingState::Exit()
{
	m_FontManager = nullptr;
	m_TextureManager = nullptr;
};

std::string LoadingState::getName()
{
	return m_Name;
};
int LoadingState::getUpdateRequirement()
{
	return m_UpdateRequitement;
};
int LoadingState::getDrawRequirement()
{
	return m_DrawRequitement;
};

void LoadingState::handleStates()
{
	m_StateManager->activate("MenuState");
	m_StateManager->deactivate("LoadingState");
};
