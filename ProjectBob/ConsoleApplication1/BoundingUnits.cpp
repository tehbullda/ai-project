#include "stdafx.h"
#include "BoundingUnits.hpp"

using namespace kartong;

#pragma once

#include "stdafx.h"


BoundingUnit::BoundingUnit(const sf::Vector2f& p_xPosOffset)
{
	m_xPosOffset = p_xPosOffset;
};
BoundingUnit::~BoundingUnit()
{

};
void BoundingUnit::setPosition(const sf::Vector2f& p_Pos)
{
	m_Position = p_Pos + m_xPosOffset;
	m_Dirty = true;
};

sf::Vector2f BoundingUnit::GetPos()
{
	return m_Position;
};

BoundingTypes BoundingUnit::GetType()
{
	return m_Type;
};


//--XXXXXXXXXXXXXXX--
//--XX-----------XX--
//--XX-----------XX--
//--XX-----------XX--
//--XX-----------XX--
//--XX-----------XX--
//--XXXXXXXXXXXXXXX--

AxisAlignedBoundingBox::AxisAlignedBoundingBox(const sf::Vector2f& p_Pos, const sf::Vector2f& p_Vol, const sf::Vector2f& p_xPosOffset) : BoundingUnit(p_xPosOffset)
{
	m_Position = p_Pos + p_xPosOffset;
	m_Volume = p_Vol;
	m_Type = BoundingTypes::BOX;
	m_Dirty = true;
};
AxisAlignedBoundingBox::~AxisAlignedBoundingBox()
{

};
void AxisAlignedBoundingBox::recalculate()
{
	m_Box.left = m_Position.x;
	m_Box.top = m_Position.y;
	m_Box.width = m_Volume.x;
	m_Box.height = m_Volume.y;
};
void AxisAlignedBoundingBox::setVolume(const sf::Vector2f& p_Vol)
{
	m_Volume = p_Vol;
};

bool AxisAlignedBoundingBox::pointIntersects(const sf::Vector2f& p_Point)
{
	if (m_Dirty)
		recalculate();
	return m_Box.contains(p_Point);
};
bool AxisAlignedBoundingBox::AABBIntersects(const sf::Vector2f& p_Pos, const sf::Vector2f& p_Vol)
{
	if (m_Dirty)
		recalculate();

	sf::FloatRect other;
	other.left = p_Pos.x;
	other.top = p_Pos.y;
	other.width = p_Vol.x;
	other.height = p_Vol.y;

	return m_Box.intersects(other);
};
bool AxisAlignedBoundingBox::circleIntersects(const sf::Vector2f& p_Pos, const float& p_Rad)
{
	if (m_Dirty)
		recalculate();
	sf::Vector2f point = p_Pos;

	if (point.x > m_Box.left)
		point.x = m_Box.left;
	if (point.x < m_Box.left + m_Box.width)
		point.x = m_Box.left + m_Box.width;
	if (point.y > m_Box.top)
		point.y = m_Box.top;
	if (point.y < m_Box.top + m_Box.height)
		point.y = m_Box.top + m_Box.height;

	if (Math::distance(point, p_Pos) < p_Rad)
		return true;
	return false;
};
void AxisAlignedBoundingBox::rayIntersects(const sf::Vector2f& p_Pos, const sf::Vector2f& p_Direction)
{
	p_Pos;
	p_Direction;
};

BoundingTypes AxisAlignedBoundingBox::GetType()
{
	return m_Type;
}

//------XXXXXXX------
//----XX-------XX----
//--XX-----------XX--
//--XX-----------XX--
//--XX-----------XX--
//----XX-------XX----
//------XXXXXXX------

BoundingSphere::BoundingSphere(const sf::Vector2f& p_Pos, const float& p_Rad, const sf::Vector2f& p_xPosOffset) : BoundingUnit(p_xPosOffset)
{
	m_Position = p_Pos + p_xPosOffset;
	m_Radius = p_Rad;
	m_Type = BoundingTypes::SPHERE;
	m_Dirty = true;
};
BoundingSphere::~BoundingSphere()
{

};
void BoundingSphere::recalculate()
{

};
void BoundingSphere::setRadius(const float& p_Rad)
{
	m_Radius = p_Rad;
};

bool BoundingSphere::pointIntersects(const sf::Vector2f& p_Point)
{
	if (Math::distance(p_Point, m_Position) < m_Radius)
		return true;
	return false;
};
bool BoundingSphere::AABBIntersects(const sf::Vector2f& p_Pos, const sf::Vector2f& p_Vol)
{
	if (m_Dirty)
		recalculate();

	sf::Vector2f point = m_Position;

	if (point.x > p_Pos.x)
		point.x = p_Pos.x;
	if (point.x < p_Pos.x + p_Vol.x)
		point.x = p_Pos.x + p_Vol.x;
	if (point.y > p_Pos.y)
		point.y = p_Pos.y;
	if (point.y < p_Pos.y + p_Vol.y)
		point.y = p_Pos.y + p_Vol.y;

	if (Math::distance(point, m_Position) < m_Radius)
		return true;
	return false;
};
bool BoundingSphere::circleIntersects(const sf::Vector2f& p_Pos, const float& p_Rad)
{
	if (Math::distance(m_Position, p_Pos) < (p_Rad + m_Radius))
		return true;
	return false;
};
void BoundingSphere::rayIntersects(const sf::Vector2f& p_Pos, const sf::Vector2f& p_Direction)
{
	p_Direction;
	p_Pos;
};

float BoundingSphere::GetRadius()
{
	return m_Radius;
};

BoundingTypes BoundingSphere::GetType()
{
	return m_Type;
}