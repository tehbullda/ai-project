#pragma once

#include "stdafx.h"
#include "TextureManager.hpp"
#include "Sprite.hpp"
#include "ParticleEmitter.hpp"
#include "Input.hpp"
#include "TextChat.hpp"
#include "Grid.hpp"
#include "GameObjectManager.hpp"
#include "AuraManager.hpp"
#include "BlackBoard.hpp"

//
#include "GraphWindow.hpp"
#include "Boss.hpp"
#include "NameHandler.hpp"
#include "PremadeSlider.hpp"
#include "PremadeTickbox.hpp"
#include "PostGameStats.hpp"
namespace AIGame
{
	class PostGameState : public kartong::State
	{
	public:
		PostGameState(const std::string& p_Name);
		~PostGameState();


		bool Initialize();
		bool Update();
		void Draw();
		void Exit();



		std::string getName();
		int getUpdateRequirement();
		int getDrawRequirement();

	private:
		void handleStates();
	private:
		std::string m_Name;
		int m_DrawRequirement;
		int m_UpdateRequirement;
	private:
		AuraManager *m_AuraManager;

		kartong::StateManager* m_StateManager;
		kartong::TextureManager* m_TextureManager;
		kartong::FontManager* m_FontManager;
		kartong::system::input::Input* m_Input;
		kartong::Window* m_Window;

		AIGame::PostGameStats* m_PostStats;

		int bob;
	private:
		std::vector<sf::Text*> m_Texts;
		kartong::Premade::PremadeButton* m_MenuButton;
	private:

	};
}