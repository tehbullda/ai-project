#pragma once

#include "stdafx.h"

namespace kartong
{
	class GraphWindow
	{
	private:
		GraphWindow();
	public:
		~GraphWindow();
		typedef std::unique_ptr<GraphWindow> Ptr;
		static Ptr Create();

		void setPosition(const sf::Vector2f& p_Pos);

		void update();
		void draw();

		void addValue(const float& p_Val);
		sf::Vector2f getSize();
	private:
		void drawGraph();
		void updatePositions();
		sf::Vector2f getVecPos(const float& p_Val, const float& p_Max, const float& p_Iter);
		int retroGetHighest();
		
	private:
		Window* m_Window;
		FontManager* m_FontManager;
		system::input::Input* m_Input;
	private:
		sf::RectangleShape* m_Background;
		sf::RectangleShape* m_GraphBack;

		sf::Text* m_Low;
		sf::Text* m_High;
		

		sf::RectangleShape* m_Point;
		sf::Vector2f m_Position;
		sf::Vector2f m_Size;
		float m_Padding;
		std::vector<float> m_DotPositions;
		
		float m_Highest;

		float temptester;

	};
}