#include "stdafx.h"

#include "KeyBindings.hpp"
#include "Input.hpp"
#include "ServiceLocator.hpp"
#include "Utils.hpp"

#include <fstream>
#include <ostream>


using namespace kartong::system::input;
KeyBindings::KeyBindings()
{
	m_Input = ServiceLocator<Input>::get_service();
};

KeyBindings::~KeyBindings()
{
	m_Input = nullptr;
};


KeyBindings::Ptr KeyBindings::Create()
{
	return KeyBindings::Ptr(new KeyBindings());
}

bool KeyBindings::getAction(const std::string& p_Action)
{
	if (m_Input->getKey(m_Binds.find(p_Action)->second))
		return true;
	return false;
};
bool KeyBindings::getActionDown(const std::string& p_Action)
{
	if (m_Input->getKeyDown(m_Binds.find(p_Action)->second))
		return true;
	return false;
};
bool KeyBindings::getActionReleased(const std::string& p_Action)
{
	if (m_Input->getKeyReleased(m_Binds.find(p_Action)->second))
		return true;
	return false;
};
void KeyBindings::bindKey(const std::string& p_Action, const int& p_Key)
{
	m_Binds.insert(std::pair<std::string, int>(p_Action, p_Key));
};
void KeyBindings::unbindAction(const int& p_Action)
{
	p_Action;
	Debug::write(EDebugLevel::INFO, "KeyBindings::unbindAction() does nothing atm");
};

void KeyBindings::loadBinds(const std::string& p_File)
{
	p_File;
};
void KeyBindings::saveBinds(const std::string& p_File)
{
	std::ofstream output;
	output.open(p_File);

	

	
	/*output << "Action" << " = " << std::to_string(7) << std::endl;
	output << "Action" << " = " << std::to_string(9) << std::endl;
	output << "Action" << " = " << std::to_string(11) << std::endl;
	output << "Action" << " = " << std::to_string(74123) << std::endl;*/

	output.close();
};
void KeyBindings::setDefaultBinds()
{

};