//Tasks.cpp

#include "stdafx.h"
#include "Tasks.hpp"
#include "Grid.hpp"
#include "Agent.hpp"
#include "Aura.hpp"
#include "GameObjectManager.hpp"
#include "AuraManager.hpp"

#include "ServiceLocator.hpp"

#include <random>

/*----------------------Walk In Range Of Boss----------------------*/
WalkinRangeOfBoss::WalkinRangeOfBoss(AIGame::Agent* p_xOwner)
{
	m_xOwner = p_xOwner;
}

WalkinRangeOfBoss::~WalkinRangeOfBoss()
{
	m_xPath_iter = m_xPath.begin();
	while (m_xPath_iter != m_xPath.end())
	{
		m_xPath_iter = m_xPath.erase(m_xPath_iter);
	}
	m_xPath.clear();
}

bool WalkinRangeOfBoss::IsInRange()
{
	//a2 + b2 = c2
	float xDiff = pow(abs(m_xTargetLocation.x - m_xOwner->GetPos().x), 2.0f);
	float yDiff = pow(abs(m_xTargetLocation.y - m_xOwner->GetPos().y), 2.0f);

	if (sqrt(xDiff + yDiff) < m_xOwner->GetRange())
		return true;

	return false;
}

BTNode::StateType WalkinRangeOfBoss::Update()
{
	if (m_eState == Running)
	{
		if (m_xBossPrevPos != m_xOwner->m_xBB->GetBossPos())
			m_eState = Failure;
		else
		{
			if (IsInRange())//in range of boss
			{
				m_eState = Success;
			}
			else
			{
				if (m_xPath_iter == m_xPath.begin())
				{
					AIGame::Node* node = *m_xPath_iter;
					sf::Vector2f tempvec;
					tempvec = kartong::ServiceLocator<AIGame::Grid>::get_service()->getPixelPos(node);

					//move char
					m_xOwner->SetPosAndOccupied(tempvec);

					m_xCurrentGridLocation = kartong::ServiceLocator<AIGame::Grid>::get_service()->getGridPos(m_xOwner->GetPos());
					if (m_xCurrentGridLocation == m_xTargetGridLocation)
						m_eState = Success;
					else
						m_eState = Failure;
				}

				else
				{
					AIGame::Node* node = *m_xPath_iter;
					sf::Vector2f tempvec;
					tempvec = kartong::ServiceLocator<AIGame::Grid>::get_service()->getPixelPos(node);

					//float xDiff = pow(abs(tempvec.x - m_xOwner->GetPos().x), 2.0f);
					//float yDiff = pow(abs(tempvec.y - m_xOwner->GetPos().y), 2.0f);
					////move char
					//if (sqrt(xDiff + yDiff) > 200)
					//{
					//	/*std::cout << "player pos: " << m_xOwner->GetPos().x / 48 << ", " << m_xOwner->GetPos().y / 48 << std::endl;
					//	std::cout << "player to pos: " << tempvec.x / 48 << ", " << tempvec.y / 48 << std::endl;
					//	std::cout << "first in path: " << m_xPath[0]->getGridPosition().x << ", " << m_xPath[0]->getGridPosition().y << std::endl;
					//	std::cout << "last in path: " << m_xPath[m_xPath.size() - 1]->getGridPosition().x << ", " << m_xPath[m_xPath.size() - 1]->getGridPosition().y << std::endl;*/
					//	std::cout << "FAAAAIIIILLLL" << std::endl;
					//}

					//move char
					m_xOwner->SetPosAndOccupied(tempvec);
					--m_xPath_iter;
				}
			}
		}
	}
	
	return m_eState;
}

void WalkinRangeOfBoss::OnInitialize()
{
	m_xPath.clear();
	m_xPath_iter = m_xPath.end();
	m_xTargetLocation = m_xOwner->m_xBB->GetBossPos();
	m_xBossPrevPos = m_xOwner->m_xBB->GetBossPos();

	if (m_xOwner->GetPos().x > m_xTargetLocation.x)
		if (m_xOwner->GetPos().x < m_xTargetLocation.x + 128)
			m_xTargetLocation.x = m_xOwner->GetPos().x;
		else
			m_xTargetLocation.x = m_xTargetLocation.x + 128;
	if (m_xOwner->GetPos().y > m_xTargetLocation.y)
		if (m_xOwner->GetPos().y < m_xTargetLocation.y + 128)
			m_xTargetLocation.y = m_xOwner->GetPos().y;
		else
			m_xTargetLocation.y = m_xTargetLocation.y + 128;


	m_xCurrentGridLocation = kartong::ServiceLocator<AIGame::Grid>::get_service()->getGridPos(m_xOwner->GetPos());
	m_xTargetGridLocation = kartong::ServiceLocator<AIGame::Grid>::get_service()->getGridPos(m_xTargetLocation);
	
	if (m_xCurrentGridLocation == m_xTargetGridLocation || IsInRange())
	{
		m_eState = Success;
	}
	else
	{
		//use a* to find the path
		m_xPath = kartong::ServiceLocator<AIGame::Grid>::get_service()->getPath(
			kartong::ServiceLocator<AIGame::Grid>::get_service()->getNode(m_xCurrentGridLocation.x, m_xCurrentGridLocation.y),
			kartong::ServiceLocator<AIGame::Grid>::get_service()->getNode(m_xTargetGridLocation.x, m_xTargetGridLocation.y));
		m_xPath_iter = m_xPath.end();

		if (m_xPath.size() < 1)
			m_eState = Failure;
		else
		{
			if (m_xPath.end() != m_xPath.begin())
				--m_xPath_iter;
			m_eState = Running;
		}
	}
}

void WalkinRangeOfBoss::OnTerminate(StateType p_eState)
{
	m_xPath_iter = m_xPath.begin();
	while (m_xPath_iter != m_xPath.end())
	{
		m_xPath_iter = m_xPath.erase(m_xPath_iter);
	}
	m_xPath.clear();
}


/*----------------------Attack Boss----------------------*/

AttackBoss::AttackBoss(AIGame::Agent* p_xOwner)
{
	m_xOwner = p_xOwner;
}

AttackBoss::~AttackBoss()
{

}

BTNode::StateType AttackBoss::Update()
{
	//create projectile or hit melee or whatever right noe print attack

	if (m_xOwner->CanAttack())
	{
		sf::Vector2f target = m_xOwner->m_xBB->GetBossPos() + sf::Vector2f(64.0f, 64.0f);
		sf::Vector2f startPos = m_xOwner->GetPos() + sf::Vector2f(8.0f, 8.0f);
		kartong::ServiceLocator<AIGame::GameObjectManager>::get_service()->CreateProjectile(startPos, target, m_xOwner->GetDamage());
		m_eState = Success;
	}
	else
		m_eState = Failure;
	
	return m_eState;
}

void AttackBoss::OnInitialize()
{
	m_xBossPos = m_xOwner->m_xBB->GetBossPos();
	m_eState = Running;
}

void AttackBoss::OnTerminate(StateType p_eState)
{

}


/*----------------------Move Away----------------------*/

MoveAway::MoveAway(AIGame::Agent* p_xOwner)
{
	m_xOwner = p_xOwner;
	m_xGrid = kartong::ServiceLocator<AIGame::Grid>::get_service();
	m_fTileSize = m_xGrid->GetTileSize();
	m_xGridSize = m_xGrid->getGridSize();
}

MoveAway::~MoveAway()
{

}

BTNode::StateType MoveAway::Update()
{
	if (m_eState == Running)
	{
		sf::Vector2i gridPos = kartong::ServiceLocator<AIGame::Grid>::get_service()->getGridPos(m_xOwner->GetPos());
		AIGame::Node* node = kartong::ServiceLocator<AIGame::Grid>::get_service()->getNode(gridPos.x, gridPos.y);
		std::vector<AIGame::Node*> neighbours = kartong::ServiceLocator<AIGame::Grid>::get_service()->getNeighbours(node);


		auto itr = neighbours.begin();
		while (itr != neighbours.end())
		{
			if ((*itr)->getType() == AIGame::NODETYPE::WALL)
				itr = neighbours.erase(itr);
			else
				++itr;
		}
		if (neighbours.size() < 1)
			m_eState = Failure;
		else
		{
			int index = rand() % neighbours.size();

			sf::Vector2f newPos = sf::Vector2f(neighbours[index]->getGridPosition() * 48);

			m_xOwner->SetPosAndOccupied(newPos);

			m_eState = Success;
		}
	}
	return m_eState;
}

void MoveAway::OnInitialize()
{
	//check if in shit
	sf::Vector2i currentGridLocation = kartong::ServiceLocator<AIGame::Grid>::get_service()->getGridPos(m_xOwner->GetPos());
	AIGame::Node* node = kartong::ServiceLocator<AIGame::Grid>::get_service()->getNode(currentGridLocation.x, currentGridLocation.y);
	
	if (node->getType() == AIGame::HAZARDS || node->getType() == AIGame::INCOMINGHAZARD || node->GetOccupiedBy() != m_xOwner->GetName())
		m_eState = Running;
	else
		m_eState = Failure;
}

void MoveAway::OnTerminate(BTNode::StateType p_eState)
{

}


/*----------------------Dispell----------------------*/

Dispell::Dispell(AIGame::Agent* p_xOwner)
{
	m_xOwner = p_xOwner;
}

Dispell::~Dispell()
{

}

BTNode::StateType Dispell::Update()
{
	if (m_eState == Running)
	{
		kartong::ServiceLocator<AIGame::AuraManager>::get_service()->DispelTarget(m_xOwner);		

		std::vector<AIGame::Aura*> auras = m_xOwner->GetAuras();

		std::vector<AIGame::Aura*> m_xAurasLeft;
		
		/*std::cout << "auras removed by: " << tempOwner->GetName() << std::endl;
		for (int i = 0; i < m_xAurasToDispel.size(); i++)
		{
			std::cout << m_xAurasToDispel[i]->GetIdentifier() << std::endl;
		}
		std::cout << "Auras to remove: " << std::endl;*/

		auto itr = auras.begin();
		while (itr != auras.end())
		{
			if ((*itr)->GetAuraType() == AIGame::Aura::AuraType::Buff)
				continue;
			else if ((*itr)->GetAuraType() == AIGame::Aura::AuraType::Debuff)
			{
				if (m_xOwner->m_xBB->CanDispellAura(*itr) == BlackBoard::DispellableTypes::Unknown
					|| m_xOwner->m_xBB->CanDispellAura(*itr) == BlackBoard::DispellableTypes::Dispellable)
				{
					m_xAurasLeft.push_back((*itr));
				}
			}

			++itr;
		}

		bool left = false;
		auto afterItr = m_xAurasToDispel.begin();
		while (afterItr != m_xAurasToDispel.end())
		{
			left = false;
			auto leftItr = m_xAurasLeft.begin();
			while (leftItr != m_xAurasLeft.end())
			{
				if ((*leftItr) == (*afterItr))
				{
					left = true;
					break;
				}
				++leftItr;
			}

			m_xOwner->m_xBB->SetAuraDispellable((*afterItr), !left);
			++afterItr;
		}

		m_eState = Success;
	}
	return m_eState;
}

void Dispell::OnInitialize()
{

	m_xAurasToDispel.clear();
	if (m_xOwner->HasAura())
	{
		int result = rand() % 5;
		std::cout << result << std::endl;
		if (result == 0)
		{
			std::vector<AIGame::Aura*> auras = m_xOwner->GetAuras();

			auto itr = auras.begin();
			while (itr != auras.end())
			{
				if ((*itr)->GetAuraType() == AIGame::Aura::AuraType::Buff)
					continue;
				else if ((*itr)->GetAuraType() == AIGame::Aura::AuraType::Debuff)
				{
					if (m_xOwner->m_xBB->CanDispellAura((*itr)) == BlackBoard::DispellableTypes::Unknown
						|| m_xOwner->m_xBB->CanDispellAura((*itr)) == BlackBoard::DispellableTypes::Dispellable)
					{
						m_xAurasToDispel.push_back((*itr));
					}
				}
				++itr;
			}
		}
	}
	else
		m_eState = Failure;

	if (m_xAurasToDispel.empty())
		m_eState = Failure;
	else
		m_eState = Running;
}

void Dispell::OnTerminate(BTNode::StateType p_eState)
{

}