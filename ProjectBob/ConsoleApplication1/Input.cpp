#include "stdafx.h"

#include "Input.hpp"
#include "ServiceLocator.hpp"

using namespace kartong::system::input;


Input::Input()
{
	m_Window = ServiceLocator<Window>::get_service();
	
};
Input::~Input()
{
	m_Window = nullptr;
};

Input::Ptr Input::Create()
{
	return Input::Ptr(new Input());
}

void Input::initialize()
{
	setAll(false);
};
void Input::update()
{
	checkKeys();
	checkMousePos();
};

bool Input::getKey(const int& p_Key)
{
	return m_Keys[p_Key];
};
bool Input::getKeyDown(const int& p_Key){
	if (m_Keys[p_Key] && !m_PrevKeys[p_Key])
		return true;
	return false;
};
bool Input::getKeyReleased(const int& p_Key){
	if (!m_Keys[p_Key] && m_PrevKeys[p_Key])
		return true;
	return false;
};
sf::Vector2f Input::getMousePos()
{
	return sf::Vector2f((float)m_MousePos.x, (float)m_MousePos.y);
};

void Input::setAll(const bool& p_All)
{
	for (int i = 0; i < 100; i++)
	{
		m_Keys[i] = p_All;
		m_PrevKeys[i] = p_All;
	}
	for (int i = 0; i < 5; i++)
	{
		m_MouseKeys[i] = p_All;
		m_PrevMouseKeys[i] = p_All;
	}
};
bool Input::getMouse(const int& p_Key)
{
	//if (m_MouseKeys[p_Key])
	//	int i = 0;
	return m_MouseKeys[p_Key];
};
bool Input::getMouseDown(const int& p_Key)
{
	if (!m_PrevMouseKeys[p_Key] && m_MouseKeys[p_Key])
		return true;
	return false;
};
bool Input::getMouseReleased(const int& p_Key)
{
	if (m_PrevMouseKeys[p_Key] && !m_MouseKeys[p_Key])
		return true;
	return false;
};
bool Input::getAnyKey()
{
	for (int i = 0; i < 100; i++)
		if (m_Keys[i])
			return true;
	for (int i = 0; i < 5; i++)
		if (m_MouseKeys[i])
			return true;
	return false;
};
void Input::checkKeys()
{
	sf::Event Event;

	for (int i = 0; i < 100; i++) {
		m_PrevKeys[i] = m_Keys[i];
	}
	for (int i = 0; i < 5; i++){
		m_PrevMouseKeys[i] = m_MouseKeys[i];
	}
	

	while (m_Window->getWindow()->pollEvent(Event))
	{
		if (Event.type == sf::Event::Closed)
			Engine::engine_shut();
		if (Event.type == sf::Event::KeyPressed)
			m_Keys[Event.key.code] = true;
		if (Event.type == sf::Event::KeyReleased)
			m_Keys[Event.key.code] = false;
		if (Event.type == sf::Event::MouseButtonPressed)
			m_MouseKeys[Event.mouseButton.button] = true;
		if (Event.type == sf::Event::MouseButtonReleased)
			m_MouseKeys[Event.mouseButton.button] = false;

	}
};
void Input::checkMousePos()
{
	m_MousePos = sf::Mouse::getPosition(*m_Window->getWindow());

};