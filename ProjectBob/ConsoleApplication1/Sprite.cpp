
#include "stdafx.h"
#include "Sprite.hpp"
#include "TextureManager.hpp"
#include "ServiceLocator.hpp"
#include <fstream>
#include <sstream>

using namespace kartong;


Sprite::Sprite(sf::Texture* p_Tex)
{
	m_TextureManager = ServiceLocator<TextureManager>::get_service();
	m_Window = ServiceLocator<Window>::get_service();
	m_Animated = false;
	m_Sprite = new sf::Sprite(*p_Tex);

};
Sprite::Sprite(const std::string& p_File)
{
	m_TextureManager = ServiceLocator<TextureManager>::get_service();
	m_Window = ServiceLocator<Window>::get_service();
	m_Animated = true;
	m_Duration = 0.0f;
	m_Timer = 0.0f;

	std::ifstream stream;
	//std::stringstream ss;
	//std::string row;
	std::string filename;

	stream.open(p_File);
	if (stream.is_open())
	{

		stream >> filename;
		stream >> m_Frames;
		stream >> m_Duration;

		for (int i = 0; i < m_Frames; i++)
		{
			Vector2* Pos = new Vector2();
			Vector2* Size = new Vector2();

			stream >> Size->m_x >> Size->m_y >> Pos->m_x >> Pos->m_y;
			m_Size.push_back(Size);
			m_Pos.push_back(Pos);
		}
	}
	else
	{
		m_Animated = false;
		m_Sprite = new sf::Sprite(*m_TextureManager->getMissingTexture());
		return;
	}
	m_Sprite = new sf::Sprite(*m_TextureManager->loadTexture(filename));
	m_Rect.width = (int)m_Size[0]->m_x;
	m_Rect.height = (int)m_Size[0]->m_y;
	m_Rect.left = (int)m_Pos[0]->m_x;
	m_Rect.top = (int)m_Pos[0]->m_y;
	m_Sprite->setTextureRect(m_Rect);
};

Sprite::~Sprite()
{
	for (unsigned int i = 0; i < m_Pos.size(); i++){
		delete m_Pos[i];
		m_Pos[i] = nullptr;
	}
	for (unsigned int i = 0; i < m_Size.size(); i++){
		delete m_Size[i];
		m_Size[i] = nullptr;
	}
	delete m_Sprite;
	m_Sprite = nullptr;
}

void Sprite::update()
{
	if (!m_Animated)
		return;

	if (m_CurrentFrame >= m_Frames)
		m_CurrentFrame = 0;

	m_Timer += DeltaTime::getDeltaTime();
	if (m_Timer > m_Duration)
	{
		m_Rect.width = (int)m_Size[m_CurrentFrame]->m_x;
		m_Rect.height = (int)m_Size[m_CurrentFrame]->m_y;
		m_Rect.left = (int)m_Pos[m_CurrentFrame]->m_x;
		m_Rect.top = (int)m_Pos[m_CurrentFrame]->m_y;
		m_Sprite->setTextureRect(m_Rect);
		m_CurrentFrame++;
		m_Timer = 0.0f;
	}
}

void Sprite::draw()
{
	m_Window->getWindow()->draw(*m_Sprite);
}

sf::FloatRect Sprite::getGlobalBounds()
{
	return m_Sprite->getGlobalBounds();
};
sf::FloatRect Sprite::getLocalBounds()
{
	return m_Sprite->getLocalBounds();
};

void Sprite::setPosition(const sf::Vector2f& p_Pos)
{
	m_Sprite->setPosition(p_Pos);
};
void Sprite::setPosition(const float& p_X, const float& p_Y)
{
	m_Sprite->setPosition(p_X, p_Y);
};
sf::Vector2f Sprite::getPosition()
{
	return m_Sprite->getPosition();
};
void Sprite::setOrigin(const sf::Vector2f& p_Point)
{
	m_Sprite->setOrigin(p_Point);
};
void Sprite::setOrigin(const float& p_X, const float& p_Y)
{
	m_Sprite->setOrigin(sf::Vector2f(p_X, p_Y));
}
void Sprite::setRotation(const float& p_Rot)
{
	m_Sprite->setRotation(p_Rot);
};
float Sprite::getRotation()
{
	return m_Sprite->getRotation();
};
void Sprite::setScale(const float& p_X, const float& p_Y)
{
	m_Sprite->setScale(p_X, p_Y);
}
void Sprite::setColor(sf::Color p_Color)
{
	m_Sprite->setColor(p_Color);
};
void Sprite::setColor(float p_R, float p_B, float p_G, float p_A)
{
	m_Sprite->setColor(sf::Color((sf::Uint8)p_R, (sf::Uint8)p_B, (sf::Uint8)p_G, (sf::Uint8)p_A));
};
sf::Color Sprite::getColor()
{
	return m_Sprite->getColor();
};
void Sprite::SetTextureRect(const sf::IntRect& p_Rect)
{
	m_Sprite->setTextureRect(p_Rect);
};