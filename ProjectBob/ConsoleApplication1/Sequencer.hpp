//Sequencer.hpp
// do a then b then c..

#pragma once
#include "stdafx.h"
#include "BTNode.hpp"

class Sequencer : public BTNode {
public:
	Sequencer(AIGame::Agent* p_xOwner);
	~Sequencer();

	void AddChild(BTNode* p_xNode);

protected:
	StateType Update();

	void OnInitialize();
	void OnTerminate(StateType p_eState);

private:
	std::vector<BTNode*>::iterator m_xCurrentChild;
	std::vector<BTNode*> m_xChildren;
};

/*Different sequencers*/