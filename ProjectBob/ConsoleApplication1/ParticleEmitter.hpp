
#pragma once

#include "stdafx.h"
#include "Particle.hpp"



namespace kartong
{
	enum MovementState
	{
		RANDOM,
		AWAY,
		TOWARD
	};
	class ParticleEmitter
	{
	public:
		ParticleEmitter(const std::string& p_Texure);

		~ParticleEmitter();

		void initialize();
		void update();
		void draw();

		void setPosition(const sf::Vector2f& p_Pos);
		void setSpawnRadius(const float& p_Min, const float& p_Max);
		void setSpawnRate(const float& p_SpawnRate);
		void setBurst(const bool& p_Burst, const float& p_PlayTime);
		//Use enum MovementState for this;
		void setMovement(const unsigned& p_Movement);

		void setLifeTime(const float& p_Min, const float& p_Max);
		void setMoveSpeed(const float& p_Min, const float& p_Max);
		void setSetStartRotation(const float& p_Min, const float& p_Max);
		void setColor(const sf::Color& p_Min, const sf::Color& p_Max);
		void setRotationSpeed(const float& p_Min, const float& p_Max);

		
		int getActiveParticles();

		void play();
		void stop();
		void clear();

		bool m_Stopped;

	private:
		void SpawnParticle();
		std::vector<Particle*> m_Particles;


		struct Stats
		{
			std::string p_File;
			Vector2 m_Position;

			float m_MaxMoveSpeed;
			float m_MinMoveSpeed;
			float m_MaxLifeTime;
			float m_MinLifeTime;
			sf::Color m_MaxColor;
			sf::Color m_MinColor;
			float m_MinStartRotation;
			float m_MaxStartRotation;
			float m_MinRotationSpeed;
			float m_MaxRotationSpeed;
		} m_Stats;



		struct EmitterStats
		{
			float m_Timer;
			bool m_Burst;
			bool m_Playing;
			float m_PlayTime;
			float m_PlayTimer;
			float m_SpawnRate;
			float m_MaxSpawnRadius;
			float m_MinSpawnRadius;
			unsigned m_Movement;
		} m_EStats;


	};
}