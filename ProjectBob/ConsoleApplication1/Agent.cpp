//Agent.cpp

#include "stdafx.h"
#include "Agent.hpp"
#include <random>
#include "ServiceLocator.hpp"
#include "PostGameStats.hpp"
#include "Grid.hpp"
#include "BTNode.hpp"
#include "TextChat.hpp"
//#include "GameObject.hpp"

using namespace AIGame;
#define TEXTUREPATH "Tests/agent.png"
#define ATTACK_CD 1.0f

Agent::Agent(sf::Vector2f p_xPos) : GameObject(p_xPos, GameObject::Type_Agent)
{
	SetSprite(TEXTUREPATH);

	m_xCollider = new kartong::BoundingSphere(p_xPos, 8.0f, sf::Vector2f(8.0f, 8.0f));

	m_fDecisionTime = 0.0f;
	m_fAttackCD = 0.0f;

	m_fTimeBetweenDecisions = (rand() % 75 + 25) / 100.0f;
	m_fRange = (float)(rand() % 300 + 48);

	m_LifeTimeTimer = 0.0f;
	m_ShotsFired = 0;
	m_DamageDealt = 0;
	m_DamageTaken = 0;
}

Agent::~Agent()
{
	if (m_fHP <= 0.0f){
		m_xNameTag->m_done = true;
		kartong::ServiceLocator<TextChat>::get_service()->sendMessage(m_xNameTag->getName(), kartong::ServiceLocator<TextChat>::get_service()->getLine(TextChat::LineTypes::Line_Death));
		kartong::ServiceLocator<kartong::AudioManager>::get_service()->playClipAtPoint("sword.wav", GetPos());
		m_xBTRoot = nullptr;

		kartong::ServiceLocator<PostGameStats>::get_service()->reportDeath(m_xNameTag->getName(), m_LifeTimeTimer);
		kartong::ServiceLocator<PostGameStats>::get_service()->reportProjectilesFired(m_xNameTag->getName(), (float)m_ShotsFired);
		kartong::ServiceLocator<PostGameStats>::get_service()->reportDamageGiven(m_xNameTag->getName(), m_DamageDealt);
		kartong::ServiceLocator<PostGameStats>::get_service()->reportDamageTaken(m_xNameTag->getName(), m_DamageTaken);
	}
	GameObject::~GameObject();
}

bool Agent::Update()
{
	m_LifeTimeTimer += kartong::DeltaTime::getDeltaTime();
	GameObject::Update(); //do first in update to get correct deltatime
	if (m_bStunned) {
		return true; //Stuns are removed by the manager so nothing else needs to happen here
	}
	if (m_fAttackCD >= 0.0f)
		m_fAttackCD -= m_fDeltaTime;
	if (m_fDecisionTime <= 0.0f)
	{
		m_fDecisionTime = m_fTimeBetweenDecisions;
		m_xBTRoot->Tick();
		if (m_xBTRoot->GetState() != BTNode::StateType::Running)
			m_xBTRoot->SetState(BTNode::StateType::Invalid);
	}
	else
		m_fDecisionTime -= m_fDeltaTime;

	if (m_fHP <= 0.0f)
		return false;

	m_xNameTag->setPosition(m_xPos - sf::Vector2f(0.0f, 16.0f));
	return true;
}

void Agent::UpdateBB()
{

}

bool Agent::CanAttack()
{
	if (m_fAttackCD <= 0.0f)
	{
		m_DamageDealt += m_fDamage;
		m_ShotsFired++;
		m_fAttackCD = ATTACK_CD;
		return true;
	}
	return false;
}

void Agent::AddBTRoot(BTNode* p_xBTRoot)
{
	m_xBTRoot = p_xBTRoot;
}

float Agent::GetRange()
{
	return m_fRange;
}

float Agent::GetDamage()
{
	return m_fDamage;
}

float Agent::GetHP() {
	return m_fHP;
}

void Agent::SetMovespeed(float ms) {
	m_fMovespeed = ms;
}

float Agent::GetMovespeed() {
	return m_fMovespeed;
}

void Agent::SetStun(bool status) {
	m_bStunned = status;
}

bool Agent::IsStunned() {
	return m_bStunned;
}


void Agent::NotifyDebuff(Aura *aura, bool remove) {
	if (!remove)
		m_auras.push_back(aura);
	else
		for (unsigned i = 0; i < m_auras.size(); i++)
			if (m_auras[i] == aura)
				m_auras.erase(m_auras.begin() + i);
}

void Agent::TakeDamage(const int &damage) {
	m_DamageTaken += damage;
	m_fHP -= damage;
	kartong::Debug::write(kartong::EDebugLevel::INFO, m_xNameTag->getName() + " took " + std::to_string(damage) + "damage");
	m_xNameTag->updateHP(m_fMaxHP, m_fHP);
}

void Agent::SetSettings(float p_fHP, float p_fDamage, unsigned int p_uID, AIGame::NameTag *p_xNameTag)
{
	m_fHP = p_fHP;
	m_fMaxHP = p_fHP;
	m_fDamage = p_fDamage;
	m_uID = p_uID;
	m_xNameTag = p_xNameTag;
}

void Agent::SetDamage(float p_fDamage)
{
	m_fDamage = p_fDamage;
}

void Agent::SetHP(float p_fHP) {
	m_fHP = p_fHP;
}

bool Agent::HasAura()
{
	return !m_auras.empty();
}

std::vector<Aura*> Agent::GetAuras()
{
	return m_auras;
}

std::string Agent::GetName()
{
	return m_xNameTag->getName();
}

void Agent::SetPosAndOccupied(sf::Vector2f p_xNewPos)
{
	m_xPos = p_xNewPos;
	sf::Vector2i currentgridlocation = kartong::ServiceLocator<AIGame::Grid>::get_service()->getGridPos(GetPos());
	kartong::ServiceLocator<AIGame::Grid>::get_service()->getNode(currentgridlocation.x, currentgridlocation.y)->SetOccupiedBy(GetName());
}