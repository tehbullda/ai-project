//GameObject.cpp

#include "stdafx.h"
#include "GameObject.hpp"
#include "ServiceLocator.hpp"
#include "GameObjectManager.hpp"

using namespace AIGame;

GameObject::GameObject(sf::Vector2f p_xPos, ObjectType type)
	:m_xPos(p_xPos),
	m_type(type)
{
	/*m_type = type;*/
	m_xBB = kartong::ServiceLocator<BlackBoard>::get_service();
}

GameObject::~GameObject()
{
	delete m_xSprite;
	m_xSprite = nullptr;

	delete m_xCollider;
	m_xCollider = nullptr;
}

void GameObject::SetPos(sf::Vector2f p_xNewPos)
{
	m_xPos = p_xNewPos;
}
void  GameObject::SetPos(const float& p_NewX, const float& p_NewY)
{
	m_xPos.x = p_NewX;
	m_xPos.y = p_NewY;
};

void GameObject::ChangePos(sf::Vector2f p_xPosToAdd)
{
	m_xPos += p_xPosToAdd;
}

void  GameObject::ChangePos(const float& p_AddX, const float& p_AddY)
{
	m_xPos.x += p_AddX;
	m_xPos.y += p_AddY;
};

void GameObject::SetSprite(const std::string& p_File)
{
	if (!m_xSprite)
		m_xSprite = new kartong::Sprite(kartong::ServiceLocator<kartong::TextureManager>::get_service()->loadTexture(p_File));
}

bool GameObject::Update()
{
	m_fDeltaTime = kartong::DeltaTime::getDeltaTime();
	m_xSprite->setPosition(m_xPos);
	m_xCollider->setPosition(m_xPos);
	return true;
}

void GameObject::UpdateBB()
{

}

sf::Vector2f GameObject::GetPos()
{
	return m_xPos;
}

kartong::Sprite* GameObject::GetSprite()
{
	return m_xSprite;
}

GameObject::ObjectType GameObject::GetType() {
	return m_type;
}

kartong::BoundingSphere* GameObject::GetCollider()
{
	return m_xCollider;
}
