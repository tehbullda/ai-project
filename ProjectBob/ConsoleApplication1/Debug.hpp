#pragma once

#include "stdafx.h"

namespace kartong
{
	enum EDebugLevel{
		NONE,
		INFO,
		WARNING,
		ERROR,
	};
	class Debug
	{
		static EDebugLevel ms_level;
	public:
		Debug();
		~Debug();

		static void set_debug_level(EDebugLevel p_Level);
		static void write(const EDebugLevel p_Level, const std::string& p_Text);
		static void write(const EDebugLevel p_Level, const std::wstring& p_Text);
		static std::string timestamp();

	private:
		

	};
}