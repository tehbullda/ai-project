#include "stdafx.h"
#include "AuraManager.hpp"
//#include "Aura.hpp"
#include <fstream>
#include <sstream>
#include "ServiceLocator.hpp"
#include "BlackBoard.hpp"

#define maxnumicons sf::Vector2f(10,3)
#define iconsize sf::Vector2f(12,12)
#define padding 4

namespace AIGame {
	AuraManager::AuraManager() {
		if (LoadAuras()) {
			kartong::Debug::write(kartong::EDebugLevel::INFO, "Successfully loaded premade auras.");
		}
		else {
			kartong::Debug::write(kartong::EDebugLevel::WARNING, "Failed to load premade auras.");
		}
	}

	AuraManager::AuraManager(std::vector<Agent*> agents) {
		SetAgentVector(agents);
		if (LoadAuras()) {
			kartong::Debug::write(kartong::EDebugLevel::INFO, "Successfully loaded premade auras.");
		}
		else {
			kartong::Debug::write(kartong::EDebugLevel::WARNING, "Failed to load premade auras.");
		}
	}


	AuraManager::~AuraManager() {
		for (unsigned i = 0; i < m_auras.size(); i++) {
			if (m_auras[i] != nullptr) {
				delete m_auras[i];
				m_auras[i] = nullptr;
			}
		}
		m_auras.clear();
		for (unsigned i = 0; i < m_agentauras.size(); i++) {
			for (unsigned j = 0; j < m_agentauras[i].m_activeauras.size(); j++) {
				if (m_agentauras[i].m_activeauras[j] != nullptr) {
					delete m_agentauras[i].m_activeauras[j];
					m_agentauras[i].m_activeauras[j] = nullptr;
				}
				m_agentauras[i].m_activeauras.erase(m_agentauras[i].m_activeauras.begin() + j);
			}
			m_agentauras[i].m_activeauras.clear();
		}
	}

	void AuraManager::UpdateAuras() {
		for (unsigned i = 0; i < m_agentauras.size(); i++) {
			for (unsigned j = 0; j < m_agentauras[i].m_activeauras.size(); j++) {
				if (m_agentauras[i].m_activeauras[j]->Update()) {
					//RemoveAuraFromAgent(m_agentauras[i].m_agent, m_agentauras[i].m_activeauras[j]);
					RemoveAuraFromAgentByIndex(i, j);
				}
				else if (m_agentauras[i].m_activeauras[j]->Ticked()) {
					HandleTick(m_agentauras[i].m_agent, m_agentauras[i].m_activeauras[j]);
				}
			}
		}
	}

	void AuraManager::HandleTick(Agent *target, Aura *aura) {
		switch (aura->GetEffect().m_type) {
		case Aura::EffectType::DamageOverTime:
		case Aura::EffectType::LinearDamageOverTime:
		case Aura::EffectType::ExponentialDamageOverTime:
			target->TakeDamage(aura->GetPower());
			break;
		case Aura::EffectType::HealingOverTime:
			target->TakeDamage(-(int)aura->GetPower());
			break;
		case Aura::EffectType::DamageMultiplier:
			break;
		case Aura::EffectType::DamageIncrease:
			//target->SetDamage(target->GetDamage() + aura->GetPower());
			break;
		case Aura::EffectType::DamageDecrease:
			//target->SetDamage(target->GetDamage() - aura->GetPower());
			break;
		case Aura::EffectType::DamageBlock:
			break;
		case Aura::EffectType::HPIncrease:
			break;
		case Aura::EffectType::HPDecrease:
			break;
		case Aura::EffectType::Stun:
			target->SetStun(true);
			break;
		case Aura::EffectType::Slow:
			//target->SetMovespeed(1.0f - (float)aura->GetPower() * 0.01f);
			break;
		case Aura::EffectType::Count:
			break;
		}
	}

	void AuraManager::HandleApplication(Agent *target, Aura *aura) {
		switch (aura->GetEffect().m_type) {
		case Aura::EffectType::DamageOverTime:
		case Aura::EffectType::LinearDamageOverTime:
		case Aura::EffectType::ExponentialDamageOverTime:
		case Aura::EffectType::HealingOverTime:
			target->NotifyDebuff(aura);
			break;
		case Aura::EffectType::DamageMultiplier:
			target->SetDamage(target->GetDamage() * aura->GetPower());
			target->NotifyDebuff(aura);
			break;
		case Aura::EffectType::DamageIncrease:
			target->SetDamage(target->GetDamage() + aura->GetPower());
			target->NotifyDebuff(aura);
			break;
		case Aura::EffectType::DamageDecrease:
			target->SetDamage(target->GetDamage() - aura->GetPower());
			target->NotifyDebuff(aura);
			break;
		case Aura::EffectType::DamageBlock:
			//Add shield
			target->NotifyDebuff(aura);
			break;
		case Aura::EffectType::HPIncrease:
			target->SetHP(target->GetHP() + aura->GetPower());
			target->NotifyDebuff(aura);
			break;
		case Aura::EffectType::HPDecrease:
			target->SetHP(target->GetHP() - aura->GetPower());
			target->NotifyDebuff(aura);
			break;
		case Aura::EffectType::Stun:
			target->SetStun(true);
			target->NotifyDebuff(aura);
			break;
		case Aura::EffectType::Slow:
			target->SetMovespeed(1.0f - (float)aura->GetPower() * 0.01f);
			target->NotifyDebuff(aura);
			break;
		case Aura::EffectType::Count:
			break;
		}
	}

	void AuraManager::HandleRemoval(Agent *target, Aura *aura) {
		switch (aura->GetEffect().m_type) {
		case Aura::EffectType::DamageOverTime:
		case Aura::EffectType::LinearDamageOverTime:
		case Aura::EffectType::ExponentialDamageOverTime:
		case Aura::EffectType::HealingOverTime:
		case Aura::EffectType::DamageMultiplier:
			target->NotifyDebuff(aura, true);
			break;
		case Aura::EffectType::DamageIncrease:
		case Aura::EffectType::DamageDecrease:
			//Set damage to standard
			target->NotifyDebuff(aura, true);
			break;
		case Aura::EffectType::DamageBlock:
			//Remove the shield
			target->NotifyDebuff(aura, true);
			break;
		case Aura::EffectType::HPIncrease:
		case Aura::EffectType::HPDecrease:
			//Set HP to standard
			target->NotifyDebuff(aura, true);
			break;
		case Aura::EffectType::Stun:
			target->SetStun(false);
			target->NotifyDebuff(aura, true);
			break;
		case Aura::EffectType::Slow:
			target->SetMovespeed(target->GetMovespeed() + ((float)aura->GetPower() * 0.01f));
			target->NotifyDebuff(aura, true);
			break;
		case Aura::EffectType::Count:
			break;
		}
	}

	void AuraManager::SetAgentVector(std::vector<Agent*> agents) {
		for (unsigned i = 0; i < m_agentauras.size(); i++) {
			for (unsigned j = 0; j < m_agentauras[i].m_activeauras.size(); j++) {
				RemoveAuraFromAgentByIndex(i, j);
			}
		}
		m_agentauras.clear();
		for (unsigned i = 0; i < agents.size(); i++) {
			m_agentauras.push_back(AgentAura(agents[i]));
		}
	}

	void AuraManager::AgentAdded(Agent *agent) {
		m_agentauras.push_back(AgentAura(agent));
	}

	void AuraManager::AgentRemoved(Agent *agent) {
		for (unsigned i = 0; i < m_agentauras.size(); i++) {
			if (m_agentauras[i].m_agent == agent) {
				m_agentauras[i].m_activeauras.clear();
				m_agentauras[i].m_agent = nullptr;
				m_agentauras.erase(m_agentauras.begin() + i);
			}
		}
	}

	void AuraManager::AddAura(Aura *aura) {
		m_auras.push_back(aura);
	}

	void AuraManager::AddAuraToAgent(Agent *target, Aura *aura) {
		for (auto it = m_agentauras.begin(); it != m_agentauras.end(); it++) {
			if (it->m_agent == target) {
				it->m_activeauras.push_back(aura);
				HandleApplication(target, aura);
				sf::Vector2f curr((float)(it->m_activeauras.size() % (int)maxnumicons.x), (float)(it->m_activeauras.size() / (int)maxnumicons.y));
				it->m_activeauras[it->m_activeauras.size() - 1]->SetIconPos(
					sf::Vector2f(curr.x * iconsize.x + padding * (curr.x - 1), curr.y * iconsize.y + padding * (curr.y - 1)));
			}
		}
	}

	void AuraManager::AddAuraToAgent(Agent *target, const std::string &effecttype, const std::string &auratype, unsigned int power) {
		bool found = false;
		Aura *aura;
		for (unsigned i = 0; i < m_auras.size(); i++) {
			if (m_auras[i]->GetEffectTypeAsString() == effecttype) {
				found = true;
				if (m_auras[i]->GetPower() != power) {
					m_auras[i]->SetPower(power);
				}
				aura = m_auras[i];
			}
		}
		if (!found) {
			std::string name = "RuntimeAddedAura" + m_auras.size();
			const uint32_t pow = power;
			m_auras.push_back(new Aura(name, StringToAuraType(auratype), StringToEffectType(effecttype), effecttype, pow, false));
			aura = m_auras[m_auras.size() - 1];
		}
		for (auto it = m_agentauras.begin(); it != m_agentauras.end(); it++) {
			if (it->m_agent == target) {
				it->m_activeauras.push_back(aura);
				HandleApplication(it->m_agent, aura);
			}
		}
	}

	void AuraManager::AddAuraToAllAgents(const std::string &effecttype, const std::string &auratype, unsigned int power) {
		Aura *aura;
		bool found = false;
		for (unsigned i = 0; i < m_auras.size(); i++) {
			if (m_auras[i]->GetEffectTypeAsString() == effecttype) {
				found = true;
				if (m_auras[i]->GetPower() != power) {
					m_auras[i]->SetPower(power);
				}
				aura = m_auras[i];
			}
		}
		if (!found) {
			std::string name = "RuntimeAddedAura" + m_auras.size();
			m_auras.push_back(new Aura(name, StringToAuraType(auratype), StringToEffectType(effecttype), effecttype, power, false));
			aura = m_auras[m_auras.size() - 1];
		}
		for (auto it = m_agentauras.begin(); it != m_agentauras.end(); it++) {
			it->m_activeauras.push_back(aura);
			HandleApplication(it->m_agent, aura);
		}
	}

	void AuraManager::AddAuraToAllAgents(Aura *aura) {
		for (auto it = m_agentauras.begin(); it != m_agentauras.end(); it++) {
			it->m_activeauras.push_back(aura);
			HandleApplication(it->m_agent, aura);
			for (unsigned i = 0; i < m_auras.size(); i++) {
				if (m_auras[i] == aura) {
					return; // The aura exists in the vector
				}
			}
			// The aura does not exist in the vector
			m_auras.push_back(aura);
		}
	}

	void AuraManager::RemoveAuraFromAgent(Agent *target, Aura *aura) {
		for (unsigned i = 0; i < m_agentauras.size(); i++) {
			if (m_agentauras[i].m_agent == target) {
				for (unsigned j = 0; j < m_agentauras[i].m_activeauras.size(); j++) {
					if (m_agentauras[i].m_activeauras[j] == aura) {
						HandleRemoval(target, aura);
						m_agentauras[i].m_activeauras.erase(m_agentauras[i].m_activeauras.begin() + j);
					}
				}
			}
		}
	}
	bool AuraManager::DispelTarget(Agent *target) {
		bool ret = false;
		for (unsigned i = 0; i < m_agentauras.size(); i++) {
			if (m_agentauras[i].m_agent == target) {
				for (unsigned j = 0; j < m_agentauras[i].m_activeauras.size(); j++) {
					if (m_agentauras[i].m_activeauras[j]->GetDispellable()) {
						if (m_agentauras[i].m_activeauras[j]->GetAuraType() == Aura::AuraType::Debuff) {
							RemoveAuraFromAgentByIndex(i, j);
							ret = true;
						}
					}
				}
			}
		}
		return ret;
	}

	std::vector<Aura*> AuraManager::GetExistingAuras() {
		return m_auras;
	}

	void AuraManager::DrawIcons() {
		for (unsigned i = 0; i < m_agentauras.size(); i++) {
			for (unsigned j = 0; j < m_agentauras[i].m_activeauras.size(); i++) {
				m_agentauras[i].m_activeauras[j]->DrawIcon();
			}
		}
	}

	bool AuraManager::LoadAuras() {
		std::ifstream stream;
		std::stringstream ss;
		std::string line;
		std::string identifier, auratype, effecttype, desc, tmp;
		unsigned int power;
		bool dispellable;
		float lifetime, radius;
		stream.open("../Data/Config/Auras/auras.txt");
		if (!stream.is_open()) {
			return false;
		}
		while (!stream.eof()) {
			std::getline(stream, line);
			if (line[0] == '/' && line[1] == '/') {
				continue;
			}
			ss << line;
			ss >> identifier >> auratype >> effecttype;
			ss >> power >> dispellable >> lifetime >> radius;
			desc = line.substr(line.find_first_of('\"'), line.find_first_of('\"') - line.find_last_of('\"'));
			m_auras.push_back(new Aura(identifier, StringToAuraType(auratype), StringToEffectType(effecttype), desc, power, dispellable, lifetime, radius));
			m_auras[m_auras.size() - 1]->SetIcon("GUIHUD/PH_icon.png");
			ss.str("");
		}
		stream.close();
		return true;
	}

	Aura::AuraType AuraManager::StringToAuraType(const std::string &type) {
		if (type == "Buff") {
			return Aura::AuraType::Buff;
		}
		else if (type == "Debuff") {
			return Aura::AuraType::Debuff;
		}
		else {
			return Aura::AuraType::Global;
		}
	}
	Aura::EffectType AuraManager::StringToEffectType(const std::string &type) {
		if (type == "DamageOverTime") {
			return Aura::EffectType::DamageOverTime;
		}
		else if (type == "LinearDamageOverTime") {
			return Aura::EffectType::LinearDamageOverTime;
		}
		else if (type == "ExponentialDamageOverTime") {
			return Aura::EffectType::ExponentialDamageOverTime;
		}
		else if (type == "HealingOverTime") {
			return Aura::EffectType::HealingOverTime;
		}
		else if (type == "DamageMultiplier") {
			return Aura::EffectType::DamageMultiplier;
		}
		else if (type == "DamageIncrease") {
			return Aura::EffectType::DamageIncrease;
		}
		else if (type == "DamageDecrease") {
			return Aura::EffectType::DamageDecrease;
		}
		else if (type == "DamageBlock") {
			return Aura::EffectType::DamageBlock;
		}
		else if (type == "HPIncrease") {
			return Aura::EffectType::HPIncrease;
		}
		else if (type == "HPDecrease") {
			return Aura::EffectType::HPDecrease;
		}
		else if (type == "Stun") {
			return Aura::EffectType::Stun;
		}
		else {
			return Aura::EffectType::Slow;
		}
	}

	void AuraManager::RemoveAuraFromAgentByIndex(int agent, int aura) {
		if ((int)m_agentauras.size() <= agent || (int)m_agentauras[agent].m_activeauras.size() <= aura) {
			return;
		}
		HandleRemoval(m_agentauras[agent].m_agent, m_agentauras[agent].m_activeauras[aura]);
		m_agentauras[agent].m_activeauras.erase(m_agentauras[agent].m_activeauras.begin() + aura);
	}
}