#pragma once

#include "stdafx.h"
#include "Particle.hpp"
#include "ParticleEmitter.hpp"
namespace kartong
{


	class ParticleManager
	{
		friend class ParticleEmitter;
	private:
		ParticleManager();
	public:
		~ParticleManager();

		typedef std::unique_ptr<ParticleManager> Ptr;
		static Ptr Create();

		ParticleEmitter* createEmitter(std::string p_texname);

		void update();
		void attach(ParticleEmitter* p_Particle);

	private:
		std::vector<ParticleEmitter*> m_Emitters;

	};
}