//Decorator.cpp

#include "stdafx.h"
#include "Decorator.hpp"

Decorator::Decorator(AIGame::Agent* p_xOwner)
{
	m_xOwner = p_xOwner;
}

Decorator::~Decorator()
{
	BTNode::~BTNode();

	delete m_xChild;
	m_xChild = nullptr;
}

void Decorator::SetChild(BTNode* p_xNode)
{
	m_xChild = p_xNode;
}

BTNode::StateType Decorator::Update()
{
	return Success;
}

void Decorator::OnInitialize()
{
	
}

void Decorator::OnTerminate(StateType p_eState)
{
	m_eState = Invalid;
}