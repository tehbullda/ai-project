
#include "stdafx.h"
#include "UnitManager.hpp"
#include "Math.hpp"

using namespace kartong;

UnitManager::UnitManager()
{
	
};
UnitManager::~UnitManager()
{
	
};

void UnitManager::setUnitSize(const float& p_Pixels)
{
	PixelsPerUnit = p_Pixels;
};
void UnitManager::setUnitSize(const float& p_WindowSize, const float& p_UnitsPerScreen)
{
	PixelsPerUnit = p_WindowSize / p_UnitsPerScreen;
};

Vector2 UnitManager::getPixels(Vector2 p_Units)
{
	return p_Units / PixelsPerUnit;
};
float UnitManager::getPixels(const float& p_Units)
{
	return p_Units / PixelsPerUnit;
};