#include "stdafx.h"
#include "PremadeTickbox.hpp"
#include "ServiceLocator.hpp"

using namespace kartong::Premade;

PremadeTickbox::PremadeTickbox(const sf::String& p_Text)
{
	m_Window = ServiceLocator<Window>::get_service();
	m_InvisButton = new PremadeButton(ServiceLocator<TextureManager>::get_service()->loadTexture("engine/slider.png"));
	m_Back.setSize(sf::Vector2f(32, 32));
	m_Box.setSize(sf::Vector2f(24, 24));
	m_Box.setOutlineThickness(1);
	m_Box.setOutlineColor(sf::Color::Black);

	m_Text = new sf::Text(p_Text, *ServiceLocator<FontManager>::get_service()->getDefaultFont(), 20);
	

};
PremadeTickbox::~PremadeTickbox()
{
	delete m_Text;
	m_Text = nullptr;
	delete m_InvisButton;
	m_InvisButton = nullptr;
};
void PremadeTickbox::setPosition(const sf::Vector2f& p_Pos)
{
	m_Back.setPosition(p_Pos);
	m_InvisButton->setPosition(p_Pos);

};

void PremadeTickbox::update()
{
	m_Box.setPosition(sf::Vector2f(m_Back.getPosition().x + 4, m_Back.getPosition().y + 4));
	m_Text->setPosition(sf::Vector2f(m_Back.getPosition().x + 40, m_Back.getPosition().y));
	
	m_InvisButton->update();
	if (m_InvisButton->isClicked())
		m_State = !m_State;
	if (m_State)
		m_Box.setFillColor(sf::Color::Green);
	else
		m_Box.setFillColor(sf::Color::White);
};
void PremadeTickbox::draw()
{
	m_Window->getWindow()->draw(m_Back);
	m_Window->getWindow()->draw(m_Box);
	m_Window->getWindow()->draw(*m_Text);
};

bool PremadeTickbox::getState()
{
	return m_State;
};