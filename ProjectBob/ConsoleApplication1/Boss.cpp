//Boss.cpp

#include "stdafx.h"
#include "Boss.hpp"
#include "ServiceLocator.hpp"
#include "SelectionSettings.hpp"
#include "Window.hpp"
#include "StateTransition.hpp"
#include "PostGameStats.hpp"

#define TEXTUREPATH "aiassignment/placeholderboss.png"

using namespace AIGame;

Boss::Boss(sf::Vector2f p_xPos) : GameObject(p_xPos, GameObject::Type_Boss)
{
	m_Grid = kartong::ServiceLocator<Grid>::get_service();
	SetSprite(TEXTUREPATH);
	m_abilitymanager = new BossAbilityManager(m_Grid);
	m_xCollider = new kartong::BoundingSphere(p_xPos, 64.0f, sf::Vector2f(64.0f, 64.0f));
	m_xNameTag = kartong::ServiceLocator<NameHandler>::get_service()->getBossNameTag();
}

Boss::~Boss()
{
	GameObject::~GameObject();
	
	m_xNameTag = nullptr;
	m_Grid = nullptr;
	delete m_abilitymanager;
}

bool Boss::Update()
{
	GameObject::Update(); //do first in update to get correct deltatime
	m_abilitymanager->Update();
	return true;
}

void Boss::UpdateBB()
{
	/*send all relevant information to the blackboard*/
	m_xBB->UpdateBossPos(m_xPos);
}
void Boss::Draw()
{
	GetSprite()->draw();
};

void Boss::TakeDamage(float p_fAmount)
{
	m_fHP -= p_fAmount;
	if (m_fHP < 0.0f)
		Die();
	m_xNameTag->updateHP(m_fMaxHP, m_fHP);
}

void Boss::Die()
{
	kartong::ServiceLocator<StateTransition>::get_service()->startTransition();
	kartong::ServiceLocator<AIGame::PostGameStats>::get_service()->setWon(true);
	kartong::ServiceLocator<kartong::AudioManager>::get_service()->stopAll();
	kartong::ServiceLocator<kartong::AudioManager>::get_service()->playClip("sword.wav");
		//std::cout << "Dead!!" << std::endl;
};

void Boss::SetSettings(float p_fHP)
{
	m_fMaxHP = m_fHP = p_fHP;
};