#pragma once
#include "stdafx.h"

namespace kartong
{
	class AudioListener
	{
	public:
		AudioListener();
	public:
		~AudioListener();

		typedef std::unique_ptr<AudioListener> Ptr;
		static Ptr Create();

		sf::Vector2f getPosition();
		void setPosition(const sf::Vector2f& p_Pos);
		float getRange();
		void setRange(const float& p_Range);



	private:
		sf::Vector2f m_Position;
		float m_Range;
	};


}