#include "stdafx.h"

#include "Math.hpp"

using namespace kartong;

const float Math::PI = 3.14159265359f;

float Math::sqrt(float value)
{
	return std::sqrtf(value);
}

float Math::sin(float radian)
{
	return std::sinf(radian);
}

float Math::cos(float radian)
{
	return std::cosf(radian);
}

float Math::tan(float radian)
{
	return std::tanf(radian);
}

float Math::to_rad(float degrees)
{
	return degrees * PI / 180.0f;
}

float Math::to_deg(float radians)
{
	return radians * 180.0f / PI;
}

template <>
bool Math::are_equal<float>(const float& a, const float& b)
{
	return std::fabs(a - b) < std::numeric_limits<float>::epsilon();
}

template <>
bool Math::are_equal<double>(const double& a, const double& b)
{
	return std::abs(a - b) < std::numeric_limits<double>::epsilon();
}

float Math::distance(sf::Vector2f p_A, sf::Vector2f p_B)
{
	//float dgd = Math::sqrt(((p_B.x - p_A.x) * (p_B.x - p_A.x)) + ((p_B.y - p_A.y) * (p_B.y - p_A.y)));
	return Math::sqrt(((p_B.x - p_A.x) * (p_B.x - p_A.x)) + ((p_B.y - p_A.y) * (p_B.y - p_A.y)));
};
float Math::dot(const sf::Vector2f& p_Vec1, const sf::Vector2f& p_Vec2)
{
	return p_Vec1.x * p_Vec2.x + p_Vec1.y * p_Vec2.y;
};
sf::Vector2f Math::normalize(const sf::Vector2f& p_Vec)
{
	float len = length(p_Vec);
	if (len > 0.0f)
	{
		float inv = 1.0f / len;
		sf::Vector2f ret;
		ret.x = p_Vec.x * inv;
		ret.y = p_Vec.y * inv;
		return ret;
	}
	return sf::Vector2f(0.0f, 0.0f);
};
sf::Vector2f Math::normalize(const sf::Vector2i& p_Vec)
{
	float len = length(p_Vec);
	if (len > 0.0f)
	{
		float inv = 1.0f / len;
		sf::Vector2f ret;
		ret.x = p_Vec.x * inv;
		ret.y = p_Vec.y * inv;
		return ret;
	}
	return sf::Vector2f(0.0f, 0.0f);
};
float Math::length(const sf::Vector2f& p_Vec)
{
	return Math::sqrt(p_Vec.x * p_Vec.x + p_Vec.y * p_Vec.y);
};
float Math::length(const sf::Vector2i& p_Vec)
{
	return Math::sqrt((float)p_Vec.x * (float)p_Vec.x + (float)p_Vec.y * (float)p_Vec.y);
};
sf::Vector2f Math::getDirection(const sf::Vector2f& p_Point1, const sf::Vector2f& p_Point2)
{
	sf::Vector2f vec;
	vec.x = p_Point2.x - p_Point1.x;
	vec.x = p_Point2.y - p_Point1.y;
	return vec;
};

/////////////////////////////////7
Vector2::Vector2()
	: m_x(0.0f)
	, m_y(0.0f)
{
};
Vector2::Vector2(float p_X, float p_Y)
	: m_x(p_X)
	, m_y(p_Y)
{
};
Vector2::Vector2(const Vector2& p_Vec)
	: m_x(p_Vec.m_x)
	, m_y(p_Vec.m_y)
{
};

Vector2& Vector2::operator=(const Vector2& p_Vec)
{
	m_x = p_Vec.m_x;
	m_y = p_Vec.m_y;
	return *this;
};
Vector2& Vector2::operator+=(const Vector2& p_Vec)
{
	m_x += p_Vec.m_x;
	m_y += p_Vec.m_y;
	return *this;
};
Vector2& Vector2::operator-=(const Vector2& p_Vec)
{
	m_x -= p_Vec.m_x;
	m_y -= p_Vec.m_y;
	return *this;
};
Vector2& Vector2::operator*=(const float p_Val)
{
	m_x *= p_Val;
	m_y *= p_Val;
	return *this;
};
Vector2& Vector2::operator/=(const float p_Val)
{
	m_x /= p_Val;
	m_y /= p_Val;
	return *this;
};
Vector2& Vector2::operator+(const Vector2& p_Vec)
{
	return Vector2(m_x + p_Vec.m_x, m_y + p_Vec.m_y);
};
Vector2& Vector2::operator-(const Vector2& p_Vec)
{
	return Vector2(m_x - p_Vec.m_x, m_y - p_Vec.m_y);
};
Vector2& Vector2::operator*(const float p_Val)
{
	return Vector2(m_x * p_Val, m_y * p_Val);
};
Vector2& Vector2::operator/(const float p_Val)
{
	return Vector2(m_x / p_Val, m_y / p_Val);
};

float Vector2::length() const
{
	return Math::sqrt(m_x * m_x + m_y * m_y);
};
float Vector2::length_squared() const
{
	return m_x * m_x + m_y * m_y;
};
float Vector2::dot(const Vector2& p_Vec) const
{
	return m_x * p_Vec.m_x + m_y * p_Vec.m_y;
};
void Vector2::normalize()
{
	float len = length();
	if (len > 0.0f)
	{
		float inv = 1.0f / len;
		m_x *= inv;
		m_y *= inv;
	}
};

sf::Vector2f Vector2::getSfVector2f() const
{
	return sf::Vector2f(m_x, m_y);
};
sf::Vector2i Vector2::getSfVector2i() const
{
	return sf::Vector2i((int)m_x, (int)m_y);
};
sf::Vector2u Vector2::getSfVector2u() const
{
	return sf::Vector2u((unsigned)m_x, (unsigned)m_y);
};