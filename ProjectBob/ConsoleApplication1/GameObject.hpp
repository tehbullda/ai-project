//GameObject.hpp

#pragma once
#include "stdafx.h"
#include "Sprite.hpp"
#include "BlackBoard.hpp"
#include "BoundingUnits.hpp"

namespace AIGame
{
	class GameObject {
	public:
		enum ObjectType {
			Type_Projectile,
			Type_Agent,
			Type_Boss,
			Type_Object,
		};
	public:
		GameObject(sf::Vector2f p_xPos, ObjectType type = Type_Object);
		~GameObject();

		void SetPos(sf::Vector2f p_xNewPos);
		void SetPos(const float& p_NewX, const float& p_NewY);
		void ChangePos(sf::Vector2f p_xPosToAdd);
		void ChangePos(const float& p_AddX, const float& p_AddY);
		void SetSprite(const std::string& p_File);

		virtual bool Update();
		virtual void UpdateBB();
		virtual void DrawParticles(){ kartong::Debug::write(kartong::EDebugLevel::INFO, "Bajs"); };

		kartong::BoundingSphere* GetCollider();
		sf::Vector2f GetPos();
		kartong::Sprite* GetSprite();
		ObjectType GetType();

		BlackBoard* m_xBB;

	protected:
		sf::Vector2f m_xPos;
		kartong::Sprite* m_xSprite;
		ObjectType m_type;

		float m_fDeltaTime;

		kartong::BoundingSphere* m_xCollider;
	};
}