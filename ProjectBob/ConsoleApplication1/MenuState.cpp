#include "stdafx.h"

#include "MenuState.hpp"
#include "ServiceLocator.hpp"
#include "Randomizer.hpp"
#include "PremadeButton.hpp"
//#include "Tooltip.hpp"
//#include "Text.hpp"

using namespace kartong;
using namespace AIGame;
MenuState::MenuState(const std::string& p_Name)
{
	m_StateManager = ServiceLocator<StateManager>::get_service();
	m_TextureManager = ServiceLocator<TextureManager>::get_service();
	m_FontManager = ServiceLocator<FontManager>::get_service();

	m_Name = p_Name;
	m_UpdateRequitement = StateRequirement::ALWAYS;
	m_DrawRequitement = StateRequirement::ALWAYS;
};
MenuState::~MenuState()
{
	for (unsigned i = 0; i < m_buttons.size(); i++) {
		delete m_buttons[i];
		m_buttons[i] = nullptr;
	}
	delete m_tooltip;
	m_tooltip = nullptr;
	m_TextureManager = nullptr;
	m_FontManager = nullptr;
	m_StateManager = nullptr;

};

bool MenuState::Initialize()
{
	m_StateManager = ServiceLocator<StateManager>::get_service();
	m_TextureManager = ServiceLocator<TextureManager>::get_service();
	m_FontManager = ServiceLocator<FontManager>::get_service();

	m_buttons.push_back(new kartong::Premade::PremadeButton(m_TextureManager->loadTexture("menu/start_game.png"), "StartGame"));
	m_buttons[0]->setPosition(sf::Vector2f(50, 300));
	m_buttons.push_back(new kartong::Premade::PremadeButton(m_TextureManager->loadTexture("menu/options.png"), "Options"));
	m_buttons[1]->setPosition(sf::Vector2f(50, 400));
	m_buttons.push_back(new kartong::Premade::PremadeButton(m_TextureManager->loadTexture("menu/quit_game.png"), "Quit"));
	m_buttons[2]->setPosition(sf::Vector2f(50, 500));

	m_tooltip = new Tooltip("Testing", sf::Vector2f(0, 0), 12, sf::Color::Black, true);
	m_tooltip->SetVisibility(false);
	m_tooltip->SetTriggerArea(sf::IntRect(0, 0, 400, 400));
	return true;
};
bool MenuState::Update()
{
	bool testing = false;
	for (unsigned i = 0; i < m_buttons.size(); i++) {
		m_buttons[i]->update();
		if (m_buttons[i]->isClicked()) {
			if (m_buttons[i]->getIdentifer() == "StartGame") {
				m_Next = "SelectionState";
				handleStates();
				break;
			}
			else if (m_buttons[i]->getIdentifer() == "Options") {
				m_Next = "OptionsState";
				handleStates();
			}
			else if (m_buttons[i]->getIdentifer() == "Quit") {
				kartong::Engine::engine_shut();
			}
		}
		else if (m_buttons[i]->isHovering()) {
			testing = true;
		}
		else {
			//m_tooltip->SetVisibility(false);
		}
	}
	if (m_tooltip) {
		//m_tooltip->SetVisibility(testing);
		m_tooltip->Update();
	}
	/*m_TextureManager->preloadTexture("GUIHUD/Resume.png");
	m_TextureManager->preloadTexture("GUIHUD/Options.png");
	m_TextureManager->preloadTexture("GUIHUD/Menu.png");
	m_TextureManager->preloadTexture("GUIHUD/TextChat_Button.png");
	m_FontManager->preLoadFont("AdobeGothicStd-Bold.otf");*/

	//handleStates();
	return true;
};
void MenuState::Draw()
{
	for (unsigned i = 0; i < m_buttons.size(); i++) {
		m_buttons[i]->draw();
	}
	m_tooltip->Draw();
	
};

void MenuState::Exit()
{
	for (unsigned i = 0; i < m_buttons.size(); i++) {
		delete m_buttons[i];
		m_buttons[i] = nullptr;
	}
	m_buttons.clear();
	delete m_tooltip;
	m_tooltip = nullptr;
	m_FontManager = nullptr;
	m_TextureManager = nullptr;
};

std::string MenuState::getName()
{
	return m_Name;
};
int MenuState::getUpdateRequirement()
{
	return m_UpdateRequitement;
};
int MenuState::getDrawRequirement()
{
	return m_DrawRequitement;
};

void MenuState::handleStates()
{
	m_StateManager->activate(m_Next);
	m_StateManager->deactivate(m_Name);
};
