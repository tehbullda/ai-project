#include "stdafx.h"

#include <algorithm>
#include <random>
#include <cctype>
#include <functional>

using namespace kartong;

void Utils::cutAtPoint(std::string p_String, const char& p_Point, std::string* p_First, std::string* p_Second)
{
	bool charfound = false;
	int counter = 0;
	for (unsigned int i = 0; i < p_String.size(); i++)
	{
		if (p_String[i] == p_Point) {
			charfound = true;
		}
		if (!charfound){
			p_First[i] = p_String[i];
			counter++;
		}
		else
			p_Second[i - counter] = p_String[i];
	}
	*p_First = rtrim(*p_First);
	*p_Second = ltrim(*p_First);
};
std::string Utils::ltrim(std::string p_String)
{
	p_String.erase(p_String.begin(), std::find_if(p_String.begin(), p_String.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
	return p_String;
};
std::string Utils::trim(std::string p_String)
{
	return ltrim(rtrim(p_String));
};
std::string Utils::rtrim(std::string p_String)
{
	p_String.erase(std::find_if(p_String.rbegin(), p_String.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), p_String.end());
	return p_String;
};

float Utils::string_to_float(std::string s) {
	return std::stof(s);
}