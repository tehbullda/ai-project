#pragma once

#include "stdafx.h"
#include "TextureManager.hpp"
#include "Sprite.hpp"
#include "ParticleEmitter.hpp"
#include "Input.hpp"
#include "TextChat.hpp"
#include "PremadeButton.hpp"

//
#include "GraphWindow.hpp"
namespace AIGame
{
	class PauseStateButtons
	{
	public:
		PauseStateButtons();

		~PauseStateButtons();

		void update();
		void draw();


	
		
	private:
		kartong::StateManager* m_StateManager;
		kartong::TextureManager* m_TextureManager;
	private:
		sf::Vector2f m_Pos;
		kartong::Premade::PremadeButton* m_Resume;
		kartong::Premade::PremadeButton* m_Options;
		kartong::Premade::PremadeButton* m_Menu;

		
		//Premade::PremadeButton* m_Quit;
	};

	class PauseState : public kartong::State
	{
	public:
		PauseState(const std::string& p_Name);
		~PauseState();


		bool Initialize();
		bool Update();
		void Draw();

		void Exit();

		std::string getName();
		int getUpdateRequirement();
		int getDrawRequirement();

	private:
		void handleStates();
	private:
		std::string m_Name;
		int m_DrawRequitement;
		int m_UpdateRequitement;
	private:
		kartong::StateManager* m_StateManager;
		kartong::TextureManager* m_TextureManager;
		kartong::FontManager* m_FontManager;
		kartong::system::input::Input* m_Input;
		kartong::Window* m_Window;

	private:
		sf::RectangleShape* m_Background;
		PauseStateButtons* m_Buttons;
	};


}
