#include "stdafx.h"
#include "Text.hpp"
#include "ServiceLocator.hpp"

namespace AIGame {
	Text::Text() {
		m_text.setFont(*kartong::ServiceLocator<kartong::FontManager>::get_service()->getDefaultFont());
	}

	Text::Text(const std::string &text, const sf::Vector2f &pos, const uint32_t &charactersize) { 
		m_text.setFont(*kartong::ServiceLocator<kartong::FontManager>::get_service()->getDefaultFont());
		m_text.setString(text);
		m_text.setPosition(pos);
		if (m_text.getCharacterSize() != charactersize) {
			std::cout << "Setting new charactersize, old: " << m_text.getCharacterSize() << " new: " << charactersize << std::endl;
			m_text.setCharacterSize(charactersize);
		}
	}

	Text::~Text() {

	}

	void Text::SetString(const std::string &text) {
		m_text.setString(text);
	}

	void Text::SetPosition(const sf::Vector2f &pos) {
		m_text.setPosition(pos);
	}

	void Text::SetCharacterSize(const uint32_t &size) {
		m_text.setCharacterSize(size);
	}

	void Text::SetTextColor(const sf::Color &color) {
		m_text.setColor(color);
	}

	void Text::AdjustTextsize(const int &adjustment) {
		m_text.setCharacterSize(m_text.getCharacterSize() + adjustment);
	}

	sf::Vector2f Text::GetPosition() {
		return m_text.getPosition();
	}

	sf::FloatRect Text::GetRect() {
		return m_text.getLocalBounds();
	}

	void Text::Draw() {
		kartong::ServiceLocator<kartong::Window>::get_service()->getWindow()->draw(m_text);
	}
}