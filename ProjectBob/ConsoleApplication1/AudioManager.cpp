#include "stdafx.h"
#include "AudioManager.hpp"
#include <sstream>
#include <fstream>
#include <ostream>
#include <iostream>


using namespace kartong;



AudioManager::AudioManager(const std::string& p_Directory)
{

	if (!loadVolume("../Data/Config/Audio/volume.txt"))
		m_Volume = { 0 };
	m_Directory = p_Directory;


};
AudioManager::~AudioManager()
{
	{
		auto it = m_Sounds.begin();
		while (it != m_Sounds.end())
		{
			if (it->second){
				delete it->second;
				it->second = nullptr;
			}
			it++;
		}
		m_Sounds.clear();
	}
	{
		auto it = m_Buffers.begin();
		while (it != m_Buffers.end())
		{
			if (it->second){
				delete it->second;
				it->second = nullptr;
			}
			it++;
		}
		m_Buffers.clear();
	}

	m_Listener = nullptr;
};

AudioManager::Ptr AudioManager::Create(const std::string& p_Directory)
{
	return AudioManager::Ptr(new AudioManager(p_Directory));
};

void AudioManager::setAudioListener(AudioListener* p_Listener)
{
	m_Listener = p_Listener;
};

void AudioManager::preLoadSound(const std::string& p_File)
{
	sf::Sound* sound = new sf::Sound;
	sf::SoundBuffer* buffer = new sf::SoundBuffer;
	buffer->loadFromFile(m_Directory + p_File);
	m_Buffers.insert(std::pair<std::string, sf::SoundBuffer*>(p_File, buffer));
	sound->setBuffer(*m_Buffers.find(p_File)->second);
	m_Sounds.insert(std::pair<std::string, sf::Sound*>(p_File, sound));
	buffer = nullptr;
	sound = nullptr;
};
void AudioManager::playClip(const std::string& p_Clip)
{
	m_Sounds.find(p_Clip)->second->play();
};
void AudioManager::playClipAtPoint(const std::string& p_Clip, const sf::Vector2f& p_Pos)
{
	sf::Sound* sound;
	if (!m_Sounds.find(p_Clip)->second){
		Debug::write(EDebugLevel::INFO, "Sound: " + p_Clip + " not found");
		return;
	}
	sound = m_Sounds.find(p_Clip)->second;
	float rangevol;
	if (m_Listener){
		float distance = Math::sqrt(((m_Listener->getPosition().x - p_Pos.x) * (m_Listener->getPosition().x - p_Pos.x)) + ((m_Listener->getPosition().y - p_Pos.y) * (m_Listener->getPosition().y - p_Pos.y)));
		if (m_Listener->getRange() <= distance)
			rangevol = 0.0f;
		else
			rangevol = 1.0f - (distance / m_Listener->getRange());
		Debug::write(EDebugLevel::INFO, std::to_string(rangevol));
	}
	else{
		rangevol = 1.0f;
	}
	if (rangevol > 1.0f)
		rangevol = 1.0f;
	sound->setVolume(m_Volume.m_Master * rangevol * 100);
	sound->play();
	//sound->setVolume(100.0f);
	sound = nullptr;
};



bool AudioManager::loadVolume(const std::string& p_File)
{
	std::ifstream file(p_File);

	if (!file.is_open())
		return false;


	while (!file.eof())
	{
		file >> m_Volume.m_Master
			>> m_Volume.m_Effects
			>> m_Volume.m_Ambient
			>> m_Volume.m_Background
			>> m_Volume.m_Speech;
	}

	return true;
};;
void AudioManager::saveVolume(const std::string& p_File)
{
	std::filebuf buf;
	buf.open(p_File, std::ios::out);
	std::ostream os(&buf);
	os << m_Volume.m_Master << "\n";
	os << m_Volume.m_Effects << "\n";
	os << m_Volume.m_Ambient << "\n";
	os << m_Volume.m_Background << "\n";
	os << m_Volume.m_Speech << "\n";
	buf.close();

};;
void AudioManager::stopAll()
{
	auto it = m_Sounds.begin();
	while (it != m_Sounds.end())
	{
		it->second->stop();
		++it;
	}
};

////    ////////  //      //
//  //  //        ////  ////
////    //  ////  //  //  //
//  //  //    //  //      //
//////  ////////  //      //

void AudioManager::BGM_attach(const std::string& p_Path, const int& p_Identifier, const bool& p_Looping)
{

};
void AudioManager::BGM_play(int& p_Identifier)
{

};
void AudioManager::BGM_stop(int& p_Identifier)
{

};
void AudioManager::BGM_pause(int& p_Identifier)
{

};
void AudioManager::BGM_reset(int& p_Identifier)
{

};
bool AudioManager::BGM_isPlaying(int& p_Identifier)
{
	return false;
};