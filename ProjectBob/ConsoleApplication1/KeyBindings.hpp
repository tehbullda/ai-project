#pragma once


namespace kartong
{
	namespace system
	{
		namespace input
		{
			class KeyBindings
			{
			private:
				KeyBindings();
			public:
				~KeyBindings();

				typedef std::unique_ptr<KeyBindings> Ptr;
				static Ptr Create();

				bool getAction(const std::string& p_Action);
				bool getActionDown(const std::string& p_Action);
				bool getActionReleased(const std::string& p_Action);

				
				void bindKey(const std::string& p_Action, const int& p_Key);
				void unbindAction(const int& p_Action);

				void loadBinds(const std::string& p_File);
				void saveBinds(const std::string& p_File);
				void setDefaultBinds();

			private:
				std::map<std::string, int> m_Binds;

				Input* m_Input;
			};
		}
	}
}