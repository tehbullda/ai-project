#include "stdafx.h"
#include "NameHandler.hpp"
#include "Randomizer.hpp"
#include "ServiceLocator.hpp"
#include <locale>

#define PADDING 5

namespace AIGame
{

	NameTag::NameTag(const std::string& p_Name)
	{
		m_Window = kartong::ServiceLocator<kartong::Window>::get_service();


		m_Name = p_Name;
		m_Text = new sf::Text(sf::String(m_Name), *kartong::ServiceLocator<kartong::FontManager>::get_service()->loadFont("tahomabd.ttf"), 12);
		m_Text->setColor(sf::Color::Black);

		m_Back.setSize(sf::Vector2f(90, 16));
		m_Back.setFillColor(sf::Color(150, 150, 150, 200));
		m_Back.setOutlineColor(sf::Color::Black);
		m_Back.setOutlineThickness(1);

		m_Position.x = (float)kartong::Randomizer::GetRandomInt(0, 1280);
		m_Position.y = (float)kartong::Randomizer::GetRandomInt(0, 720);

		m_HPBack.setSize(sf::Vector2f(m_Back.getSize().x, 5));
		m_HPBack.setOutlineThickness(1);
		m_HPBack.setFillColor(sf::Color(100, 100, 100, 100));
		m_HPBack.setOutlineColor(sf::Color::Black);
		m_HPBar.setSize(sf::Vector2f(m_Back.getSize().x, 5));
		m_HPBar.setFillColor(sf::Color(255, 0, 0, 200));

		m_Dirty = true;

		test = 00;
	};
	NameTag::~NameTag()
	{
		delete m_Text;
		m_Text = nullptr;
	};

	void NameTag::setPosition(const sf::Vector2f& p_Pos)
	{
		m_Position = p_Pos;
		m_Dirty = true;
	};

	void NameTag::update()
	{
		if (m_Dirty){
			m_Back.setPosition(m_Position);
			m_Text->setPosition(sf::Vector2f(m_Position.x + 2, m_Position.y));
			m_HPBack.setPosition(sf::Vector2f(m_Position.x, m_Position.y + m_Back.getGlobalBounds().height));
			m_HPBar.setPosition(sf::Vector2f(m_Position.x, m_Position.y + m_Back.getGlobalBounds().height));
			m_Dirty = false;
		}
		test += 0.4f;
	};
	void NameTag::draw()
	{
		m_Window->getWindow()->draw(m_Back);
		m_Window->getWindow()->draw(*m_Text);
		m_Window->getWindow()->draw(m_HPBack);
		m_Window->getWindow()->draw(m_HPBar);
	};
	void NameTag::updateHP(const float& p_Max, const float& p_Curr)
	{

		float rel = p_Curr / p_Max;
		if (rel < 0)
			rel = 0;
		if (rel > 1)
			rel = 1;

		m_HPBar.setScale(rel, 1);
	}
	std::string NameTag::getName()
	{
		return m_Name;
	};

	////////
	BossNameTag::BossNameTag()
	{
		m_Window = kartong::ServiceLocator<kartong::Window>::get_service();


		m_Name = "BOSS";
		m_Text = new sf::Text(sf::String(m_Name), *kartong::ServiceLocator<kartong::FontManager>::get_service()->loadFont("tahomabd.ttf"), 15);
		m_Text->setColor(sf::Color::Black);

		m_Back.setSize(sf::Vector2f(m_Window->getSize().x, 20.0f));
		m_Back.setFillColor(sf::Color(150, 150, 150, 200));
		m_Back.setOutlineColor(sf::Color::Black);
		m_Back.setOutlineThickness(1);

		m_Position = sf::Vector2f(0.0f, m_Window->getSize().y - 40.0f);

		m_HPBack.setSize(sf::Vector2f(m_Back.getSize().x, 10));
		m_HPBack.setOutlineThickness(2);
		m_HPBack.setFillColor(sf::Color(100, 100, 100, 100));
		m_HPBack.setOutlineColor(sf::Color::Black);
		m_HPBar.setSize(sf::Vector2f(m_Back.getSize().x, 10));
		m_HPBar.setFillColor(sf::Color(255, 0, 0, 200));

		m_Back.setPosition(m_Position);
		m_Text->setPosition(sf::Vector2f(m_Position.x + 2, m_Position.y));
		m_HPBack.setPosition(sf::Vector2f(m_Position.x, m_Position.y + m_Back.getGlobalBounds().height));
		m_HPBar.setPosition(sf::Vector2f(m_Position.x, m_Position.y + m_Back.getGlobalBounds().height));

	}

	BossNameTag::~BossNameTag()
	{
		delete m_Text;
		m_Text = nullptr;
	}

	void BossNameTag::draw()
	{
		m_Window->getWindow()->draw(m_Back);
		m_Window->getWindow()->draw(*m_Text);
		m_Window->getWindow()->draw(m_HPBack);
		m_Window->getWindow()->draw(m_HPBar);
	};
	void BossNameTag::updateHP(const float& p_Max, const float& p_Curr)
	{
		float rel = p_Curr / p_Max;
		if (rel < 0)
			rel = 0;
		if (rel > 1)
			rel = 1;

		m_HPBar.setScale(rel, 1);
	}

	////////

	NameHandler::NameHandler()
	{
		std::ifstream file("../Data/Config/Names/Names.txt");
		if (file.is_open()){
			while (!file.eof()){
				std::string name;
				std::getline(file, name);
				m_Names.push_back(name);
			}
		}
	};
	NameHandler::~NameHandler()
	{
		for (unsigned i = 0; i < m_Tags.size(); i++)
		{
			delete m_Tags[i];
			m_Tags[i] = nullptr;
		}

		delete m_BossTag;
		m_BossTag = nullptr;
	};

	NameHandler::Ptr NameHandler::Create()
	{
		return NameHandler::Ptr(new NameHandler());
	};

	NameTag* NameHandler::getNameTag()
	{
		std::string name = "NoNamesLeft";
		if (m_Names.size() > 0){
			int rand = kartong::Randomizer::GetRandomInt(0, m_Names.size());
			if (rand != 0)
				rand--;
			name = m_Names[rand];
			m_Names.erase(m_Names.begin() + rand);
		}
		NameTag* tag = new NameTag(name);
		m_Tags.push_back(tag);
		return tag;
	};

	NameTag* NameHandler::getNameTag(const std::string& p_Name)
	{
		p_Name;
		std::string name = "NoNamesLeft";
		NameTag* tag = new NameTag(name);
		m_Tags.push_back(tag);
		return tag;
	};
	std::string NameHandler::requestName()
	{
		std::string name = "NoNamesLeft";
		if (m_Names.size() > 0){
			int rand = kartong::Randomizer::GetRandomInt(0, m_Names.size());
			if (rand != 0)
				rand--;
			name = m_Names[rand];
			m_Names.erase(m_Names.begin() + rand);
		}
		return name;
	};

	void NameHandler::updateTags()
	{
		for (unsigned i = 0; i < m_Tags.size(); i++){
			m_Tags[i]->update();
		}
	};
	void NameHandler::drawTags()
	{
		for (unsigned i = 0; i < m_Tags.size(); i++){
			m_Tags[i]->draw();
			if (m_Tags[i]->m_done){
				delete m_Tags[i];
				m_Tags.erase(m_Tags.begin() + i);
			}
		}
		m_BossTag->draw();
	};

	BossNameTag* NameHandler::getBossNameTag()
	{
		if (!m_BossTag)
			m_BossTag = new BossNameTag;
		
		return m_BossTag;
	}
}