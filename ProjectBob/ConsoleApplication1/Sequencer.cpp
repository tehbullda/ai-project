//Sequencer.cpp

#include "stdafx.h"
#include "Sequencer.hpp"

Sequencer::Sequencer(AIGame::Agent* p_xOwner)
{

}

Sequencer::~Sequencer()
{
	m_xCurrentChild = m_xChildren.begin();
	while (m_xCurrentChild != m_xChildren.end())
	{
		(*m_xCurrentChild) = nullptr;
		m_xCurrentChild = m_xChildren.erase(m_xCurrentChild);
	}
	m_xChildren.clear();

	BTNode::~BTNode();
}

void Sequencer::AddChild(BTNode* p_xNode)
{
	m_xChildren.push_back(p_xNode);
}

BTNode::StateType Sequencer::Update()
{
	while (true)
	{
		BTNode::StateType status = (*m_xCurrentChild)->Tick();
		if (status != Success)
		{
			return status;
		}

		if (++m_xCurrentChild == m_xChildren.end())
		{
			return Success;
		}
	}

	return Invalid;
}

void Sequencer::OnInitialize()
{
	m_xCurrentChild = m_xChildren.begin();
}

void Sequencer::OnTerminate(StateType p_eState)
{
	m_xCurrentChild = m_xChildren.begin();
	while (m_xCurrentChild != m_xChildren.end())
	{
		(*m_xCurrentChild)->SetState(BTNode::StateType::Invalid);
		++m_xCurrentChild;
	}
}