#pragma once
#include "Grid.hpp"
#include "Aura.hpp"
#include "Agent.hpp"
namespace AIGame {
	class BossAbility {
	public:
		BossAbility(Grid *grid, float cooldown, float casttime, int power, std::string name, bool appliesdebuff, Aura *aura);
		virtual ~BossAbility();
		virtual bool Update() = 0;
		virtual void StartCast() = 0;
		virtual void Cast() = 0;
		
		void UpdateAgentVector();
		void SetPower(int power) {
			m_power = power;
		}
		bool Casting() {
			return m_casting;
		}
		bool AppliesDebuff() {
			return m_appliesdebuff;
		}
		int GetPower() {
			return m_power;
		}
		std::string GetName() {
			return m_name;
		}
		Aura* GetAura() {
			return m_aura;
		}
	protected:
		Grid *m_grid;
		Aura *m_aura;
		std::vector<Agent*> m_agents;
		bool m_appliesdebuff;
		int m_power;
		float m_cooldown;
		float m_currenttime;
		float m_casttime;
		std::string m_name;
		bool m_casting;
	};

	class Smash : public BossAbility {
	public:
		Smash(Grid *grid, float cooldown, float casttime, int power = 50, std::string name = "Smash", bool appliesdebuff = false, Aura *aura = nullptr);
		~Smash();
		bool Update();
		void StartCast();
		void Cast();
	protected:
		sf::Vector2i m_target;
		
	};

	class Cleave : public BossAbility {
		enum EDirection {
			Up,
			Down,
			Left,
			Right,
			Count
		};
	public:
		Cleave(Grid *grid, float cooldown, float casttime, int power = 30, std::string name = "Cleave", bool appliesdebuff = false, Aura *aura = nullptr);
		~Cleave();
		bool Update();
		void StartCast();
		void Cast();
	protected:
		EDirection m_dir;
		std::vector<sf::Vector2i> m_affectedtiles;
	};
	class Plague : public BossAbility {
	public:
		Plague(Grid *grid, float cooldown, float casttime, int power = 10, std::string name = "Plague", bool appliesdebuff = false, Aura *aura = nullptr);
		~Plague();
		bool Update();
		void StartCast();
		void Cast();
		void SpreadPlague();

	};
	class Babaam : public BossAbility {
	public:
		Babaam(Grid *grid, float cooldown, float casttime, int power = 10, std::string name = "Babaam", bool appliesdebuff = false, Aura *aura = nullptr);
		~Babaam();
		bool Update();
		void StartCast();
		void Cast();
	private:
		std::vector<sf::Vector2f> m_slowtiles;
	};
	class SpreadLava : public BossAbility {
	public:
		SpreadLava(Grid *grid, float cooldown, float casttime, int power = 10, std::string name = "SpreadLava", bool appliesdebuff = false, Aura *aura = nullptr);
		~SpreadLava();
		bool Update();
		void StartCast();
		void Cast();
		void SpreadOnRandomTiles(double chance = 0.20);
		//Spreads in a square, radius 1 will affect a 3x3 area, 2 will affect a 5x5, 3 7x7 etc.
		void SpreadFromSetTile(Node *target, int radius);
	private:
		void ClearLava();
		void CheckAgents();

	private:
		std::vector<sf::Vector2i> m_lavatiles;
		float m_lifetime, m_ticktime;
	};
	class JumpAway : public BossAbility {
	public:
		JumpAway(Grid *grid, float cooldown, float casttime, int power = 5, std::string name = "JumpAway", bool appliesdebuff = false, Aura *aura = nullptr);
		~JumpAway();
		bool Update();
		void StartCast();
		void Cast();
	private:
		sf::Vector2i m_target;
	};
	class BossAbilityManager {
	public:
		BossAbilityManager(Grid *grid);
		~BossAbilityManager();

		bool Update();
		void Cast(const std::string &name = "Random");
		void AddAbility(BossAbility* ability, float damagemult = 1.0f);
	private:
		void AddDefaultAbilities();
		void SetDefaultAsActive();
		BossAbility* StringToAbility(const std::string &name);


	private:
		std::vector<BossAbility*> m_activeabilities;
		std::vector<BossAbility*> m_defaultabilities;
		Grid *m_grid;
		bool m_spelliscasting;
	};
}