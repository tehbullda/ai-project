#include "stdafx.h"

#include "SelectionState.hpp"
#include "ServiceLocator.hpp"
#include "Randomizer.hpp"

#define EDGEPADDING 30
#define TICKPADDING 50
#define SLIDERPADDINGX 320
#define SLIDERPADDINGY 65
#define DMGSLIDERPADDING 250

using namespace kartong;
using namespace AIGame;

SelectionState::SelectionState(const std::string& p_Name)
{
	m_StateManager = ServiceLocator<kartong::StateManager>::get_service();
	m_TextureManager = ServiceLocator<TextureManager>::get_service();
	m_FontManager = ServiceLocator<FontManager>::get_service();
	m_Input = ServiceLocator<system::input::Input>::get_service();
	m_Window = ServiceLocator<Window>::get_service();
	m_Name = p_Name;
	m_UpdateRequirement = StateRequirement::FIRST;
	m_DrawRequirement = StateRequirement::ALWAYS;

	m_Settings = SelectionSettings::Create();
	ServiceLocator<SelectionSettings>::set_service(m_Settings.get());
	//////////
};
SelectionState::~SelectionState()
{
	Exit();

};

bool SelectionState::Initialize()
{

	m_StateManager = ServiceLocator<kartong::StateManager>::get_service();
	m_TextureManager = ServiceLocator<TextureManager>::get_service();
	m_FontManager = ServiceLocator<FontManager>::get_service();
	m_Input = ServiceLocator<system::input::Input>::get_service();
	m_Window = ServiceLocator<Window>::get_service();

	m_Start = new Premade::PremadeButton(m_TextureManager->loadTexture("menu/start.png"));
	m_Start->setPosition(sf::Vector2f(m_Window->getWindow()->getSize().x - m_Start->getSize().x - EDGEPADDING, m_Window->getWindow()->getSize().y - m_Start->getSize().y - EDGEPADDING));
	m_StartDefaults = new Premade::PremadeButton(m_TextureManager->loadTexture("menu/default.png"));
	m_StartDefaults->setPosition(sf::Vector2f(m_Window->getWindow()->getSize().x - (m_StartDefaults->getSize().x + EDGEPADDING) * 2 + 20, m_Window->getWindow()->getSize().y - m_StartDefaults->getSize().y - EDGEPADDING));

	m_AutoSpawn = new Premade::PremadeTickbox("Auto spawn Agents");
	m_AutoSpawn->setPosition(sf::Vector2f(EDGEPADDING, m_Window->getSize().y - 200));


	m_Ability_1 = new Premade::PremadeTickbox("Ability: Smash");
	m_Ability_1->setPosition(sf::Vector2f(EDGEPADDING, EDGEPADDING));
	m_Ability_2 = new Premade::PremadeTickbox("Ability: Cleave");
	m_Ability_2->setPosition(sf::Vector2f(EDGEPADDING, EDGEPADDING + TICKPADDING));
	m_Ability_3 = new Premade::PremadeTickbox("Ability: Plague");
	m_Ability_3->setPosition(sf::Vector2f(EDGEPADDING, EDGEPADDING + TICKPADDING * 2));
	m_Ability_4 = new Premade::PremadeTickbox("Ability: Babaam");
	m_Ability_4->setPosition(sf::Vector2f(EDGEPADDING, EDGEPADDING + TICKPADDING * 3));
	m_Ability_5 = new Premade::PremadeTickbox("Ability: SpreadLava");
	m_Ability_5->setPosition(sf::Vector2f(EDGEPADDING, EDGEPADDING + TICKPADDING * 4));
	m_Ability_6 = new Premade::PremadeTickbox("Ability: JumpAway");
	m_Ability_6->setPosition(sf::Vector2f(EDGEPADDING, EDGEPADDING + TICKPADDING * 5));

	m_DMG_Ability_1 = new Premade::PremadeSlider("Damage %", 500);
	m_DMG_Ability_1->setPosition(sf::Vector2f(EDGEPADDING + DMGSLIDERPADDING, EDGEPADDING));
	m_DMG_Ability_2 = new Premade::PremadeSlider("Damage %", 500);
	m_DMG_Ability_2->setPosition(sf::Vector2f(EDGEPADDING + DMGSLIDERPADDING, EDGEPADDING + TICKPADDING));
	m_DMG_Ability_3 = new Premade::PremadeSlider("Damage %", 500);
	m_DMG_Ability_3->setPosition(sf::Vector2f(EDGEPADDING + DMGSLIDERPADDING, EDGEPADDING + TICKPADDING * 2));
	m_DMG_Ability_4 = new Premade::PremadeSlider("Damage %", 500);
	m_DMG_Ability_4->setPosition(sf::Vector2f(EDGEPADDING + DMGSLIDERPADDING, EDGEPADDING + TICKPADDING * 3));
	m_DMG_Ability_5 = new Premade::PremadeSlider("Damage %", 500);
	m_DMG_Ability_5->setPosition(sf::Vector2f(EDGEPADDING + DMGSLIDERPADDING, EDGEPADDING + TICKPADDING * 4));
	m_DMG_Ability_6 = new Premade::PremadeSlider("Damage %", 500);
	m_DMG_Ability_6->setPosition(sf::Vector2f(EDGEPADDING + DMGSLIDERPADDING, EDGEPADDING + TICKPADDING * 5));

	m_Slider_HP_Agent = new Premade::PremadeSlider("Agent HP", 2000);
	m_Slider_HP_Agent->setPosition(sf::Vector2f(m_Window->getSize().x - EDGEPADDING - SLIDERPADDINGX, EDGEPADDING));
	m_Slider_HP_Boss = new Premade::PremadeSlider("Boss HP", 100000);
	m_Slider_HP_Boss->setPosition(sf::Vector2f(m_Window->getSize().x - EDGEPADDING - SLIDERPADDINGX, EDGEPADDING + SLIDERPADDINGY));
	/*m_Slider_AI_Tick = new Premade::PremadeSlider("AI-Ticks per sec", 60);
	m_Slider_AI_Tick->setPosition(sf::Vector2f(m_Window->getSize().x - EDGEPADDING - SLIDERPADDINGX, EDGEPADDING + SLIDERPADDINGY * 2));*/
	m_Slider_Agents_Num = new Premade::PremadeSlider("Agents", 100);
	m_Slider_Agents_Num->setPosition(sf::Vector2f(m_Window->getSize().x - EDGEPADDING - SLIDERPADDINGX, EDGEPADDING + SLIDERPADDINGY * 2));
	m_Slider_BossDMG = new Premade::PremadeSlider("Boss Damage Percent", 500);
	m_Slider_BossDMG->setPosition(sf::Vector2f(m_Window->getSize().x - EDGEPADDING - SLIDERPADDINGX, EDGEPADDING + SLIDERPADDINGY * 3));
	m_Slider_AgentDMG = new Premade::PremadeSlider("Agent Damage Percent", 500);
	m_Slider_AgentDMG->setPosition(sf::Vector2f(m_Window->getSize().x - EDGEPADDING - SLIDERPADDINGX, EDGEPADDING + SLIDERPADDINGY * 4));
	//m_Slider_BossSpeed = new Premade::PremadeSlider("Boss Speed Percent", 500);
	//m_Slider_BossSpeed->setPosition(sf::Vector2f(m_Window->getSize().x - EDGEPADDING - SLIDERPADDINGX, EDGEPADDING + SLIDERPADDINGY * 6));
	//m_Slider_AgentSpeed = new Premade::PremadeSlider("Agent Speed Percent", 500);
	//m_Slider_AgentSpeed->setPosition(sf::Vector2f(m_Window->getSize().x - EDGEPADDING - SLIDERPADDINGX, EDGEPADDING + SLIDERPADDINGY * 7));

	return true;
};
bool SelectionState::Update()
{
	updateAbilities();
	updateSliders();

	m_AutoSpawn->update();


	handleStates();
	return true;
};
void SelectionState::Draw()
{
	m_Start->draw();
	m_StartDefaults->draw();
	m_AutoSpawn->draw();
	m_Ability_1->draw();
	m_Ability_2->draw();
	m_Ability_3->draw();
	m_Ability_4->draw();
	m_Ability_5->draw();
	m_Ability_6->draw();
	m_Slider_HP_Agent->draw();
	//m_Slider_AI_Tick->draw();
	m_Slider_Agents_Num->draw();
	m_Slider_HP_Boss->draw();
	m_Slider_BossDMG->draw();
	m_Slider_AgentDMG->draw();
	//m_Slider_BossSpeed->draw();
	//m_Slider_AgentSpeed->draw();
	m_DMG_Ability_1->draw();
	m_DMG_Ability_2->draw();
	m_DMG_Ability_3->draw();
	m_DMG_Ability_4->draw();
	m_DMG_Ability_5->draw();
	m_DMG_Ability_6->draw();
};

void SelectionState::Exit()
{
	delete m_Ability_1;
	m_Ability_1 = nullptr;
	delete m_Ability_2;
	m_Ability_2 = nullptr;
	delete m_Ability_3;
	m_Ability_3 = nullptr;
	delete m_Ability_4;
	m_Ability_4 = nullptr;
	delete m_Ability_5;
	m_Ability_5 = nullptr;
	delete m_Ability_6;
	m_Ability_6 = nullptr;
	delete m_Slider_AgentDMG;
	m_Slider_AgentDMG = nullptr;
	delete m_Slider_Agents_Num;
	m_Slider_Agents_Num = nullptr;
	//delete m_Slider_AI_Tick;
	//m_Slider_AI_Tick = nullptr;
	delete m_Slider_BossDMG;
	m_Slider_BossDMG = nullptr;
	delete m_Slider_HP_Agent;
	m_Slider_HP_Agent = nullptr;
	delete m_Slider_HP_Boss;
	m_Slider_HP_Boss = nullptr;
	//delete m_Slider_AgentSpeed;
	//m_Slider_AgentSpeed = nullptr;
	//delete m_Slider_BossSpeed;
	//m_Slider_BossSpeed = nullptr;
	delete m_DMG_Ability_1;
	m_DMG_Ability_1 = nullptr;
	delete m_DMG_Ability_2;
	m_DMG_Ability_2 = nullptr;
	delete m_DMG_Ability_3;
	m_DMG_Ability_3 = nullptr;
	delete m_DMG_Ability_4;
	m_DMG_Ability_4 = nullptr;
	delete m_DMG_Ability_5;
	m_DMG_Ability_5 = nullptr;
	delete m_DMG_Ability_6;
	m_DMG_Ability_6 = nullptr;
	delete m_AutoSpawn;
	m_AutoSpawn = nullptr;


	delete m_Start;
	m_Start = nullptr;
	delete m_StartDefaults;
	m_StartDefaults = nullptr;
};

std::string SelectionState::getName()
{
	return m_Name;
};
int SelectionState::getUpdateRequirement()
{
	return m_UpdateRequirement;
};
int SelectionState::getDrawRequirement()
{
	return m_DrawRequirement;
};

void SelectionState::handleStates()
{
	m_Start->update();
	m_StartDefaults->update();
	if (m_Start->isClicked()){
		setSettings();
		m_StateManager->activate("GameState");
		m_StateManager->deactivate("SelectionState");
		return;
	}
	if (m_StartDefaults->isClicked()){
		m_Settings->setDefaults();
		m_StateManager->activate("GameState");
		m_StateManager->deactivate("SelectionState");
		return;
	}
};

void SelectionState::updateAbilities()
{
	m_Ability_1->update();
	m_Ability_2->update();
	m_Ability_3->update();
	m_Ability_4->update();
	m_Ability_5->update();
	m_Ability_6->update();
};
void SelectionState::updateSliders()
{
	m_Slider_HP_Agent->update();
	//m_Slider_AI_Tick->update();
	m_Slider_HP_Boss->update();
	m_Slider_Agents_Num->update();
	m_Slider_BossDMG->update();
	m_Slider_AgentDMG->update();
	//m_Slider_BossSpeed->update();
	//m_Slider_AgentSpeed->update();

	m_DMG_Ability_1->update();
	m_DMG_Ability_2->update();
	m_DMG_Ability_3->update();
	m_DMG_Ability_4->update();
	m_DMG_Ability_5->update();
	m_DMG_Ability_6->update();
};


void SelectionState::setSettings()
{
	if (m_Ability_1->getState())
		m_Settings->addAbility("Smash");
	if (m_Ability_2->getState())
		m_Settings->addAbility("Cleave");
	if (m_Ability_3->getState())
		m_Settings->addAbility("Plague");
	if (m_Ability_4->getState())
		m_Settings->addAbility("Babaam");
	if (m_Ability_5->getState())
		m_Settings->addAbility("SpreadLava");
	if (m_Ability_6->getState())
		m_Settings->addAbility("JumpAway");

	m_Settings->setSetting(SettingTypes::AgentDMG, m_Slider_AgentDMG->getValue());
	m_Settings->setSetting(SettingTypes::AgentHP, m_Slider_HP_Agent->getValue());
	//m_Settings->setSetting(SettingTypes::AgentSpeed, m_Slider_AgentSpeed->getValue());

	m_Settings->setSetting(SettingTypes::BossDMG, m_Slider_AgentDMG->getValue());
	m_Settings->setSetting(SettingTypes::BossHP, m_Slider_HP_Boss->getValue());
	//m_Settings->setSetting(SettingTypes::BossSpeed, m_Slider_BossSpeed->getValue());

	//m_Settings->setSetting(SettingTypes::AITicks, m_Slider_AI_Tick->getValue());
	m_Settings->setSetting(SettingTypes::AgentNum, m_Slider_Agents_Num->getValue());

	m_Settings->setAutoSpawn(m_AutoSpawn->getState());

	m_Settings->setAbilityDMG("Smash", m_DMG_Ability_1->getValue());
	m_Settings->setAbilityDMG("Cleave", m_DMG_Ability_2->getValue());
	m_Settings->setAbilityDMG("Plague", m_DMG_Ability_3->getValue());
	m_Settings->setAbilityDMG("Babaam", m_DMG_Ability_4->getValue());
	m_Settings->setAbilityDMG("SpreadLava", m_DMG_Ability_5->getValue());
	m_Settings->setAbilityDMG("JumpAway", m_DMG_Ability_6->getValue());
};