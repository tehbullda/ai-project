//BlackBoard.cpp

#include "stdafx.h"
#include "BlackBoard.hpp"
#include "AuraManager.hpp"
#include "ServiceLocator.hpp"

BlackBoard::BlackBoard()
{
	//get auranames - add name and unknown to the map
	std::vector<AIGame::Aura*> m_auras = kartong::ServiceLocator<AIGame::AuraManager>::get_service()->GetExistingAuras();

	auto itr = m_auras.begin();
	while (itr != m_auras.end())
	{
		m_xAurasDispel.insert(std::pair<AIGame::Aura*, DispellableTypes>((*itr), Unknown));
		++itr;
	}
}

BlackBoard::~BlackBoard()
{

}

BlackBoard::Ptr BlackBoard::Create()
{
	return BlackBoard::Ptr(new BlackBoard());
}

sf::Vector2f BlackBoard::GetBossPos()
{
	return m_xBossPos;
}

void BlackBoard::UpdateBossPos(sf::Vector2f p_xPos)
{
	m_xBossPos = p_xPos;
}

void BlackBoard::AddMessage(const std::string &message) {
	m_messages.push_back(message);
}

std::vector<std::string> BlackBoard::GetMessages() {
	return m_messages;
}

BlackBoard::DispellableTypes BlackBoard::CanDispellAura(AIGame::Aura* p_xAura)
{
	auto itr = m_xAurasDispel.begin();
	while(itr != m_xAurasDispel.end())
	{
		if ((*itr).first->GetIdentifier() == p_xAura->GetIdentifier())
			return (*itr).second;
		++itr;
	}

	return Invalid;
}

void BlackBoard::SetAuraDispellable(AIGame::Aura* p_xAura, bool p_bDispellable)
{
	auto itr = m_xAurasDispel.begin();
	while (itr != m_xAurasDispel.end())
	{
		if ((*itr).first->GetIdentifier() == p_xAura->GetIdentifier())
		{
			if (p_bDispellable)
				(*itr).second = Dispellable;
			else
				(*itr).second = NotDispellable;
			break;
		}
		++itr;
	}
}