#pragma once

#include "stdafx.h"

#include "Window.hpp"
#include "Input.hpp"
#include "KeyBindings.hpp"
#include "StateManager.hpp"
#include "TextureManager.hpp"
#include "UnitManager.hpp"
#include "AudioManager.hpp"
#include "AudioListener.hpp"
#include "FontManager.hpp"
#include "DeltaTime.hpp"
#include "FPSGraph.hpp"
#include "ParticleManager.hpp"

#include "PostGameStats.hpp"

namespace kartong
{
	
	class Engine
	{
	public:
		Engine();
		~Engine();

		bool initialize();
		void run();
		void draw();

		static void engine_shut();
	private:
		void debugInit();
		void debugUpdate();
		void debugDraw();
	private:
		Window::Ptr m_Window;
		StateManager::Ptr m_StateManager;
		TextureManager::Ptr m_TextureManager;
		FontManager::Ptr m_FontManager;
		AudioManager::Ptr m_AudioManager;
		DeltaTime::Ptr m_DeltaTime;
		ParticleManager::Ptr m_ParticleManager;

		system::input::Input::Ptr m_Input;
		system::input::KeyBindings::Ptr m_KeyBinds;

	private:
		//Debug
		FPSGraph::Ptr m_FPSGraph;

		AIGame::PostGameStats::Ptr m_PostGameStats;
		
		
	};
}