//Selector.cpp

#include "stdafx.h"
#include "Selector.hpp"

Selector::Selector(AIGame::Agent* p_xOwner)
{

}

Selector::~Selector()
{
	auto itr = m_xChildren.begin();
	while (itr != m_xChildren.end())
	{
		(*itr) = nullptr;
		itr = m_xChildren.erase(itr);
	}
	m_xChildren.clear();

	BTNode::~BTNode();
}

void Selector::AddChild(BTNode* p_xNode)
{
	m_xChildren.push_back(p_xNode);
}

BTNode::StateType Selector::Update()
{
	while (true)
	{
		BTNode::StateType status = (*m_xCurrentChild)->Tick();

		if (status != Failure)
		{
			return status;
		}

		if (++m_xCurrentChild == m_xChildren.end())
		{
			return Failure;
		}
	}
	return Invalid;
}

void Selector::OnInitialize()
{
	m_xCurrentChild = m_xChildren.begin();
}

void Selector::OnTerminate(BTNode::StateType p_eState)
{
	auto itr = m_xChildren.begin();
	while (itr != m_xChildren.end())
	{
		(*itr)->SetState(BTNode::StateType::Invalid);
		++itr;
	}
	//m_eState = Invalid;
}

/*Different selectors*/
