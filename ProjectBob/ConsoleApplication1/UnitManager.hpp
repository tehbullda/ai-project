#pragma once

#include "stdafx.h"
#include "Math.hpp"

namespace kartong
{
	static float PixelsPerUnit;
	static float WinResH;

	class UnitManager
	{
	public:
		UnitManager();
		~UnitManager();

		//Not reccomended to change after it is set
		static void setUnitSize(const float& p_Pixels);
		//Not reccomended to change after it is set
		static void setUnitSize(const float& p_WindowSize, const float& p_UnitsPerScreen);


		static Vector2 getPixels(Vector2 p_Units);
		static float getPixels(const float& p_Units);

	
		

	};
}
