#include "stdafx.h"

#include "StateManager.hpp"

using namespace kartong;

StateManager::StateManager()
{
	m_CurrentStack.clear();
	m_States.clear();
};
StateManager::~StateManager()
{
	auto it = m_States.begin();
	while (it != m_States.end())
	{
		if (it->second)
		{
			delete it->second;
			it->second = nullptr;
		}
		it++;
	}
	m_States.clear();
	m_CurrentStack.clear();
};
StateManager::Ptr StateManager::Create()
{
	return StateManager::Ptr(new StateManager());
};

void StateManager::initialize()
{

};
void StateManager::update()
{
	for (unsigned int i = 0; i < m_CurrentStack.size(); i++)
	{
		if (m_CurrentStack[i]->getUpdateRequirement() == StateRequirement::ALWAYS){
			if (!m_CurrentStack[i]->Update()) {
				deactivate(m_CurrentStack[i]->getName());
			}
			continue;
		}
		else if (i == m_CurrentStack.size() - 1){ //If last
			if (m_CurrentStack[i]->getUpdateRequirement() != StateRequirement::NEVER){
				if (!m_CurrentStack[i]->Update()) {
					deactivate(m_CurrentStack[i]->getName());
				}
			}
		}
	}
};
void StateManager::draw()
{
	for (unsigned int i = 0; i < m_CurrentStack.size(); i++)
	{
		if (m_CurrentStack[i]->getDrawRequirement() == StateRequirement::ALWAYS){
			m_CurrentStack[i]->Draw();
		}
		else if (i == (m_CurrentStack.size() - 1)){ //If last
			if (m_CurrentStack[i]->getDrawRequirement() != StateRequirement::NEVER){
				m_CurrentStack[i]->Draw();
			}
		}
	}
};

void StateManager::attach(const std::string& p_Name, State* p_State)
{
	m_States.insert(std::pair<std::string, State*>(p_Name, p_State));
};
void StateManager::moveToTop(const std::string& p_Name)
{
	for (unsigned int i = 0; i < m_CurrentStack.size(); i++)
	{
		if (m_States.find(p_Name)->second == m_CurrentStack[i])
		{
			Debug::write(EDebugLevel::INFO, "Moved to top in stack: " + p_Name);
			m_CurrentStack.erase(m_CurrentStack.begin() + i);
		}
	}
	m_CurrentStack.push_back(m_States.find(p_Name)->second);
};
void StateManager::activate(const std::string& p_Name)
{
	for (unsigned int i = 0; i < m_CurrentStack.size(); i++)
	{
		if (m_CurrentStack[i]->getName() == p_Name)
		{
			Debug::write(EDebugLevel::INFO, "State already in active stack: " + p_Name);
			return;
		}
	}
	auto it = m_States.begin();
	while (it != m_States.end())
	{
		
		if (it->second->getName() == p_Name){
			Debug::write(EDebugLevel::INFO, "Added to active: " + p_Name);
			it->second->Initialize();
			m_CurrentStack.push_back(it->second);
		}
		it++;
	}
};
void StateManager::deactivate(const std::string& p_Name)
{
	for (unsigned int i = 0; i < m_CurrentStack.size(); i++)
	{
		if (m_CurrentStack[i]->getName() == p_Name){
			Debug::write(EDebugLevel::INFO, "Removed from active: " + p_Name);
			m_CurrentStack[i]->Exit();
			m_CurrentStack[i] = nullptr;
			m_CurrentStack.erase(m_CurrentStack.begin() + i);
			if (m_CurrentStack.size() == 0) {
				Debug::write(EDebugLevel::INFO, "WARNING: No State active");
			}
		}
	}
};
