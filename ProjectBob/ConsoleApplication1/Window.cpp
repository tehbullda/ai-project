#include "stdafx.h"

#include "Window.hpp"

using namespace kartong;
Window::Window(const std::string& p_WinName, const int& p_Style, float p_Width, const float& p_Height, const int& p_FPS)
{
	m_Window = new sf::RenderWindow(sf::VideoMode((unsigned int)p_Width, (unsigned int)p_Height), p_WinName, (unsigned int)p_Style);
	m_Window->setFramerateLimit(p_FPS);
	m_WindowRect = new sf::FloatRect();
	m_Size.x = p_Width;
	m_Size.y = p_Height;
	m_Icon = new sf::Image();
	m_Icon->loadFromFile("../Data/Textures/engine/Icon.png");
	m_Window->setIcon(16, 16, m_Icon->getPixelsPtr());
	m_DefaultView = new sf::View();
	m_DefaultView->setSize(p_Width, p_Height);
	m_DefaultView->setCenter(p_Width / 2, p_Height / 2);
	
};
Window::~Window()
{
	if (m_Window){
		delete m_Window;
		m_Window = nullptr;
	}
	if (m_WindowRect){
		delete m_WindowRect;
		m_WindowRect = nullptr;
	}
	if (m_Icon){
		delete m_Icon;
		m_Icon = nullptr;
	}
	if (m_DefaultView){
		delete m_DefaultView;
		m_DefaultView = nullptr;
	}
};

Window::Ptr Window::Create(const std::string& p_WinName, const int& p_Style, float p_Width, const float& p_Height, const int& p_FPS)
{
	return Window::Ptr(new Window(p_WinName, p_Style, p_Width, p_Height, p_FPS));
}

bool Window::initialize()
{

	return true;
};
void Window::updateRect()
{
		m_WindowRect->top = m_Window->getView().getCenter().x - (m_Window->getView().getSize().x / 2.0f);
		m_WindowRect->left = m_Window->getView().getCenter().y - (m_Window->getView().getSize().y / 2.0f);
		m_WindowRect->width = (float)m_Window->getSize().x;
		m_WindowRect->left = (float)m_Window->getSize().y;
}
void Window::display()
{
	m_Window->display();
};
void Window::close()
{
	m_Window->close();
};
void Window::clear()
{
	m_Window->clear(m_ClearColor);
};
void Window::setClearColor(const sf::Color& p_Color)
{
	m_ClearColor = p_Color;
}
void Window::setView(sf::View& p_View){
	m_Window->setView(p_View);
};
sf::Vector2f Window::getPos()
{
	return m_Position;
};
sf::Vector2f Window::getSize()
{
	return m_Size;
}

sf::FloatRect* Window::getFloatRect()
{
	return m_WindowRect;
};

sf::RenderWindow* Window::getWindow()
{
	return m_Window;
};
void Window::setDefaultView()
{
	m_Window->setView(*m_DefaultView);
};