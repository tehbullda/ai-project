#include "stdafx.h"
#include "ParticleManager.hpp"

using namespace kartong;


ParticleManager::ParticleManager()
{

};

ParticleManager::~ParticleManager()
{
	for (unsigned i = 0; i < m_Emitters.size(); i++){
		delete m_Emitters[i];
		m_Emitters.erase(m_Emitters.begin() + i);
	}

};

ParticleManager::Ptr ParticleManager::Create()
{
	return ParticleManager::Ptr(new ParticleManager());
};

void ParticleManager::update()
{
	for (unsigned i = 0; i < m_Emitters.size(); i++){
		m_Emitters[i]->update();
		if (m_Emitters[i]->m_Stopped && m_Emitters[i]->getActiveParticles() <= 0){
			delete m_Emitters[i];
			m_Emitters.erase(m_Emitters.begin() + i);
		}
	}

};

ParticleEmitter* ParticleManager::createEmitter(std::string p_texname)
{
	ParticleEmitter* emi = new ParticleEmitter(p_texname);
	m_Emitters.push_back(emi);
	return emi;
};
