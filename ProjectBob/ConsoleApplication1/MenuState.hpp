#pragma once

#include "stdafx.h"
#include "TextureManager.hpp"
#include "FontManager.hpp"
#include "PremadeButton.hpp"
#include "Tooltip.hpp"

namespace AIGame
{
	class MenuState : public kartong::State
	{
	public:
		MenuState(const std::string& p_Name);
		~MenuState();


		bool Initialize();
		bool Update();
		void Draw();
		void Exit();


		std::string getName();
		int getUpdateRequirement();
		int getDrawRequirement();

	private:
		void handleStates();
	private:
		std::string m_Name, m_Next;
		int m_DrawRequitement;
		int m_UpdateRequitement;
	private:
		kartong::StateManager* m_StateManager;
		kartong::TextureManager* m_TextureManager;
		kartong::FontManager* m_FontManager;

		std::vector<kartong::Premade::PremadeButton*> m_buttons;

		Tooltip *m_tooltip;


	private:

	};
}