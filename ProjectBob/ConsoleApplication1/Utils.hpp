#pragma once

#include "stdafx.h"

namespace kartong
{
	class Utils
	{
	public:
		static void cutAtPoint(std::string p_String, const char& p_Point, std::string* p_First, std::string* p_Second);
		static std::string ltrim(std::string p_String);
		static std::string trim(std::string p_String);
		static std::string rtrim(std::string p_String);

		float string_to_float(std::string s);

	};
}