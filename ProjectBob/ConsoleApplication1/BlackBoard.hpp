//BlackBoard.hpp

#include "stdafx.h"
#pragma once

#include "Aura.hpp"

/*Keeps track of all information needed for BT*/
class BlackBoard{
public:
	enum DispellableTypes
	{
		Unknown,
		Dispellable,
		NotDispellable,
		Invalid,
	};
private:
	BlackBoard();
public:
	~BlackBoard();

	typedef std::unique_ptr<BlackBoard> Ptr;
	static Ptr Create();

	void UpdateBossPos(sf::Vector2f p_xPos);
	sf::Vector2f GetBossPos();

	void AddMessage(const std::string &message);
	std::vector<std::string> GetMessages();

	DispellableTypes CanDispellAura(AIGame::Aura* p_xAura);

	void SetAuraDispellable(AIGame::Aura* p_xAura, bool p_bDispellable);
private:
	sf::Vector2f m_xBossPos;
	std::vector<std::string> m_messages;
	std::map<AIGame::Aura*, DispellableTypes> m_xAurasDispel;
};