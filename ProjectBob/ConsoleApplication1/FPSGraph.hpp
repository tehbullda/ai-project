#pragma once
#include "GraphWindow.hpp"

namespace kartong
{
	class FPSGraph
	{
	private:
		FPSGraph();
	public:
		~FPSGraph();
		typedef std::unique_ptr<FPSGraph> Ptr;
		static Ptr Create();

		void update();
		void draw();


	private:
		float getAverage();
		void toggleActive();
	private:
		bool m_Active;

		FontManager* m_FontManager;
		system::input::Input* m_Input;
		Window* m_Window;
		GraphWindow::Ptr m_Graph;
		sf::Text* m_FPStext;
		float m_RefreshRate;
		float m_Timer;

		int m_fps;

	private:
		std::vector<float> m_Values;
	};
}