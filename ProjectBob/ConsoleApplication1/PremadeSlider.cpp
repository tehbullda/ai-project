#include "stdafx.h"
#include "PremadeSlider.hpp"
#include "ServiceLocator.hpp"

#define PADDING 2

using namespace kartong::Premade;

PremadeSlider::PremadeSlider(const std::string& p_Text, const float& p_Max)
{
	m_Window = ServiceLocator<Window>::get_service();
	m_Input = ServiceLocator<system::input::Input>::get_service();
	m_Slider = new PremadeButton(ServiceLocator<TextureManager>::get_service()->loadTexture("engine/slider.png"));
	m_Track.setSize(sf::Vector2f(300, 4));
	m_Track.setFillColor(sf::Color(8, 8, 8, 200));


	m_Back.setSize(sf::Vector2f(310, 30));
	m_Back.setOutlineThickness(1);
	m_Back.setOutlineColor(sf::Color::Black);
	m_Back.setFillColor(sf::Color(200, 200, 200, 200));


	m_Num = new sf::Text("0.0", *ServiceLocator<FontManager>::get_service()->loadFont("tahomabd.ttf"), 12);
	m_Num->setColor(sf::Color::Black);

	m_Text = new sf::Text(p_Text, *ServiceLocator<FontManager>::get_service()->loadFont("tahomabd.ttf"), 12);
	m_Text->setColor(sf::Color::White);

	m_maxValue = p_Max;

};
PremadeSlider::~PremadeSlider()
{
	delete m_Text;
	m_Text = nullptr;
	delete m_Num;
	m_Num = nullptr;
	delete m_Slider;
	m_Slider = nullptr;
};

void PremadeSlider::update()
{
	m_Text->setPosition(sf::Vector2f(m_Back.getPosition().x, m_Back.getPosition().y - 16));
	m_Track.setPosition(sf::Vector2f(m_Back.getPosition().x + 5, m_Back.getPosition().y + 12));
	m_Slider->setPosition(sf::Vector2f(m_Slider->getPosition().x, m_Back.getPosition().y - 1));

	m_Slider->update();
	updatePercent();
	m_Num->setString(std::to_string((int)((m_Value * 0.01) * m_maxValue)));
	m_Num->setColor(sf::Color((sf::Uint8)(255 * (m_Value * 0.01)), 0, 0, 255));

	if (m_Slider->isHeld())
		m_Slider->setPosition(sf::Vector2f(m_Input->getMousePos().x - m_Slider->getSize().x / 2, m_Slider->getPosition().y));

	m_MinPos = m_Track.getPosition().x + PADDING;
	m_MaxPos = (m_Track.getPosition().x + m_Track.getGlobalBounds().width) - m_Slider->getSize().x - PADDING;

	if (m_Slider->getPosition().x < m_MinPos)
		m_Slider->setPosition(sf::Vector2f(m_MinPos, m_Slider->getPosition().y));
	if (m_Slider->getPosition().x > m_MaxPos)
		m_Slider->setPosition(sf::Vector2f(m_MaxPos, m_Slider->getPosition().y));

	m_Num->setOrigin(sf::Vector2f(m_Num->getGlobalBounds().width / 2, m_Num->getGlobalBounds().height / 2));
	m_Num->setPosition(sf::Vector2f(m_Slider->getPosition().x + m_Slider->getSize().x / 2, m_Slider->getPosition().y + m_Slider->getSize().y / 2 - PADDING));
};
void PremadeSlider::updatePercent()
{
	float max = m_MaxPos - m_MinPos;
	float curr = m_Slider->getPosition().x - m_MinPos;
	m_Value = (curr / max) * 100;
};
void PremadeSlider::draw()
{
	m_Window->getWindow()->draw(m_Back);
	m_Window->getWindow()->draw(m_Track);
	m_Slider->draw();
	m_Window->getWindow()->draw(*m_Text);
	m_Window->getWindow()->draw(*m_Num);
};

int PremadeSlider::getPercentage()
{
	return m_Value;
};
float PremadeSlider::getValue()
{
	return (m_Value * 0.01f) * m_maxValue;
};
void PremadeSlider::setPosition(const sf::Vector2f& p_Pos)
{
	m_Back.setPosition(p_Pos);
};