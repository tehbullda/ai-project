//GameObjectManager

#pragma once
#include "stdafx.h"
//#include "GameObject.hpp"
#include "Agent.hpp"
#include "Projectile.hpp"
#include "Boss.hpp"
#include "BTCreator.hpp"
#include "SelectionSettings.hpp"

namespace AIGame {

	class GameObjectManager{

	private:
		GameObjectManager();

	public:
		typedef std::unique_ptr<GameObjectManager> Ptr;
		static Ptr Create();

		~GameObjectManager();

		static unsigned int GetNewPlayerID()
		{
			return m_uiNewPlayerID++;
		}

		void Update();
		void DrawObjects();

		void CheckCollisionsProjBoss();
		void SetBossPos(sf::Vector2i pos);

		void CreateObject(GameObject::ObjectType p_xType);
		void CreateProjectile(sf::Vector2f p_xStartPos, sf::Vector2f p_xEndPos, float p_fDamage);

		std::vector<Agent*> GetAgentVector(); 

		void Reset();

	private:
		static unsigned int m_uiNewPlayerID;

		std::vector<GameObject*> m_xObjectList;
		std::vector<Projectile*> m_xProjectiles;
		std::vector<Agent*> m_xAgents;
		Boss* m_xBoss;

		SelectionSettings* m_xSettings;
		BTCreator* m_xBTCreator;
	};
}
