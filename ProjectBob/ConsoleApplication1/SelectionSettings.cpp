#include "stdafx.h"

#include "SelectionSettings.hpp"

using namespace AIGame;

SelectionSettings::SelectionSettings()
{

};
SelectionSettings::~SelectionSettings()
{

};
SelectionSettings::Ptr SelectionSettings::Create()
{
	return SelectionSettings::Ptr(new SelectionSettings());
};

std::vector<std::string> SelectionSettings::getAbilities()
{
	return m_Abilities;
};
float SelectionSettings::getSetting(const SettingTypes& p_Setting)
{
	return m_Settings.find(p_Setting)->second;
};

void SelectionSettings::addAbility(const std::string& p_Ability)
{
	kartong::Debug::write(kartong::EDebugLevel::INFO, "Settings: Ability added: " + p_Ability);
	m_Abilities.push_back(p_Ability);
};
void SelectionSettings::setSetting(const SettingTypes& p_Setting, const float& p_Value)
{
	std::string SettingTypesNames[] =
	{
		"BossDMG",
		"BossHP",
		"BossSpeed",
		"AgentDMG",
		"AgentHP",
		"AgentSpeed",
		"AgentNum",
		"AITicks"
	};
	kartong::Debug::write(kartong::EDebugLevel::INFO, "Settings: " + SettingTypesNames[p_Setting] + "  set to: " + std::to_string(p_Value));
	m_Settings.insert(std::pair<SettingTypes, float>(p_Setting, p_Value));
	m_Default = false;
};
void SelectionSettings::setDefaults()
{
	kartong::Debug::write(kartong::EDebugLevel::INFO, "Settings: Default Set");
	m_Settings.clear();
	m_Abilities.clear();

	m_Settings.insert(std::pair<SettingTypes, float>(SettingTypes::AgentDMG, 100));
	m_Settings.insert(std::pair<SettingTypes, float>(SettingTypes::AgentHP, 100));
	m_Settings.insert(std::pair<SettingTypes, float>(SettingTypes::AgentSpeed, 100));
	m_Settings.insert(std::pair<SettingTypes, float>(SettingTypes::BossDMG, 100));
	m_Settings.insert(std::pair<SettingTypes, float>(SettingTypes::BossHP, 35000));
	m_Settings.insert(std::pair<SettingTypes, float>(SettingTypes::BossSpeed, 100));
	m_Settings.insert(std::pair<SettingTypes, float>(SettingTypes::AITicks, 5));
	m_Settings.insert(std::pair<SettingTypes, float>(SettingTypes::AgentNum, 10));
	m_Default = true;


};
bool SelectionSettings::isDefault()
{
	return m_Default;
};
float SelectionSettings::getAbilityDMG(const std::string& p_Ability)
{
	return m_AbilityDMG.find(p_Ability)->second;
};
void SelectionSettings::setAbilityDMG(const std::string& p_Ability, const float& p_Value)
{
	kartong::Debug::write(kartong::EDebugLevel::INFO, "Settings: " + p_Ability + " DMG% set to: " + std::to_string(p_Value));
	m_AbilityDMG.insert(std::pair<std::string, float>(p_Ability, p_Value));
};
bool SelectionSettings::getAutoSpawn()
{
	return m_AutoSpawn;
};
void SelectionSettings::setAutoSpawn(const bool& p_Auto)
{
	m_AutoSpawn = p_Auto;
};
