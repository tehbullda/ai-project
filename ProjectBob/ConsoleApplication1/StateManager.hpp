#pragma once

#include "stdafx.h"
#include "State.hpp"

namespace kartong
{
	class StateManager
	{
	private:
		StateManager();
	public:
		~StateManager();

		typedef std::unique_ptr<StateManager> Ptr;
		static Ptr Create();

		void initialize();
		void update();
		void draw();
		

		void attach(const std::string& p_Name, State* p_State);

		

		void moveToTop(const std::string& p_State);
		void activate(const std::string& p_Name);
		void deactivate(const std::string& p_Name);

	private:
		std::map<std::string, State*> m_States;
		std::vector<State*> m_CurrentStack;


	};

}