#pragma once
#include "stdafx.h"
#include "Agent.hpp"
#include "Aura.hpp"

namespace AIGame {
	class AuraManager {
		struct AgentAura {
			AgentAura(Agent *agent, bool active = true) {
				m_agent = agent;
				m_active = active;
			}
			AgentAura(Agent *agent, std::vector<Aura*> auras, bool active = true) {
				m_agent = agent;
				m_activeauras = auras;
				m_active = active;
			}
			Agent* m_agent;
			std::vector<Aura*> m_activeauras;
			bool m_active;
		};
	public:
		AuraManager();
		AuraManager(std::vector<Agent*> agents);
		~AuraManager();

		void UpdateAuras();
		void HandleTick(Agent *target, Aura *aura);
		void HandleApplication(Agent *target, Aura *aura);
		void HandleRemoval(Agent *target, Aura *aura);
		void SetAgentVector(std::vector<Agent*> agents);
		void AgentAdded(Agent *agent);
		void AgentRemoved(Agent *agent);

		void AddAura(Aura *aura);
		void AddAuraToAgent(Agent *target, Aura *aura);
		/*	EffectTypes: 
			DamageOverTime,
			LinearDamageOverTime,
			ExponentialDamageOverTime,
			HealingOverTime,
			DamageMultiplier,
			DamageIncrease,
			DamageDecrease,
			DamageBlock,
			HPIncrease,
			HPDecrease,
			Stun,
			Slow
			AuraTypes:
			Buff,
			Debuff,
			Global
		*/
		void AddAuraToAgent(Agent *target, const std::string &effecttype, const std::string &auratype, unsigned int power);
		/*	EffectTypes:
		DamageOverTime,
		LinearDamageOverTime,
		ExponentialDamageOverTime,
		HealingOverTime,
		DamageMultiplier,
		DamageIncrease,
		DamageDecrease,
		DamageBlock,
		HPIncrease,
		HPDecrease,
		Stun,
		Slow
		AuraTypes:
		Buff,
		Debuff,
		Global
		*/
		void AddAuraToAllAgents(const std::string &effecttype, const std::string &auratype, unsigned int power);
		void AddAuraToAllAgents(Aura *aura);
		void RemoveAuraFromAgent(Agent *target, Aura *aura);
		bool DispelTarget(Agent *target);

		std::vector<Aura*> GetExistingAuras();

		void DrawIcons();

	private:
		bool LoadAuras();
		void RemoveAuraFromAgentByIndex(int agent, int aura);
	public:
		static Aura::AuraType StringToAuraType(const std::string &type);
		static Aura::EffectType StringToEffectType(const std::string &type);

	private:
		std::vector<AgentAura> m_agentauras;
		std::vector<Aura*> m_auras;
		
	};
}