#include "stdafx.h"

#include "Debug.hpp"
#include <chrono>
#include <ctime>
#include <cassert>
#include <io.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
using namespace kartong;

EDebugLevel Debug::ms_level = EDebugLevel::INFO;

void Debug::set_debug_level(EDebugLevel p_Level){
	ms_level = p_Level;
};

void Debug::write(const EDebugLevel p_Level, const std::string& p_Text){
	if ((uint32_t)p_Level < (uint32_t)ms_level)
		return;
	std::cout << timestamp().c_str() << ": ";
	std::cout << p_Text << std::endl;

}

void Debug::write(const EDebugLevel p_Level, const std::wstring& p_Text){
	if ((uint32_t)p_Level < (uint32_t)ms_level)
		return;

	std::string t = timestamp();
	std::cout << timestamp().c_str() << ": ";
	std::wcout << p_Text << std::endl;

}
std::string Debug::timestamp(){
	static char tsmp[32];
	auto now = std::chrono::system_clock::now();
	auto s = std::chrono::duration_cast<std::chrono::seconds>(now.time_since_epoch());
	auto tt = std::chrono::system_clock::to_time_t(now);

	std::strftime(tsmp, sizeof(tsmp), "%H:%M:%S", localtime(&tt));
	return std::string(tsmp);
};