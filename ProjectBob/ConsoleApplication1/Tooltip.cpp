#include "stdafx.h"
#include "Tooltip.hpp"
#include "ServiceLocator.hpp"
namespace AIGame {
#define maxrowlength_default_pixels (uint32_t)(kartong::ServiceLocator<kartong::Window>::get_service()->getSize().x / 3)
#define rectoffset_pixels 5

	Tooltip::Tooltip(const std::string &text, const sf::Vector2f &pos, const uint32_t &charactersize, const sf::Color fillcolor, const bool &followsmouse)
		/*: Text(text, pos, charactersize)*/ {
			m_maxrowlength = maxrowlength_default_pixels;
			m_boundingrect.setFillColor(fillcolor);
			m_boundingrect.setOutlineThickness(1);
			m_boundingrect.setPosition(pos);
			m_text.setCharacterSize(charactersize);
			m_text.setPosition(pos);
			SetText(text);
			m_followsmouse = followsmouse;
			m_visible = true;
			m_triggerarea = sf::IntRect(0, 0, 0, 0);
	}

	Tooltip::~Tooltip() {

	}

	void Tooltip::SetText(const std::string &text) {
		//Text::SetString(text);
		for (unsigned i = 0; i < text.size(); i++) {
			m_text.setString(m_text.getString() + text[i]);
			if (text[i] == ' ') {
				if (m_text.findCharacterPos(m_text.getString().getSize()).x - m_text.getGlobalBounds().left > m_maxrowlength) {
					AdjustText();
				}
			}
		}
		UpdateRect();
	}

	void Tooltip::Update() {
		if (m_triggerarea.width > 0 && m_triggerarea.height > 0) {
			if (m_triggerarea.contains((sf::Vector2i(kartong::ServiceLocator<kartong::system::input::Input>::get_service()->getMousePos())))) {
				SetVisibility(true);
			}
			else {
				SetVisibility(false);
			}
		}
		if (m_followsmouse && m_visible) {
			sf::Vector2f newpos = kartong::ServiceLocator<kartong::system::input::Input>::get_service()->getMousePos();
			if (newpos.x > kartong::ServiceLocator<kartong::Window>::get_service()->getSize().x - m_boundingrect.getLocalBounds().width) {
				m_text.setPosition(newpos.x - m_text.getLocalBounds().width - rectoffset_pixels, newpos.y - rectoffset_pixels * 3);
			}
			else {
				m_text.setPosition(newpos.x + rectoffset_pixels, newpos.y - rectoffset_pixels * 3);
			}
			UpdateRect();
		}
	}

	void Tooltip::SetTriggerArea(const sf::IntRect area) {
		m_triggerarea = area;
	}

	void Tooltip::SetMaxRowlength(const uint32_t &pixels) {
		m_maxrowlength = pixels;
	}

	void Tooltip::SetVisibility(const bool &status) {
		m_visible = status;
	}

	void Tooltip::SetMouseFollow(const bool &status) {
		m_followsmouse = status;
	}

	void Tooltip::SetRectFillColor(const sf::Color &color) {
		m_boundingrect.setFillColor(color);
	}

	void Tooltip::SetRectOutlineColor(const sf::Color &color) {
		m_boundingrect.setOutlineColor(color);
	}

	void Tooltip::SetRectOutlineThickness(const float &thickness) {
		m_boundingrect.setOutlineThickness(thickness);
	}

	bool Tooltip::GetVisibility() {
		return m_visible;
	}

	bool Tooltip::GetMouseFollow() {
		return m_followsmouse;
	}

	void Tooltip::Draw() {
		if (m_visible) {
			kartong::ServiceLocator<kartong::Window>::get_service()->getWindow()->draw(m_boundingrect);
			Text::Draw();
		}
	}

	//private
	void Tooltip::AdjustText() {
		m_text.setString(m_text.getString() + '\n');
	}

	void Tooltip::UpdateRect() {
		m_boundingrect.setPosition(sf::Vector2f(m_text.getGlobalBounds().left - rectoffset_pixels, m_text.getGlobalBounds().top - rectoffset_pixels));
		m_boundingrect.setSize(sf::Vector2f(m_text.getGlobalBounds().width + rectoffset_pixels * 2, m_text.getGlobalBounds().height + rectoffset_pixels * 2));
	}
}