#pragma once

#include "stdafx.h"

namespace kartong
{
	enum BoundingTypes
	{
		BOX,
		SPHERE,
		SIZE
	};
	class BoundingUnit
	{
	public:
		BoundingUnit(const sf::Vector2f& p_xPosOffset = sf::Vector2f(0.0f, 0.0f));
		~BoundingUnit();

		virtual bool pointIntersects(const sf::Vector2f& p_Point) = 0;
		virtual bool AABBIntersects(const sf::Vector2f& p_Pos, const sf::Vector2f& p_Vol) = 0;
		virtual bool circleIntersects(const sf::Vector2f& p_Pos, const float& p_Rad) = 0;
		virtual void rayIntersects(const sf::Vector2f& p_Pos, const sf::Vector2f& p_Direction) = 0;
		//virtual void setVolume(const sf::Vector2f& p_Vol);
		//virtual void setRadius(const float& p_Rad);

		void setPosition(const sf::Vector2f& p_Pos);
		sf::Vector2f GetPos();
		virtual BoundingTypes GetType();
	private:
		virtual void recalculate() = 0;


	protected:
		sf::Vector2f m_Position;
		sf::Vector2f m_xPosOffset;
		bool m_Dirty;
		BoundingTypes m_Type;
	};


	class AxisAlignedBoundingBox : public BoundingUnit
	{
	public:
		AxisAlignedBoundingBox(const sf::Vector2f& p_Pos, const sf::Vector2f& p_Vol, const sf::Vector2f& p_xPosOffset = sf::Vector2f(0.0f, 0.0f));
		~AxisAlignedBoundingBox();

		void  setVolume(const sf::Vector2f& p_Vol);

		bool pointIntersects(const sf::Vector2f& p_Point);
		bool AABBIntersects(const sf::Vector2f& p_Pos, const sf::Vector2f& p_Vol);
		bool circleIntersects(const sf::Vector2f& p_Pos, const float& p_Rad);
		void rayIntersects(const sf::Vector2f& p_Pos, const sf::Vector2f& p_Direction);

		BoundingTypes GetType();

	private:
		void recalculate();
	private:
		sf::Vector2f m_Volume;
		sf::FloatRect m_Box;
	};


	class BoundingSphere : public BoundingUnit
	{
	public:
		BoundingSphere(const sf::Vector2f& p_Pos, const float& p_Rad, const sf::Vector2f& p_xPosOffset = sf::Vector2f(0.0f, 0.0f));
		~BoundingSphere();

		void setRadius(const float& p_Rad);

		bool pointIntersects(const sf::Vector2f& p_Point);
		bool AABBIntersects(const sf::Vector2f& p_Pos, const sf::Vector2f& p_Vol);
		bool circleIntersects(const sf::Vector2f& p_Pos, const float& p_Rad);
		void rayIntersects(const sf::Vector2f& p_Pos, const sf::Vector2f& p_Direction);

		BoundingTypes GetType();

		float GetRadius();
	private:
		void recalculate();
	private:
		float m_Radius;
	};
};
