#pragma once

#include "stdafx.h"

namespace kartong
{
	class TextureManager
	{
	public:
		TextureManager(const std::string& p_Directory);
		~TextureManager();

		typedef std::unique_ptr<TextureManager> Ptr;
		static Ptr Create(const std::string& p_Directory);

		void preloadTexture(const std::string& p_File);
		sf::Texture* loadTexture(const std::string& p_File);
		sf::Texture* getMissingTexture();


	private:
		std::map<std::string, sf::Texture*> m_Textures;
		std::string m_Directory;

	};
}