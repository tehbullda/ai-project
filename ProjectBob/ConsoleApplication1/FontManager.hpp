#pragma once
#include "stdafx.h"

namespace kartong
{
	class FontManager
	{
		FontManager(const std::string& p_Directory);
	public:
		~FontManager();

		typedef std::unique_ptr<FontManager> Ptr;
		static Ptr Create(const std::string& p_Directory);

		void preLoadFont(const std::string& p_File);
		sf::Font* loadFont(const std::string& p_File);

		sf::Font* getDefaultFont();

	private:
		std::map<std::string, sf::Font*> m_Fonts;
		std::string m_Directory;
	};
}