//Agent.hp

#pragma once
#include "stdafx.h"
#include "GameObject.hpp"
//#include "BTNode.hpp"
#include "NameHandler.hpp"

class BTNode;

namespace AIGame
{
	class Aura;
class Agent : public GameObject {
	enum AgentType {
		DPSRanged,
		DPSMelee,
		Healer,
		Tank,
		Laban
	};
	struct Attributes {
		uint32_t m_hitpoints;
		std::string m_name;
		AgentType m_type;
	};
public:
	Agent(sf::Vector2f p_xPos);
	~Agent();


		bool Update();
		void UpdateBB();

		bool CanAttack();
		float GetRange();
		void AddBTRoot(BTNode* p_xBTRoot);
		void ApplyAura(Aura *aura);

		float GetDamage();
		float GetHP();
		void SetMovespeed(float ms);
		float GetMovespeed();
		void SetStun(bool status);
		bool IsStunned();
		bool HasAura();

		void NotifyDebuff(Aura *aura, bool remove = false);

		void TakeDamage(const int &damage);
		void SetDamage(float p_fDamage);
		void SetHP(float p_fHP);

		void SetSettings(float p_fHP, float p_fDamage, unsigned int p_uID, NameTag* p_xNameTag);

		std::vector<Aura*> GetAuras();

		std::string GetName();

		void SetPosAndOccupied(sf::Vector2f p_xNewPos);

	private:
		BTNode* m_xBTRoot;
		Attributes m_attributes;

		NameTag* m_xNameTag;
		std::vector<Aura*> m_auras;
		float m_fRange;
		float m_fAttackCD;
		float m_fDecisionTime;
		float m_fDamage;
		float m_fMovespeed;
		bool m_bStunned;
		float m_fTimeBetweenDecisions;
		float m_fHP;
		float m_fMaxHP;

		float m_LifeTimeTimer;
		int m_ShotsFired;
		int m_DamageDealt;
		int m_DamageTaken;

		unsigned int m_uID;
	};
}
