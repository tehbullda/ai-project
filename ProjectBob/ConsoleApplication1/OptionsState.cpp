#include "stdafx.h"

#include "OptionsState.hpp"
#include "ServiceLocator.hpp"
#include "Randomizer.hpp"
#include "PremadeButton.hpp"

using namespace kartong;
using namespace AIGame;
OptionsState::OptionsState(const std::string& p_Name)
{
	m_StateManager = ServiceLocator<StateManager>::get_service();
	m_TextureManager = ServiceLocator<TextureManager>::get_service();
	m_FontManager = ServiceLocator<FontManager>::get_service();

	m_Name = p_Name;
	m_UpdateRequitement = StateRequirement::ALWAYS;
	m_DrawRequitement = StateRequirement::ALWAYS;
};
OptionsState::~OptionsState()
{
	for (unsigned i = 0; i < m_buttons.size(); i++) {
		delete m_buttons[i];
		m_buttons[i] = nullptr;
	}
	m_TextureManager = nullptr;
	m_FontManager = nullptr;
	m_StateManager = nullptr;

};

bool OptionsState::Initialize()
{
	m_StateManager = ServiceLocator<StateManager>::get_service();
	m_TextureManager = ServiceLocator<TextureManager>::get_service();
	m_FontManager = ServiceLocator<FontManager>::get_service();

	//m_buttons.push_back(new kartong::Premade::PremadeButton(m_TextureManager->loadTexture("menu/start_game.png"), "Back"));
	//m_buttons[0]->setPosition(sf::Vector2f(50, 300));
	//m_buttons.push_back(new kartong::Premade::PremadeButton(m_TextureManager->loadTexture("menu/options.png"), "Options"));
	//m_buttons[1]->setPosition(sf::Vector2f(50, 400));
	m_buttons.push_back(new kartong::Premade::PremadeButton(m_TextureManager->loadTexture("menu/quit_game.png"), "Back"));
	m_buttons[0]->setPosition(sf::Vector2f(50, 500));

	return true;
};
bool OptionsState::Update()
{
	for (unsigned i = 0; i < m_buttons.size(); i++) {
		m_buttons[i]->update();
		if (m_buttons[i]->isClicked()) {
			if (m_buttons[i]->getIdentifer() == "StartGame") {
				handleStates();
			}
			else if (m_buttons[i]->getIdentifer() == "Options") {

			}
			else if (m_buttons[i]->getIdentifer() == "Back") {
				handleStates();
			}
		}
	}
	/*m_TextureManager->preloadTexture("GUIHUD/Resume.png");
	m_TextureManager->preloadTexture("GUIHUD/Options.png");
	m_TextureManager->preloadTexture("GUIHUD/Menu.png");
	m_TextureManager->preloadTexture("GUIHUD/TextChat_Button.png");
	m_FontManager->preLoadFont("AdobeGothicStd-Bold.otf");*/

	//handleStates();
	return true;
};
void OptionsState::Draw()
{
	for (unsigned i = 0; i < m_buttons.size(); i++) {
		m_buttons[i]->draw();
	}
};

void OptionsState::Exit()
{
	for (unsigned i = 0; i < m_buttons.size(); i++) {
		delete m_buttons[i];
		m_buttons[i] = nullptr;
	}
	m_buttons.clear();
	m_FontManager = nullptr;
	m_TextureManager = nullptr;
};

std::string OptionsState::getName()
{
	return m_Name;
};
int OptionsState::getUpdateRequirement()
{
	return m_UpdateRequitement;
};
int OptionsState::getDrawRequirement()
{
	return m_DrawRequitement;
};

void OptionsState::handleStates()
{
	m_StateManager->activate("MenuState");
	m_StateManager->deactivate(m_Name);
};
