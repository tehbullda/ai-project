// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <conio.h>

#include "Engine.hpp"

#include "vld.h"


using namespace kartong;

int _tmain(int argc, _TCHAR* argv[])
{
	argc;
	argv;
	Engine engine;

	if (engine.initialize())
		engine.run();
	else
	{
		Debug::write(EDebugLevel::ERROR, "Initialize failed");
		_getch();
	}

	
	return 0;
}